/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    08 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include "interfaces.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

Frame *construct_frame(
    void *(*FUNC_contruct)(),
    Error (*FUNC_destruct)(Frame *self),
    Frame *(*FUNC_new)(Frame *self, uint32_t size),
    Error (*FUNC_clean)(Frame *self),
    Error (*FUNC_resize)(Frame *self, uint32_t size),
    Error (*FUNC_reset)(Frame *self),
    Error (*FUNC_copy_into)(Frame *self, Frame *destiny),
    Error (*FUNC_copy_interval)(
        Frame *self, Frame *destiny, uint32_t start, uint32_t end),
    Error (*FUNC_print)(Frame *self),
    uint32_t (*FUNC_size)(Frame *self),
    uint32_t (*FUNC_quantity)(Frame *self),
    Error (*FUNC_to_matrix)(Frame *self, Matrix *matrix),
    Error (*FUNC_position_push_cartesian)(
        Frame *self, CoordinatesCartesian point, uint32_t position),
    Error (*FUNC_position_push_spherical)(
        Frame *self, CoordinatesSpherical point, uint32_t position),
    Error (*FUNC_push_cartesian)(Frame *self, CoordinatesCartesian point),
    Error (*FUNC_push_spherical)(Frame *self, CoordinatesSpherical point),
    Error (*FUNC_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, uint32_t position),
    Error (*FUNC_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, uint32_t position),
    Error (*FUNC_highest_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, AXIS axis),
    Error (*FUNC_highest_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, AXIS axis),
    Error (*FUNC_lowest_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, AXIS axis),
    Error (*FUNC_lowest_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, AXIS axis),
    Error (*FUNC_move_points)(Frame *self, float x_axis, float y_axis, float z_axis),
    Error (*FUNC_trade_axis)(Frame *self, AXIS a, AXIS b),
    Error (*FUNC_mirror_axis)(Frame *self, AXIS a, SIGNAL signal));

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif