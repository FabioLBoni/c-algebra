/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    23 de jan de 2024
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <stdlib.h>

#include "i_Logger.h"

Logger *construct_logger(
    void *(*FUNC_construct)(),
    Error (*FUNC_destruct)(Logger *self),
    LoggerConfig *(*FUNC_subscribe)(
        Logger *self,
        void   *log_object,
        void (*FUNC_log)(
            void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority)),
    LoggerConfig *(*FUNC_subscribe_config)(
        Logger       *self,
        LoggerConfig *config,
        void (*FUNC_log)(
            void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority)),
    void (*FUNC_close_flow)(Logger *self),
    void (*FUNC_open_flow)(Logger *self),
    void (*FUNC_log)(Logger *self, LOG_PRIORITY level, char *format, ...))
{
    Logger *logger = (Logger *)calloc(1, sizeof(Logger));
    if (logger == NULL)
    {
        goto return_error;
    }

    logger->logger = FUNC_construct();
    if (logger->logger == NULL)
    {
        goto return_error;
    }

    logger->FUNC_destruct         = FUNC_destruct;
    logger->FUNC_subscribe        = FUNC_subscribe;
    logger->FUNC_subscribe_config = FUNC_subscribe_config;
    logger->FUNC_close_flow       = FUNC_close_flow;
    logger->FUNC_open_flow        = FUNC_open_flow;
    logger->FUNC_log              = FUNC_log;

    return logger;

return_error:
    if (logger != NULL)
    {
        free(logger);
    }

    return NULL;
}
