/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:  01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "../OO_Dto/dto.h"
#include "../OO_Error/oo_Error.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

typedef struct Equation                Equation;
typedef struct EquationDiscoveryMethod EquationDiscoveryMethod;
typedef struct EquationTerm            EquationTerm;
typedef struct Frame                   Frame;
typedef struct Logger                  Logger;
typedef struct Matrix                  Matrix;
typedef struct MeetingPointMethod      MeetingPointMethod;

struct Equation
{
    void *equation;

    /*!
        \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

        \param self Object that invoked the method.
        \param variable Signal to which axis the function return value is.
        \param value Constant value of the left term of an equation. 

        \note Upon any errors returns NULL.
    */
    Equation *(*FUNC_new)(Equation *self, EquationVariableLabel dependent_variable);

    /*!
        \brief Completely destruct the invoker interface and object memory allocations.

        \param self Object that invoked the method.
    */
    Error (*FUNC_destruct)(Equation *self);

    Error (*FUNC_replace_dependent_variable)(
        Equation *self, EquationVariableLabel dependent_variable);

    EquationVariableLabel (*FUNC_dependent_variable)(Equation *self);

    /*!
        \brief Destruct all the EquationTerm referenced by the invoker and set quantity to 0. 

        \param self Object that invoked the method.
    */
    Error (*FUNC_reset)(Equation *self);

    /*!
        \brief Make a copy of the invoker.

        \param self Object that invoked the method. Will have its content copied into destiny.
        \param destiny Object that will recieve the copy.
    */
    Error (*FUNC_copy_into)(Equation *self, Equation *destiny);

    /*!
        \brief Print the equation in a stylized format.

        \param self Object that invoked the method.
    */
    Error (*FUNC_print)(Equation *self);

    /*!
        \brief Agregate compatible terms.

        \param self Object that invoked the method.

        \warning Process will always target self, beeing destructive towards the original data.
    */
    Error (*FUNC_aggregate)(Equation *self);

    /*!
        \brief Ordenate the array of EquationTerm by the term type then term core.

        \param self Object that invoked the method.

        \warning Process will always target self, beeing destructive towards the original data.
    */
    Error (*FUNC_ordenate)(Equation *self);

    /*!
        \brief Add term to the equation behaviour.

        \param self Object that invoked the method.
        \param term Object containing a piece of equation behaviour.
    */
    Error (*FUNC_add_term)(Equation *self, EquationTerm *term);

    /*!
        \brief Extract the terms existent at invoker.

        \param self Object that invoked the method.
        \param terms Pointer to an array of EquationTerm objects. The array will hold all terms extracted.
        \param quantity Pointer to a variable that will signal how much terms were extracted.
    */
    Error (*FUNC_get_terms)(Equation *self, EquationTerm *(*terms[]), uint16_t *quantity);

    /*!
        \brief Use a EquationDiscoveryMethod interface to find the most aproximate equation to the given collection of points.

        \param self Object that invoked the method. One of the equations to be analyzed.
        \param method Object that implements a method of finding a curve equation from a collection of points.
        \param frame Object that hold the collection of points.
    */
    Error (*FUNC_find_equation)(
        Equation *self, Frame *frame, EquationDiscoveryMethod *method);

    /*!
        \brief Calculate a single point of the equation.

        \param self Object that invoked the method.
        \param point Pointer to DTO that will hold the result of the calculation.
        \param giving Confirmation of the axis you expect to be used for calculation.
    */
    Error (*FUNC_calculate)(
        Equation        *self,
        EquationVariable variables[],
        uint16_t         variables_quantity,
        float           *result);

    /*!
        \brief Use a MeetingPointMethod interface to find the meeting points between two curve equations.

        \param self Object that invoked the method. One of the equations to be analyzed.
        \param equation Object contains one of the equations to be analyzed.
        \param method Object that implements a method of finding curve meeting points.
        \param frame Object that will hold the collection of meeting points found by method.
    */
    Error (*FUNC_meeting_point)(
        Equation *self, Equation *equation, MeetingPointMethod *method, Frame *frame);

    /*!
        \brief Calculate the integral of the equation, therefore, calculate the integral of all the terms of the equation.

        \param self Object that invoked the method.
        \param destiy Object that will recieve the integral result.
    */
    Error (*FUNC_integrate)(Equation *self, Equation *destiny);

    /*!
        \brief Calculate the derivative of the equation, therefore, calculate the derivative of all the terms of the equation.

        \param self Object that invoked the method.
        \param destiy Object that will recieve the derivative result.
    */
    Error (*FUNC_derivative)(Equation *self, Equation *destiny);

    Error (*FUNC_power)(Equation *self, Equation *destiny, float power);
};

struct EquationDiscoveryMethod
{
    void *method;

    /*!
        \brief Completely destruct the invoker interface and object memory allocations.

        \param self Object that invoked the method.
    */
    Error (*FUNC_destruct)(EquationDiscoveryMethod *self);

    /*!
        \brief Will execute least squares numeric method to discover a equation from a collection of points.

        \param self Object that invoked the method.
        \param frame Object containing the collection of points to be analized by the method.
        \param equation Object that will hold the resultant equation that the method discover.

        \warning equation needs to have prepared terms pre-execution, so the least squares method knows what is expected as result.
    */
    Error (*FUNC_equation)(
        EquationDiscoveryMethod *self, Frame *frame, Equation *equation);
};

struct EquationTerm
{
    void *term;

    /*!
        \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

        \param self Object that invoked the method.

        \note Upon any errors returns NULL.
    */
    EquationTerm *(*FUNC_new)(EquationVariableLabel variable_label, EquationTerm *self);

    /*!
        \brief Completely destruct the invoker interface and object memory allocations.

        \param self Object that invoked the method.
    */
    Error (*FUNC_destruct)(EquationTerm *self);

    /*!
        \brief Will modify completly the invoker behaviour.

        \param self Object that invoked the method.
        \param core Is the main or fundamental part of the term.
        \param coeficient It is the scalar that multiply the variable.
    */
    Error (*FUNC_change)(EquationTerm *self, float core, float coeficient);

    /*!
        \brief Make a copy of the invoker.

        \param self Object that invoked the method. Will have its content copied into destiny.
        \param destiny Pointer to the Object that will recieve the copy.
    */
    Error (*FUNC_copy_into)(EquationTerm *self, EquationTerm **destiny);

    /*!
        \brief Print the term in a stylized format.

        \param self Object that invoked the method.
    */
    Error (*FUNC_print)(EquationTerm *self);

    /*!
        \brief Agregate two equal typed and cored terms if they have same AXIS variable reference.

        \param self Object that invoked the method.
        \param term Object that will be agregated by self.

        \warning Returned error should be treated carefully. There is nothing wrong having two different cored terms, but will be impossible to 
            agregate them, even so, the method will return an error sinalizing the failure of the agregation.
    */
    Error (*FUNC_aggregate)(EquationTerm *self, EquationTerm *term);

    /*!
        \brief Getter for the EquationTerm concrete implementation type.

        \param self Object that invoked the method.

        \note Used to differentiate concrete implementations of the EquationTerm interface.
    */
    EQUATION_TERM (*FUNC_type)(EquationTerm *self);

    /*!
        \brief Getter for the core of the term.

        \param self Object that invoked the method.
    */
    float (*FUNC_core)(EquationTerm *self);

    /*!
        \brief Getter for the coeficient of the term.

        \param self Object that invoked the method.
    */
    float (*FUNC_coeficient)(EquationTerm *self);

    EquationVariableLabel (*FUNC_variable)(EquationTerm *self);

    /*!
        \brief Calculate the value of the term relative to a given axis value.

        \param self Object that invoked the method.
        \param point DTO that hold cartesian coordinates.
        \param givin Enum that mark what coordinates will be used from point to calculate the term.
        \param result Pointer to the variable that will hold the calculation result.
    */
    Error (*FUNC_calculate)(EquationTerm *self, EquationVariable variable, float *result);

    /*!
        \brief Calculate the integral of the term.

        \param self Object that invoked the method.

        \warning Process will always target self, beeing destructive towards the original data.
    */
    Error (*FUNC_integrate)(EquationTerm *self);

    /*!
        \brief Calculate the derivative of the term.

        \param self Object that invoked the method.

        \warning Process will always target self, beeing destructive towards the original data.
    */
    Error (*FUNC_derivative)(EquationTerm *self);

    /*!
        \brief Multiply two terms.

        \param self Object that invoked the method, will be multiplied by term.
        \param term Object that will be multiplied by self.
        \param destiny Object that will hold the result of the multiplication.
    */
    Error (*FUNC_multiply)(EquationTerm *self, EquationTerm *term, EquationTerm *destiny);

    /*!
        \brief Divide two terms.

        \param self Object that invoked the method, will be divided by term.
        \param term Object that will be divided by self.
        \param destiny Object that will hold the result of the division.
    */
    Error (*FUNC_divide)(EquationTerm *self, EquationTerm *term, EquationTerm *destiny);
};

struct Frame
{
    void *frame;

    /*!
        \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

        \param self Object that invoked the method.
        
        \note Upon any errors returns NULL.
    */
    Frame *(*FUNC_new)(Frame *self, uint32_t size);

    /*!
        \brief Completely destruct the invoker interface and object memory allocations.

        \param self Object that invoked the method.
    */
    Error (*FUNC_destruct)(Frame *self);

    /*!
        \brief Clean the frame by setting its quantity to 0.

        \param self Object that invoked the method.

        \warning Process will always target self, beeing destructive towards the original data.
    */
    Error (*FUNC_clean)(Frame *self);

    /*!
        \brief Allocates the necessary memory for the new matrix size.

        \param self Object that invoked the method.
        \param size Quantity of positions that frame should have.

        \warning Process will always target self, beeing destructive towards the original data.

        \note Updates point quantity to 0.
    */
    Error (*FUNC_resize)(Frame *self, uint32_t size);

    /*!
        \brief Free all allocated positions, safely preparing Frame to recieve new allocations.

        \param self Object that invoked the method.

        \warning Process will always target self, beeing destructive towards the original data.

        \note Update point quantity to 0.
    */
    Error (*FUNC_reset)(Frame *self);

    /*!
        \brief Make a copy of the invoker.

        \param self Object that invoked the method.
        \param destiny Object that will recieve the copy.
    */
    Error (*FUNC_copy_into)(Frame *self, Frame *destiny);

    /*!
        \brief Copy an interval of the invoker into destiny, preserving any previously data present at destiny.

        \param self Object that invoked the method.
        \param destiny Object that will recieve the copy.
        \param start Starting position of self to copy into destiny.
        \param end Final position of self to copy into destiny.

        \warning If you wish destiny to be composed only of the start~end interval, you must enforce a clean destiny is passed as argument.
    */
    Error (*FUNC_copy_interval)(
        Frame *self, Frame *destiny, uint32_t start, uint32_t end);

    /*!
        \brief Print the frame in a stylized format.

        \param self Object that invoked the method.
    */
    Error (*FUNC_print)(Frame *self);

    /*!
        \brief Getter for the size of the frame.

        \param self Object that invoked the method.
    */
    uint32_t (*FUNC_size)(Frame *self);

    /*!
        \brief Getter for the quantity of points at the frame.

        \param self Object that invoked the method.
    */
    uint32_t (*FUNC_quantity)(Frame *self);

    /*!
        \brief Resize and fill the matrix with an representation of the frame values.

        \param self Object that invoked the method.
        \param matrix Object that will recieve the representation of the frame
    */
    Error (*FUNC_to_matrix)(Frame *self, Matrix *matrix);

    /*!
        \brief Add a point to a previously initialized position.

        \param self Object that invoked the method.
        \param point Value to be inserted into the frame.
        \param position Position of the frame to be pushed into.

        \warning Process will always target self, beeing destructive towards the original data.
        \warning Push into uninitialized position will lead to an error.

        \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
    */
    Error (*FUNC_position_push_cartesian)(
        Frame *self, CoordinatesCartesian point, uint32_t position);

    /*!
        \brief Add a point to a previously initialized position.

        \param self Object that invoked the method.
        \param point Value to be inserted into the frame.
        \param position Position of the frame to be pushed into.

        \warning Process will always target self, beeing destructive towards the original data.
        \warning Push into uninitialized position will lead to an error.

        \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
    */
    Error (*FUNC_position_push_spherical)(
        Frame *self, CoordinatesSpherical point, uint32_t position);

    /*!
        \brief Add a point to the next uninitialized position.

        \param self Object that invoked the method.
        \param point Value to be inserted into the frame.

        \warning Process will always target self, beeing destructive towards the original data.

        \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
    */
    Error (*FUNC_push_cartesian)(Frame *self, CoordinatesCartesian point);

    /*!
        \brief Add a point to the next uninitialized position.

        \param self Object that invoked the method.
        \param point Value to be inserted into the frame.

        \warning Process will always target self, beeing destructive towards the original data.

        \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
    */
    Error (*FUNC_push_spherical)(Frame *self, CoordinatesSpherical point);

    /*!
        \brief Extract a point from the frame at requested position.

        \param self Object that invoked the method.
        \param point Pointer to the variable that will hold the extracted data.
        \param position Position of the value.
    */
    Error (*FUNC_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, uint32_t position);

    /*!
        \brief Extract a point from the frame at requested position.

        \param self Object that invoked the method.
        \param point Pointer to the variable that will hold the extracted data.
        \param position Position of the value.
    */
    Error (*FUNC_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, uint32_t position);

    /*!
        \brief Extract the point with the hisghest value relative to axis.

        \param self Object that invoked the method.
        \param point Pointer to the variable that will hold the extracted data.
        \param axis Axis that must be highest of the frame.
    */
    Error (*FUNC_highest_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, AXIS axis);

    /*!
        \brief Extract the point with the hisghest value relative to axis.

        \param self Object that invoked the method.
        \param point Pointer to the variable that will hold the extracted data.
        \param axis Axis that must be highest of the frame.
    */
    Error (*FUNC_highest_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, AXIS axis);

    /*!
        \brief Extract the point with the lowest value relative to axis.

        \param self Object that invoked the method.
        \param point Pointer to the variable that will hold the extracted data.
        \param axis Axis that must be lowest of the frame.
    */
    Error (*FUNC_lowest_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, AXIS axis);

    /*!
        \brief Extract the point with the lowest value relative to axis.

        \param self Object that invoked the method.
        \param point Pointer to the variable that will hold the extracted data.
        \param axis Axis that must be lowest of the frame.
    */
    Error (*FUNC_lowest_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, AXIS axis);

    /*!
        \brief Move all points of frame respectively to parameters.

        \param self Object that invoked the method.
        \param x_axis Distance that will be moved relative to x axis.
        \param y_axis Distance that will be moved relative to y axis.
        \param z_axis Distance that will be moved relative to z axis.
    */
    Error (*FUNC_move_points)(Frame *self, float x_axis, float y_axis, float z_axis);

    /*!
        \brief Trade the cartesian coordinates of axis a and axis b for every point of the frame.

        \param self Object that invoked the method.
        \param a Axis that will be trade position with b.
        \param b Axis that will be trade position with a.
    */
    Error (*FUNC_trade_axis)(Frame *self, AXIS a, AXIS b);

    /*!
        \brief Change the signal of the every point of the frame based on axis and signal arguments.

        \param self Object that invoked the method.
        \param axis Axis that will recieve any changes.
        \param signal Flag to sinalize the inversion of points that are on that signal side of the axis.

        \note signal beeing POSITIVE sends all points to positive counterpart of the axis.
        \note signal beeing NEGATIVE sends all points to negative counterpart of the axis.
        \note signal beeing NEUTRAL invert the axis signal of all points.
    */
    Error (*FUNC_mirror_axis)(Frame *self, AXIS axis, SIGNAL signal);
};

struct Logger
{
    void *logger;

    /*!
        \brief Completely destruct the invoker interface and object memory allocations.

        \param self Object that invoked the method.
    */
    Error (*FUNC_destruct)(Logger *self);

    /*!
        \brief Subscribe a function to be potential call when logging.

        \param self Object that invoked the method.
        \param log_object Object that logger will link to self parameter of FUNC_log. In case of non object oriented function subscribe, it is safe to pass NULL.
        \param FUNC_log Pointer to function that will be called by the logger.

        \return Pointer to DTO that hold the configurations of the subscribed function.
            Modifying its values will affect the behavior between Logger and FUNC_log.

        \note Safe to modify returned value at runtime.
    */
    LoggerConfig *(*FUNC_subscribe)(
        Logger *self,
        void   *log_object,
        void (*FUNC_log)(
            void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority));

    /*!
    \brief Subscribe a function to be potential call when logging.

    \attention LoggerEnlist is bound to have the #define LOGGER_ENLIST_SIZE as maximum subscribed functions quantity. This should 
        be improved to dynamic size in the future.

    \param self Object that invoked the method.
    \param config Pointer to already existent configuration, it will be referenced by the logger.
    \param FUNC_log Pointer to function that will be called by the logger.

    \return Pointer to DTO that hold the configurations of the subscribed function.
        Modifying its values will affect the behavior between Logger and FUNC_log.

    \note Safe to modify returned value at runtime.
*/
    LoggerConfig *(*FUNC_subscribe_config)(
        Logger       *self,
        LoggerConfig *config,
        void (*FUNC_log)(
            void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority));

    void (*FUNC_close_flow)(Logger *self);
    void (*FUNC_open_flow)(Logger *self);

    /*!
        \brief Log via all subscribed functions that are active and meets the priority of the log.

        \param self Object that invoked the method.
        \param priority Level of priority of the log.
        \param message String that will be logged.
    */
    void (*FUNC_log)(Logger *self, LOG_PRIORITY level, char *format, ...);
};

struct Matrix
{
    void *matrix;

    /*!
        \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

        \param self Object that invoked the method.
        \param rows Quantity of rows that matrix should have.
        \param collumns Quantity of collumns that matrix should have. 

        \note Upon any errors returns NULL.
    */
    Matrix *(*FUNC_new)(Matrix *self, uint32_t rows, uint32_t collumns);

    /*!
        \brief Completely destruct the invoker interface and object memory allocations.

        \param self Object that invoked the method.
    */
    Error (*FUNC_destruct)(Matrix *self);

    /*!
        \brief Clean the matrix by inserting 0 in all positions.

        \param self Object that invoked the method.
    */
    Error (*FUNC_clean)(Matrix *self);

    /*!
        \brief Allocates the necessary memory for the new matrix size.

        \param self Object that invoked the method.
        \param rows Quantity of rows that matrix should have.
        \param collumns Quantity of collumns that matrix should have. 
    */
    Error (*FUNC_resize)(Matrix *self, uint32_t rows, uint32_t collumns);

    /*!
        \brief Free all allocated positions, safely preparing Matrix to recieve new allocations.

        \param self Object that invoked the method.
    */
    Error (*FUNC_reset)(Matrix *self);

    /*!
        \brief Make a copy of the invoker.

        \param self Object that invoked the method.
        \param destiny Object that will recieve the copy.

        \note Update rows and collumns quantity to 0.
    */
    Error (*FUNC_copy_into)(Matrix *self, Matrix *destiny);

    /*!
        \brief Print the matrix in a stylized format.

        \param self Object that invoked the method.
    */
    Error (*FUNC_print)(Matrix *self);

    /*!
        \brief Getter for the rows quantity of the matrix.

        \param self Object that invoked the method.
    */
    uint32_t (*FUNC_rows)(Matrix *self);

    /*!
        \brief Getter for the collumns quantity of the matrix.

        \param self Object that invoked the method.
    */
    uint32_t (*FUNC_collumns)(Matrix *self);

    /*!
        \brief Getter for the order of the matrix.

        \param self Object that invoked the method.

        \note Order is an aspect of square matrices, 0 signal that it is not a square matrix.
    */
    uint32_t (*FUNC_order)(Matrix *self);

    /*!
        \brief Add a value to a position.

        \param self Object that invoked the method.
        \param value Value to be inserted into the matrix.
        \param row Desired row.
        \param collumn Desired collumn.
    */
    Error (*FUNC_push)(Matrix *self, float value, uint32_t row, uint32_t collumn);

    /*!
        \brief Extract a value from position.

        \param self Object that invoked the method.
        \param value Pointer to the variable that will hold the extracted value.
        \param row Desired row.
        \param collumn Desired collumn.
    */
    Error (*FUNC_pull)(Matrix *self, float *value, uint32_t row, uint32_t collumn);

    /*!
        \brief Multiply the matrix by a scalar number.

        \param self Object that invoked the method.
        \param scalar Value to be multiplied.
    */
    Error (*FUNC_multiply_scalar)(Matrix *self, float scalar);

    /*!
        \brief Multiply two matrices, always self X multiplier.

        \param self Object that invoked the method.
        \param multiplier Object that will be multiplied.
        \param destiny Object that will hold the result of the multiplication.
    */
    Error (*FUNC_multiply_matrix)(Matrix *self, Matrix *multiplier, Matrix *destiny);

    /*!
        \brief Calculated the determinant of the matrix.

        \param self Object that invoked the method.
        \param determinant Pointer to the variable that will hold the value of the determinant.

        \note Determinant is an aspect of square matrices, any non square matrices will return an error.
    */
    Error (*FUNC_determinant)(Matrix *self, float *determinant);

    /*!
        \brief Transpose the matrix, sizing a ixj into a jxi.

        \param self Object that invoked the method.
        \param destiny Object that will hold the result of the multiplication.
    */
    Error (*FUNC_transposed)(Matrix *self, Matrix *destiny);

    /*!
        \brief Produce the matrix with all the fields with its calculated minorized values.

        \param self Object that invoked the method.
        \param destiny Object that will hold the result of the multiplication.
    */
    Error (*FUNC_minorized)(Matrix *self, Matrix *destiny);

    /*!
        \brief Multiply each field of the matrix by the universal place sign(1 or -1), inverting the signal of multiple fields.

        \param self Object that invoked the method.
        \param destiny Object that will hold the result of the multiplication.
    */
    Error (*FUNC_place_signify)(Matrix *self);

    /*!
        \brief Produce the cofactor matrix, that is the combination of its minorized matrix and place sign process.

        \param self Object that invoked the method.
        \param destiny Object that will hold the result of the multiplication.
    */
    Error (*FUNC_cofactor)(Matrix *self, Matrix *destiny);

    /*!
        \brief Produce the adjoint matrix, that is the multiplication of its transposed matrix and cofactor matrix.

        \param self Object that invoked the method.
        \param destiny Object that will hold the result of the multiplication.
    */
    Error (*FUNC_adjoint)(Matrix *self, Matrix *destiny);

    /*!
        \brief Produce the inverse matrix, that is the multiplication of its inversed determinant scalar and the adjoint matrix.

        \param self Object that invoked the method.
        \param destiny Object that will hold the result of the multiplication.
    */
    Error (*FUNC_inverse)(Matrix *self, Matrix *destiny);
};

struct MeetingPointMethod
{
    void *method;

    /*!
        \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

        \note Upon any errors returns NULL.
    */

    /*!
        \brief Completely destruct the invoker interface and object memory allocations.

        \param self Object that invoked the method.
    */
    Error (*FUNC_destruct)(MeetingPointMethod *self);

    /*!
        \brief Find the meeting points between two curve equations.

        \param self Object that invoked the method.
        \param frame Object that will hold the collection of meeting points found by method.
        \param equation1 Object contains one of the equations to be analyzed.
        \param equation2 Object contains one of the equations to be analyzed.
    */
    Error (*FUNC_meeting_point)(
        MeetingPointMethod *self, Frame *frame, Equation *equation1, Equation *equation2);
};

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif
