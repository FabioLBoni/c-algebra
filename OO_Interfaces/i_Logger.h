/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    23 de jan de 2024
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include "interfaces.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

Logger *construct_logger(
    void *(*FUNC_construct)(),
    Error (*FUNC_destruct)(Logger *self),
    LoggerConfig *(*FUNC_subscribe)(
        Logger *self,
        void   *log_object,
        void (*FUNC_log)(
            void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority)),
    LoggerConfig *(*FUNC_subscribe_config)(
        Logger       *self,
        LoggerConfig *config,
        void (*FUNC_log)(
            void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority)),
    void (*FUNC_close_flow)(Logger *self),
    void (*FUNC_open_flow)(Logger *self),
    void (*FUNC_log)(Logger *self, LOG_PRIORITY level, char *format, ...));

#ifdef __cplusplus /* If this is a C++ compiler, end C linkage */
}
#endif