/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    14 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <stdlib.h>

#include "i_MeetingPointMethod.h"

MeetingPointMethod *construct_meeting_point_method(
    void *(*FUNC_construct)(),
    Error (*FUNC_destruct)(MeetingPointMethod *self),
    Error (*FUNC_meeting_point)(
        MeetingPointMethod *self, Frame *frame, Equation *equation1, Equation *equation2))
{
    MeetingPointMethod *meetingPointMethod =
        (MeetingPointMethod *)calloc(1, sizeof(MeetingPointMethod));
    if (meetingPointMethod == NULL)
    {
        goto return_error;
    }

    meetingPointMethod->method = FUNC_construct();
    if (meetingPointMethod->method == NULL)
    {
        goto return_error;
    }

    meetingPointMethod->FUNC_destruct = FUNC_destruct;

    meetingPointMethod->FUNC_meeting_point = FUNC_meeting_point;

    return meetingPointMethod;

return_error:
    if (meetingPointMethod != NULL)
    {
        free(meetingPointMethod);
    }

    return NULL;
}
