/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include "interfaces.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

Matrix *construct_matrix(
    void *(*FUNC_contruct)(),
    Error (*FUNC_destruct)(Matrix *self),
    Matrix *(*FUNC_new)(Matrix *self, uint32_t rows, uint32_t collumns),
    Error (*FUNC_clean)(Matrix *self),
    Error (*FUNC_resize)(Matrix *self, uint32_t rows, uint32_t collumns),
    Error (*FUNC_reset)(Matrix *self),
    Error (*FUNC_copy_into)(Matrix *self, Matrix *destiny),
    Error (*FUNC_print)(Matrix *self),
    uint32_t (*FUNC_rows)(Matrix *self),
    uint32_t (*FUNC_collumns)(Matrix *self),
    uint32_t (*FUNC_order)(Matrix *self),
    Error (*FUNC_push)(Matrix *self, float value, uint32_t row, uint32_t collumn),
    Error (*FUNC_pull)(Matrix *self, float *value, uint32_t row, uint32_t collumn),
    Error (*FUNC_multiply_scalar)(Matrix *self, float scalar),
    Error (*FUNC_multiply_matrix)(Matrix *self, Matrix *multiplier, Matrix *destiny),
    Error (*FUNC_determinant)(Matrix *self, float *determinant),
    Error (*FUNC_transposed)(Matrix *self, Matrix *destiny),
    Error (*FUNC_minorized)(Matrix *self, Matrix *destiny),
    Error (*FUNC_place_signify)(Matrix *self),
    Error (*FUNC_cofactor)(Matrix *self, Matrix *destiny),
    Error (*FUNC_adjoint)(Matrix *self, Matrix *destiny),
    Error (*FUNC_inverse)(Matrix *self, Matrix *destiny));

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif