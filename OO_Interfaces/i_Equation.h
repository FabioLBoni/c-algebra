/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include "interfaces.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

Equation *construct_equation(
    void *(*FUNC_construct)(),
    Equation *(*FUNC_new)(Equation *self, EquationVariableLabel dependent_variable),
    Error (*FUNC_destruct)(Equation *self),
    Error (*FUNC_replace_dependent_variable)(
        Equation *self, EquationVariableLabel dependent_variable),
    EquationVariableLabel (*FUNC_dependent_variable)(Equation *self),
    Error (*FUNC_reset)(Equation *self),
    Error (*FUNC_copy_into)(Equation *self, Equation *destiny),
    Error (*FUNC_print)(Equation *self),
    Error (*FUNC_aggregate)(Equation *self),
    Error (*FUNC_ordenate)(Equation *self),
    Error (*FUNC_add_term)(Equation *self, EquationTerm *term),
    Error (*FUNC_get_terms)(Equation *self, EquationTerm *(*terms[]), uint16_t *quantity),
    Error (*FUNC_find_equation)(
        Equation *self, Frame *frame, EquationDiscoveryMethod *method),
    Error (*FUNC_calculate)(
        Equation        *self,
        EquationVariable variables[],
        uint16_t         variables_quantity,
        float           *result),
    Error (*FUNC_meeting_point)(
        Equation *self, Equation *equation, MeetingPointMethod *method, Frame *frame),
    Error (*FUNC_integrate)(Equation *self, Equation *destiny),
    Error (*FUNC_derivative)(Equation *self, Equation *destiny),
    Error (*FUNC_power)(Equation *self, Equation *destiny, float power));

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif
