/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <stdlib.h>

#include "i_Equation.h"

Equation *construct_equation(
    void *(*FUNC_construct)(),
    Equation *(*FUNC_new)(Equation *self, EquationVariableLabel dependent_variable),
    Error (*FUNC_destruct)(Equation *self),
    Error (*FUNC_replace_dependent_variable)(
        Equation *self, EquationVariableLabel dependent_variable),
    EquationVariableLabel (*FUNC_dependent_variable)(Equation *self),
    Error (*FUNC_reset)(Equation *self),
    Error (*FUNC_copy_into)(Equation *self, Equation *destiny),
    Error (*FUNC_print)(Equation *self),
    Error (*FUNC_aggregate)(Equation *self),
    Error (*FUNC_ordenate)(Equation *self),
    Error (*FUNC_add_term)(Equation *self, EquationTerm *term),
    Error (*FUNC_get_terms)(Equation *self, EquationTerm *(*terms[]), uint16_t *quantity),
    Error (*FUNC_find_equation)(
        Equation *self, Frame *frame, EquationDiscoveryMethod *method),
    Error (*FUNC_calculate)(
        Equation        *self,
        EquationVariable variables[],
        uint16_t         variables_quantity,
        float           *result),
    Error (*FUNC_meeting_point)(
        Equation *self, Equation *equation, MeetingPointMethod *method, Frame *frame),
    Error (*FUNC_integrate)(Equation *self, Equation *destiny),
    Error (*FUNC_derivative)(Equation *self, Equation *destiny),
    Error (*FUNC_power)(Equation *self, Equation *destiny, float power))
{
    Equation *equation = (Equation *)calloc(1, sizeof(Equation));
    if (equation == NULL)
    {
        goto return_error;
    }

    equation->equation = FUNC_construct();
    if (equation->equation == NULL)
    {
        goto return_error;
    }

    equation->FUNC_new      = FUNC_new;
    equation->FUNC_destruct = FUNC_destruct;

    equation->FUNC_replace_dependent_variable = FUNC_replace_dependent_variable;
    equation->FUNC_dependent_variable         = FUNC_dependent_variable;

    equation->FUNC_reset     = FUNC_reset;
    equation->FUNC_copy_into = FUNC_copy_into;
    equation->FUNC_print     = FUNC_print;

    equation->FUNC_aggregate = FUNC_aggregate;
    equation->FUNC_ordenate  = FUNC_ordenate;

    equation->FUNC_find_equation = FUNC_find_equation;

    equation->FUNC_add_term  = FUNC_add_term;
    equation->FUNC_get_terms = FUNC_get_terms;

    equation->FUNC_calculate     = FUNC_calculate;
    equation->FUNC_meeting_point = FUNC_meeting_point;

    equation->FUNC_integrate  = FUNC_integrate;
    equation->FUNC_derivative = FUNC_derivative;
    equation->FUNC_power      = FUNC_power;

    return equation;

return_error:
    if (equation != NULL)
    {
        free(equation);
    }

    return NULL;
}
