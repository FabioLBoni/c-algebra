/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    14 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <stdlib.h>

#include "i_EquationTerm.h"

EquationTerm *construct_equation_term(
    void *(*FUNC_construct)(),
    EquationTerm *(*FUNC_new)(EquationVariableLabel variable_label, EquationTerm *self),
    Error (*FUNC_destruct)(EquationTerm *self),
    Error (*FUNC_change)(EquationTerm *self, float core, float coeficient),
    Error (*FUNC_copy_into)(EquationTerm *self, EquationTerm **destiny),
    Error (*FUNC_print)(EquationTerm *self),
    Error (*FUNC_aggregate)(EquationTerm *self, EquationTerm *term),
    EQUATION_TERM (*FUNC_type)(EquationTerm *self),
    float (*FUNC_core)(EquationTerm *self),
    float (*FUNC_coeficient)(EquationTerm *self),
    EquationVariableLabel (*FUNC_variable)(EquationTerm *self),
    Error (*FUNC_calculate)(EquationTerm *self, EquationVariable variable, float *result),
    Error (*FUNC_integrate)(EquationTerm *self),
    Error (*FUNC_derivative)(EquationTerm *self),
    Error (*FUNC_multiply)(EquationTerm *self, EquationTerm *term, EquationTerm *destiny),
    Error (*FUNC_divide)(EquationTerm *self, EquationTerm *term, EquationTerm *destiny))
{
    EquationTerm *equationTerm = (EquationTerm *)calloc(1, sizeof(EquationTerm));
    if (equationTerm == NULL)
    {
        goto return_error;
    }

    equationTerm->term = FUNC_construct();
    if (equationTerm->term == NULL)
    {
        goto return_error;
    }

    equationTerm->FUNC_new = FUNC_new;

    equationTerm->FUNC_destruct = FUNC_destruct;

    equationTerm->FUNC_variable = FUNC_variable;
    equationTerm->FUNC_change   = FUNC_change;

    equationTerm->FUNC_copy_into = FUNC_copy_into;
    equationTerm->FUNC_print     = FUNC_print;

    equationTerm->FUNC_aggregate = FUNC_aggregate;

    equationTerm->FUNC_type = FUNC_type;

    equationTerm->FUNC_core       = FUNC_core;
    equationTerm->FUNC_coeficient = FUNC_coeficient;

    equationTerm->FUNC_calculate = FUNC_calculate;

    equationTerm->FUNC_integrate  = FUNC_integrate;
    equationTerm->FUNC_derivative = FUNC_derivative;
    equationTerm->FUNC_multiply   = FUNC_multiply;
    equationTerm->FUNC_divide     = FUNC_divide;

    return equationTerm;

return_error:
    if (equationTerm != NULL)
    {
        free(equationTerm);
    }

    return NULL;
}