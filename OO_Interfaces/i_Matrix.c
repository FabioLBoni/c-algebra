/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <stdlib.h>

#include "i_Matrix.h"

Matrix *construct_matrix(
    void *(*FUNC_contruct)(),
    Error (*FUNC_destruct)(Matrix *self),
    Matrix *(*FUNC_new)(Matrix *self, uint32_t rows, uint32_t collumns),
    Error (*FUNC_clean)(Matrix *self),
    Error (*FUNC_resize)(Matrix *self, uint32_t rows, uint32_t collumns),
    Error (*FUNC_reset)(Matrix *self),
    Error (*FUNC_copy_into)(Matrix *self, Matrix *destiny),
    Error (*FUNC_print)(Matrix *self),
    uint32_t (*FUNC_rows)(Matrix *self),
    uint32_t (*FUNC_collumns)(Matrix *self),
    uint32_t (*FUNC_order)(Matrix *self),
    Error (*FUNC_push)(Matrix *self, float value, uint32_t row, uint32_t collumn),
    Error (*FUNC_pull)(Matrix *self, float *value, uint32_t row, uint32_t collumn),
    Error (*FUNC_multiply_scalar)(Matrix *self, float scalar),
    Error (*FUNC_multiply_matrix)(Matrix *self, Matrix *multiplier, Matrix *destiny),
    Error (*FUNC_determinant)(Matrix *self, float *determinant),
    Error (*FUNC_transposed)(Matrix *self, Matrix *destiny),
    Error (*FUNC_minorized)(Matrix *self, Matrix *destiny),
    Error (*FUNC_place_signify)(Matrix *self),
    Error (*FUNC_cofactor)(Matrix *self, Matrix *destiny),
    Error (*FUNC_adjoint)(Matrix *self, Matrix *destiny),
    Error (*FUNC_inverse)(Matrix *self, Matrix *destiny))
{
    Matrix *matrix = (Matrix *)calloc(1, sizeof(Matrix));
    if (matrix == NULL)
    {
        goto return_error;
    }

    matrix->matrix = FUNC_contruct();
    if (matrix->matrix == NULL)
    {
        goto return_error;
    }

    matrix->FUNC_destruct = FUNC_destruct;

    matrix->FUNC_new = FUNC_new;

    matrix->FUNC_clean     = FUNC_clean;
    matrix->FUNC_reset     = FUNC_reset;
    matrix->FUNC_resize    = FUNC_resize;
    matrix->FUNC_copy_into = FUNC_copy_into;

    matrix->FUNC_rows     = FUNC_rows;
    matrix->FUNC_collumns = FUNC_collumns;
    matrix->FUNC_order    = FUNC_order;

    matrix->FUNC_push = FUNC_push;
    matrix->FUNC_pull = FUNC_pull;

    matrix->FUNC_print           = FUNC_print;
    matrix->FUNC_multiply_scalar = FUNC_multiply_scalar;
    matrix->FUNC_multiply_matrix = FUNC_multiply_matrix;
    matrix->FUNC_determinant     = FUNC_determinant;
    matrix->FUNC_transposed      = FUNC_transposed;
    matrix->FUNC_minorized       = FUNC_minorized;
    matrix->FUNC_place_signify   = FUNC_place_signify;
    matrix->FUNC_cofactor        = FUNC_cofactor;
    matrix->FUNC_adjoint         = FUNC_adjoint;
    matrix->FUNC_inverse         = FUNC_inverse;

    return matrix;

return_error:
    if (matrix != NULL)
    {
        free(matrix);
    }

    return NULL;
}
