/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <stdlib.h>

#include "i_EquationDiscoveryMethod.h"

EquationDiscoveryMethod *construct_equation_discovery_method(
    void *(*FUNC_construct)(),
    Error (*FUNC_destruct)(EquationDiscoveryMethod *self),
    Error (*FUNC_equation)(
        EquationDiscoveryMethod *self, Frame *frame, Equation *equation))
{
    EquationDiscoveryMethod *equationDiscoveryMethod =
        (EquationDiscoveryMethod *)calloc(1, sizeof(EquationDiscoveryMethod));
    if (equationDiscoveryMethod == NULL)
    {
        goto return_error;
    }

    equationDiscoveryMethod->method = FUNC_construct();
    if (equationDiscoveryMethod->method == NULL)
    {
        goto return_error;
    }

    equationDiscoveryMethod->FUNC_destruct = FUNC_destruct;

    equationDiscoveryMethod->FUNC_equation = FUNC_equation;

    return equationDiscoveryMethod;

return_error:
    if (equationDiscoveryMethod != NULL)
    {
        free(equationDiscoveryMethod);
    }

    return NULL;
}
