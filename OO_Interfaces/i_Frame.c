/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    08 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <stdlib.h>

#include "i_Frame.h"

Frame *construct_frame(
    void *(*FUNC_contruct)(),
    Error (*FUNC_destruct)(Frame *self),
    Frame *(*FUNC_new)(Frame *self, uint32_t size),
    Error (*FUNC_clean)(Frame *self),
    Error (*FUNC_resize)(Frame *self, uint32_t size),
    Error (*FUNC_reset)(Frame *self),
    Error (*FUNC_copy_into)(Frame *self, Frame *destiny),
    Error (*FUNC_copy_interval)(
        Frame *self, Frame *destiny, uint32_t start, uint32_t end),
    Error (*FUNC_print)(Frame *self),
    uint32_t (*FUNC_size)(Frame *self),
    uint32_t (*FUNC_quantity)(Frame *self),
    Error (*FUNC_to_matrix)(Frame *self, Matrix *matrix),
    Error (*FUNC_position_push_cartesian)(
        Frame *self, CoordinatesCartesian point, uint32_t position),
    Error (*FUNC_position_push_spherical)(
        Frame *self, CoordinatesSpherical point, uint32_t position),
    Error (*FUNC_push_cartesian)(Frame *self, CoordinatesCartesian point),
    Error (*FUNC_push_spherical)(Frame *self, CoordinatesSpherical point),
    Error (*FUNC_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, uint32_t position),
    Error (*FUNC_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, uint32_t position),
    Error (*FUNC_highest_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, AXIS axis),
    Error (*FUNC_highest_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, AXIS axis),
    Error (*FUNC_lowest_pull_cartesian)(
        Frame *self, CoordinatesCartesian *point, AXIS axis),
    Error (*FUNC_lowest_pull_spherical)(
        Frame *self, CoordinatesSpherical *point, AXIS axis),
    Error (*FUNC_move_points)(Frame *self, float x_axis, float y_axis, float z_axis),
    Error (*FUNC_trade_axis)(Frame *self, AXIS a, AXIS b),
    Error (*FUNC_mirror_axis)(Frame *self, AXIS a, SIGNAL signal))
{
    Frame *frame = (Frame *)calloc(1, sizeof(Frame));
    if (frame == NULL)
    {
        goto return_error;
    }

    frame->frame = FUNC_contruct();
    if (frame->frame == NULL)
    {
        goto return_error;
    }

    frame->FUNC_destruct = FUNC_destruct;

    frame->FUNC_new = FUNC_new;

    frame->FUNC_clean  = FUNC_clean;
    frame->FUNC_reset  = FUNC_reset;
    frame->FUNC_resize = FUNC_resize;

    frame->FUNC_copy_into     = FUNC_copy_into;
    frame->FUNC_copy_interval = FUNC_copy_interval;

    frame->FUNC_print     = FUNC_print;
    frame->FUNC_to_matrix = FUNC_to_matrix;

    frame->FUNC_size     = FUNC_size;
    frame->FUNC_quantity = FUNC_quantity;

    frame->FUNC_position_push_cartesian = FUNC_position_push_cartesian;
    frame->FUNC_position_push_spherical = FUNC_position_push_spherical;

    frame->FUNC_push_cartesian = FUNC_push_cartesian;
    frame->FUNC_push_spherical = FUNC_push_spherical;

    frame->FUNC_pull_cartesian = FUNC_pull_cartesian;
    frame->FUNC_pull_spherical = FUNC_pull_spherical;

    frame->FUNC_highest_pull_cartesian = FUNC_highest_pull_cartesian;
    frame->FUNC_highest_pull_spherical = FUNC_highest_pull_spherical;

    frame->FUNC_lowest_pull_cartesian = FUNC_lowest_pull_cartesian;
    frame->FUNC_lowest_pull_spherical = FUNC_lowest_pull_spherical;

    frame->FUNC_move_points = FUNC_move_points;
    frame->FUNC_trade_axis  = FUNC_trade_axis;
    frame->FUNC_mirror_axis = FUNC_mirror_axis;

    return frame;

return_error:
    if (frame != NULL)
    {
        free(frame);
    }

    return NULL;
}
