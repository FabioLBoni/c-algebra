/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    14 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include "interfaces.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

EquationTerm *construct_equation_term(
    void *(*FUNC_construct)(),
    EquationTerm *(*FUNC_new)(EquationVariableLabel variable_label, EquationTerm *self),
    Error (*FUNC_destruct)(EquationTerm *self),
    Error (*FUNC_change)(EquationTerm *self, float core, float coeficient),
    Error (*FUNC_copy_into)(EquationTerm *self, EquationTerm **destiny),
    Error (*FUNC_print)(EquationTerm *self),
    Error (*FUNC_aggregate)(EquationTerm *self, EquationTerm *term),
    EQUATION_TERM (*FUNC_type)(EquationTerm *self),
    float (*FUNC_core)(EquationTerm *self),
    float (*FUNC_coeficient)(EquationTerm *self),
    EquationVariableLabel (*FUNC_variable)(EquationTerm *self),
    Error (*FUNC_calculate)(EquationTerm *self, EquationVariable variable, float *result),
    Error (*FUNC_integrate)(EquationTerm *self),
    Error (*FUNC_derivative)(EquationTerm *self),
    Error (*FUNC_multiply)(EquationTerm *self, EquationTerm *term, EquationTerm *destiny),
    Error (*FUNC_divide)(EquationTerm *self, EquationTerm *term, EquationTerm *destiny));

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif
