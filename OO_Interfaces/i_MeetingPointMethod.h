/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    14 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include "interfaces.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

MeetingPointMethod *construct_meeting_point_method(
    void *(*FUNC_construct)(),
    Error (*FUNC_destruct)(MeetingPointMethod *self),
    Error (*FUNC_meeting_point)(
        MeetingPointMethod *self,
        Frame              *frame,
        Equation           *equation1,
        Equation           *equation2));

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif