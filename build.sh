#!/bin/sh
set -x
gcc -Os "$@" main.c \
    OO_Equation/c_CompositeEquation.c \
    OO_EquationDiscoveryMethod/c_LeastSquares.c \
    OO_EquationTerm/c_CompositeTerm2D.c \
    OO_EquationTerm/c_IntegerPowerTerm2D.c \
    OO_Frame/c_FragFrame.c \
    OO_Logger/c_LoggerEnlist.c \
    OO_Matrix/c_FragMatrix.c \
    OO_MeetingPointMethod/c_NewtonRaphson.c \
    OO_Interfaces/i_Equation.c \
    OO_Interfaces/i_EquationDiscoveryMethod.c \
    OO_Interfaces/i_EquationTerm.c \
    OO_Interfaces/i_Frame.c \
    OO_Interfaces/i_Logger.c \
    OO_Interfaces/i_Matrix.c \
    OO_Interfaces/i_MeetingPointMethod.c \
    OO_Error/oo_Error.c \
    OO_Global/oo_Global.c \
-Wall -o c-algebra -lm 