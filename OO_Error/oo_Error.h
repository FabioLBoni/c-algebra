/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    24 de jul de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include <stdint.h>

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

#define OO_SUCCESS ((Error){.error = OO_OK})

typedef struct ErrorString
{
    char *HEADER_PRODUCED;
    char *HEADER_RECEIVED;

    char *FAILED_MEMORY_ALLOCATION;
    char *NULL_ARGUMENT;
    char *SIZE_OVERFLOW;
    char *OUT_BOUNDARIES;
    char *DIVISION_BY_ZERO;
} ErrorString;
extern const ErrorString ERROR_STRING;

typedef enum OO_ERROR
{
    OO_OK = 0,

    //GENERIC DESCRIPTION ERRORS
    FAILED_MEMORY_ALLOCATION,
    NULL_ARGUMENT,
    SIZE_OVERFLOW,
    OUT_BOUNDARIES,
    DIVISION_BY_ZERO,
    //GENERIC DESCRIPTION ERRORS

    //CUSTOM DESCRIPTION ERRORS
    INVALID_ARGUMENT,
    INVALID_SELF_DATA,
    INVALID_AXIS,
    IMPOSSIBLE_AGGREGATION,
    IMPOSSIBLE_EXECUTION,
    DUPLICATED_AXIS,
    //CUSTOM DESCRIPTION ERRORS

} OO_ERROR;

typedef enum ERROR_FILE_ID
{
    GRID_MATRIX = 1 * 1000000,
    FRAG_MATRIX = 2 * 1000000,

    BLOCK_FRAME = 11 * 1000000,
    FRAG_FRAME  = 12 * 1000000,

    LOGGER_ENLIST = 21 * 1000000,

    COMPOSITE_EQUATION = 31 * 1000000,

    LEAST_SQUARES  = 201 * 1000000,
    NEWTON_RAPHSON = 202 * 1000000,

    COMPOSITE_TERM_2D     = 100 * 1000000,
    INTEGER_POWER_TERM_2D = 101 * 1000000,
    EXPONENTIAL_TERM_2D   = 102 * 1000000,
    LOGARITHIMIC_TERM_2D  = 103 * 1000000,
    TRIGONOMETRIC_TERM_2D = 104 * 1000000,
    RATIONAL_TERM_2D      = 104 * 1000000,
    IRATIONAL_TERM_2D     = 104 * 1000000,
} ERROR_FILE_ID;

typedef struct Error
{
    char    *description;
    OO_ERROR error;
    uint32_t file_line;
} Error;

#ifdef __cplusplus /* If this is a C++ compiler, end C linkage */
}
#endif
