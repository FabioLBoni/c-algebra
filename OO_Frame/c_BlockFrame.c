/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "../OO_Global/oo_Global.h"
#include "c_BlockFrame.h"

static Frame *new(Frame *self, uint32_t size);

static void *construct();
static Error destruct(Frame *self);

static Error clean(Frame *self);
static Error reset(Frame *self);
static Error resize(Frame *self, uint32_t size);

static Error copy_into(Frame *self, Frame *destiny);
static Error copy_interval(Frame *self, Frame *destiny, uint32_t start, uint32_t end);

static uint32_t size(Frame *self);
static uint32_t quantity(Frame *self);

static Error print(Frame *self);
static Error to_matrix(Frame *self, Matrix *matrix);

static Error
position_push_cartesian(Frame *self, CoordinatesCartesian point, uint32_t position);
static Error
position_push_spherical(Frame *self, CoordinatesSpherical point, uint32_t position);

static Error push_cartesian(Frame *self, CoordinatesCartesian point);
static Error push_spherical(Frame *self, CoordinatesSpherical point);

static Error pull_cartesian(Frame *self, CoordinatesCartesian *point, uint32_t position);
static Error pull_spherical(Frame *self, CoordinatesSpherical *point, uint32_t position);

static Error highest_pull_cartesian(Frame *self, CoordinatesCartesian *point, AXIS axis);
static Error highest_pull_spherical(Frame *self, CoordinatesSpherical *point, AXIS axis);

static Error lowest_pull_cartesian(Frame *self, CoordinatesCartesian *point, AXIS axis);
static Error lowest_pull_spherical(Frame *self, CoordinatesSpherical *point, AXIS axis);

static Error move_points(Frame *self, float x_axis, float y_axis, float z_axis);
static Error trade_axis(Frame *self, AXIS a, AXIS b);
static Error mirror_axis(Frame *self, AXIS axis, SIGNAL signal);

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param frame_size Quantity of positions.
    \param logger Optional object to controll the flow of the logs and its priorities.

    \note Upon any errors returns NULL.
*/
Frame *new_block_frame(uint32_t frame_size, Logger *logger)
{
    Error error = {0};

    Frame *frame = construct_frame(
        construct,
        destruct,
        new,
        clean,
        resize,
        reset,
        copy_into,
        copy_interval,
        print,
        size,
        quantity,
        to_matrix,
        position_push_cartesian,
        position_push_spherical,
        push_cartesian,
        push_spherical,
        pull_cartesian,
        pull_spherical,
        highest_pull_cartesian,
        highest_pull_spherical,
        lowest_pull_cartesian,
        lowest_pull_spherical,
        move_points,
        trade_axis,
        mirror_axis);
    if (frame == NULL)
    {
        goto return_error;
    }

    BlockFrame *blockFrame = (BlockFrame *)(frame->frame);

    if (logger != NULL)
    {
        blockFrame->logger   = logger;
        blockFrame->FUNC_log = logger->FUNC_log;
    }
    else
    {
        blockFrame->FUNC_log = simple_logger;
    }

    error = frame->FUNC_resize(frame, frame_size);
    if (error.error != OO_OK)
    {
        blockFrame->FUNC_log(
            blockFrame->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return frame;

return_error:
    if (frame != NULL)
    {
        frame->FUNC_destruct(frame);
    }

    return NULL;
}

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param self Object that invoked the method.
    \param size Quantity of position.

    \note Upon any errors returns NULL.
*/
static Frame *new(Frame *self, uint32_t frame_size)
{
    BlockFrame *blockFrame = (BlockFrame *)(self->frame);

    return new_block_frame(frame_size, blockFrame->logger);
}

/*!
    \brief Allocate any necessary memory and make variable initializations.

    \note Upon any errors returns NULL.
*/
static void *construct()
{
    return calloc(1, sizeof(BlockFrame));
}

/*!
    \brief Completely destruct the invoker interface and object memory allocations.

    \param self Object that invoked the method.
*/
static Error destruct(Frame *self)
{
    if (self != NULL)
    {
        if (self->frame != NULL)
        {
            // BlockFrame *myself = (BlockFrame *)(self->frame);

            reset(self);

            free(self->frame);
            self->frame = NULL;
        }

        free(self);
        self = NULL;
    }

    return OO_SUCCESS;
}

/*!
    \brief Clean the frame by setting its quantity to 0.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error clean(Frame *self)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    myself->quantity = 0;

    return OO_SUCCESS;
}

/*!
    \brief Allocates the necessary memory for the new matrix size.

    \param self Object that invoked the method.
    \param size Quantity of positions that frame should have.

    \warning Process will always target self, beeing destructive towards the original data.

    \note Update point quantity to 0.
*/
static Error resize(Frame *self, uint32_t size)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error error = {0};

    error = reset(self);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    myself->point = (CoordinatesCartesian *)calloc(size, sizeof(CoordinatesCartesian));
    if (myself->point == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    myself->size     = size;
    myself->quantity = 0;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Free all allocated positions, safely preparing BlockFrame to recieve new allocations.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.

    \note Update point quantity to 0.
*/
static Error reset(Frame *self)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    if (myself != NULL)
    {
        if (myself->point != NULL)
        {
            free(myself->point);
            myself->point = NULL;
        }

        myself->quantity = 0;
        myself->size     = 0;
    }

    return OO_SUCCESS;
}

/*!
    \brief Make a copy of the invoker.

    \param self Object that invoked the method.
    \param destiny Object that will recieve the copy.
*/
static Error copy_into(Frame *self, Frame *destiny)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error error = {0};

    if (destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = destiny->FUNC_resize(destiny, myself->size);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        error = destiny->FUNC_push_cartesian(destiny, myself->point[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Copy an interval of the invoker into destiny, preserving any previously data present at destiny.

    \param self Object that invoked the method.
    \param destiny Object that will recieve the copy.
    \param start Starting position of self to copy into destiny.
    \param end Final position of self to copy into destiny.

    \warning If you wish destiny to be composed only of the start~end interval, you must enforce a clean destiny is passed as argument.
*/
static Error copy_interval(Frame *self, Frame *destiny, uint32_t start, uint32_t end)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error                error = {0};
    Frame               *aux   = NULL;
    CoordinatesCartesian point;
    bool                 using_aux = false;

    if (destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (end - start <= 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "start position is equal or before end position",
            .error       = INVALID_ARGUMENT,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description,
            start,
            end);

        goto return_error;
    }

    if (myself->quantity < end)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.OUT_BOUNDARIES,
            .error       = OUT_BOUNDARIES,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (destiny->FUNC_quantity(destiny) > 0)
    {
        using_aux = true;

        aux = new_block_frame(1, myself->logger);
        if (aux == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = BLOCK_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        error = destiny->FUNC_copy_into(destiny, aux);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    error = destiny->FUNC_resize(
        destiny, destiny->FUNC_quantity(destiny) + (end - start) - 1);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (using_aux)
    {
        for (uint32_t i = 0; i < aux->FUNC_quantity(aux); i++)
        {
            error = aux->FUNC_pull_cartesian(aux, &point, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = destiny->FUNC_push_cartesian(destiny, point);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }

        aux->FUNC_destruct(aux);
        aux = NULL;
    }

    for (uint32_t i = start + 1; i < end; i++)
    {
        error = destiny->FUNC_push_cartesian(destiny, myself->point[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    if (aux != NULL)
    {
        aux->FUNC_destruct(aux);
    }

    return error;
}

/*!
    \brief Print the frame in a stylized format.

    \param self Object that invoked the method.
*/
static Error print(Frame *self)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    // printf("\n");
    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        printf("%f;%f;%f:", myself->point[i].x, myself->point[i].y, myself->point[i].z);
    }
    // printf("\n");

    return OO_SUCCESS;
}

/*!
    \brief Getter for the size of the frame.

    \param self Object that invoked the method.
*/
static uint32_t size(Frame *self)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    return myself->size;
}

/*!
    \brief Getter for the quantity of points at the frame.

    \param self Object that invoked the method.
*/
static uint32_t quantity(Frame *self)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    return myself->quantity;
}

/*!
    \brief Resize and fill the matrix with an representation of the frame values.

    \param self Object that invoked the method.
    \param matrix Object that will recieve the representation of the frame
*/
static Error to_matrix(Frame *self, Matrix *matrix)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error error = {0};

    if (matrix == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = matrix->FUNC_resize(matrix, myself->quantity, 3);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        error = matrix->FUNC_push(matrix, myself->point[i].x, i, 0);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = matrix->FUNC_push(matrix, myself->point[i].y, i, 1);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = matrix->FUNC_push(matrix, myself->point[i].z, i, 2);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add a point to a previously initialized position.

    \param self Object that invoked the method.
    \param point Value to be inserted into the frame.
    \param position Position of the frame to be pushed into.

    \warning Process will always target self, beeing destructive towards the original data.
    \warning Push into uninitialized position will lead to an error.

    \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
*/
static Error
position_push_cartesian(Frame *self, CoordinatesCartesian point, uint32_t position)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error error = {0};

    if (position >= myself->quantity)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "pushed into invalid position",
            .error       = INVALID_ARGUMENT,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    myself->point[position] = point;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add a point to a previously initialized position.

    \param self Object that invoked the method.
    \param point Value to be inserted into the frame.
    \param position Position of the frame to be pushed into.

    \warning Process will always target self, beeing destructive towards the original data.
    \warning Push into uninitialized position will lead to an error.

    \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
*/
static Error
position_push_spherical(Frame *self, CoordinatesSpherical point, uint32_t position)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error                error     = {0};
    CoordinatesCartesian cartesian = {0};

    cartesian.x = point.radius * sin(point.phi) * cos(point.theta);
    cartesian.y = point.radius * sin(point.phi) * sin(point.theta);
    cartesian.z = point.radius * cos(point.phi);

    error = position_push_cartesian(self, cartesian, position);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add a point to the next uninitialized position.

    \param self Object that invoked the method.
    \param point Value to be inserted into the frame.

    \warning Process will always target self, beeing destructive towards the original data.

    \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
*/
static Error push_cartesian(Frame *self, CoordinatesCartesian point)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error error = {0};

    if (myself->quantity >= myself->size)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.SIZE_OVERFLOW,
            .error       = SIZE_OVERFLOW,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    myself->point[myself->quantity] = point;
    myself->quantity++;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add a point to the next uninitialized position.

    \param self Object that invoked the method.
    \param point Value to be inserted into the frame.

    \warning Process will always target self, beeing destructive towards the original data.

    \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
*/
static Error push_spherical(Frame *self, CoordinatesSpherical point)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error                error     = {0};
    CoordinatesCartesian cartesian = {0};

    cartesian.x = point.radius * sin(point.phi) * cos(point.theta);
    cartesian.y = point.radius * sin(point.phi) * sin(point.theta);
    cartesian.z = point.radius * cos(point.phi);

    error = push_cartesian(self, cartesian);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract a point from the frame at requested position.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param position Position of the value.
*/
static Error pull_cartesian(Frame *self, CoordinatesCartesian *point, uint32_t position)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error error = {0};

    if (position >= myself->quantity)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.OUT_BOUNDARIES,
            .error       = OUT_BOUNDARIES,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    *point = myself->point[position];

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract a point from the frame at requested position.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param position Position of the value.
*/
static Error pull_spherical(Frame *self, CoordinatesSpherical *point, uint32_t position)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error                error     = {0};
    CoordinatesCartesian cartesian = {0};

    error = pull_cartesian(self, &cartesian, position);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (cartesian.x == 0)
    {
        cartesian.x = 0.00000000001;
    }

    if (cartesian.z == 0)
    {
        cartesian.z = 0.00000000001;
    }

    point->radius =
        pow(pow(cartesian.x, 2) + pow(cartesian.y, 2) + pow(cartesian.z, 2), 0.5);
    point->theta = atan(cartesian.y / cartesian.x);
    point->phi = atan(pow(pow(cartesian.x, 2) + pow(cartesian.y, 2), 0.5) / cartesian.z);

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the point with the highest value relative to axis.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param axis Axis that must be highest of the frame.
*/
static Error highest_pull_cartesian(Frame *self, CoordinatesCartesian *point, AXIS axis)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error    error = {0};
    uint32_t line  = 0;

    *point = myself->point[0];

    for (uint32_t i = 1; i < myself->quantity; i++)
    {
        switch (axis)
        {
        case X:
            if (point->x < myself->point[i].x)
            {
                *point = myself->point[i];
            }
            break;
        case Y:
            if (point->y < myself->point[i].y)
            {
                *point = myself->point[i];
            }
            break;
        case Z:
            if (point->z < myself->point[i].z)
            {
                *point = myself->point[i];
            }
            break;
        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = BLOCK_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the point with the highest value relative to axis.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param axis Axis that must be highest of the frame.
*/
static Error highest_pull_spherical(Frame *self, CoordinatesSpherical *point, AXIS axis)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error                error    = {0};
    CoordinatesSpherical auxPoint = {0};
    uint32_t             line     = 0;

    error = pull_spherical(self, point, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 1; i < myself->quantity; i++)
    {
        error = pull_spherical(self, &auxPoint, 0);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        switch (axis)
        {
        case RADIUS:
            if (point->radius < auxPoint.radius)
            {
                *point = auxPoint;
            }
            break;
        case THETA:
            if (point->theta < auxPoint.theta)
            {
                *point = auxPoint;
            }
            break;
        case PHI:
            if (point->phi < auxPoint.phi)
            {
                *point = auxPoint;
            }
            break;
        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = BLOCK_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the point with the lowest value relative to axis.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param axis Axis that must be lowest of the frame.
*/
static Error lowest_pull_cartesian(Frame *self, CoordinatesCartesian *point, AXIS axis)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error    error = {0};
    uint32_t line  = 0;

    *point = myself->point[0];

    for (uint32_t i = 1; i < myself->quantity; i++)
    {
        switch (axis)
        {
        case X:
            if (point->x > myself->point[i].x)
            {
                *point = myself->point[i];
            }
            break;
        case Y:
            if (point->y > myself->point[i].y)
            {
                *point = myself->point[i];
            }
            break;
        case Z:
            if (point->z > myself->point[i].z)
            {
                *point = myself->point[i];
            }
            break;
        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = BLOCK_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the point with the lowest value relative to axis.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param axis Axis that must be lowest of the frame.
*/
static Error lowest_pull_spherical(Frame *self, CoordinatesSpherical *point, AXIS axis)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error                error    = {0};
    CoordinatesSpherical auxPoint = {0};
    uint32_t             line     = 0;

    error = pull_spherical(self, point, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 1; i < myself->quantity; i++)
    {
        error = pull_spherical(self, &auxPoint, 0);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        switch (axis)
        {
        case RADIUS:
            if (point->radius > auxPoint.radius)
            {
                *point = auxPoint;
            }
            break;
        case THETA:
            if (point->theta > auxPoint.theta)
            {
                *point = auxPoint;
            }
            break;
        case PHI:
            if (point->phi > auxPoint.phi)
            {
                *point = auxPoint;
            }
            break;
        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = BLOCK_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Move all points of frame respectively to parameters.

    \param self Object that invoked the method.
    \param x_axis Distance that will be moved relative to x axis.
    \param y_axis Distance that will be moved relative to y axis.
    \param z_axis Distance that will be moved relative to z axis.
*/
static Error move_points(Frame *self, float x_axis, float y_axis, float z_axis)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        myself->point[i].x += x_axis;
        myself->point[i].y += y_axis;
        myself->point[i].z += z_axis;
    }

    return OO_SUCCESS;
}

/*!
    \brief Trade the cartesian coordinates of axis a and axis b for every point of the frame.

    \param self Object that invoked the method.
    \param a Axis that will be trade position with b.
    \param b Axis that will be trade position with a.
*/
static Error trade_axis(Frame *self, AXIS a, AXIS b)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);

    Error error = {0};
    float aux   = 0;

    if ((a == X && b == Y) || (b == X && a == Y))
    {
        for (uint32_t i = 0; i < myself->quantity; i++)
        {
            aux                = myself->point[i].x;
            myself->point[i].x = myself->point[i].y;
            myself->point[i].y = aux;
        }
    }
    else if ((a == X && b == Z) || (b == X && a == Z))
    {
        for (uint32_t i = 0; i < myself->quantity; i++)
        {
            aux                = myself->point[i].x;
            myself->point[i].x = myself->point[i].z;
            myself->point[i].z = aux;
        }
    }
    else if ((a == Y && b == Z) || (b == Y && a == Z))
    {
        for (uint32_t i = 0; i < myself->quantity; i++)
        {
            aux                = myself->point[i].y;
            myself->point[i].y = myself->point[i].z;
            myself->point[i].z = aux;
        }
    }
    else if (a != b)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "received unknown axis",
            .error       = INVALID_ARGUMENT,
            .file_line   = BLOCK_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Change the signal of the every point of the frame based on axis and signal arguments.

    \param self Object that invoked the method.
    \param axis Axis that will recieve any changes.
    \param signal Flag to sinalize the inversion of points that are on that signal side of the axis.

    \note signal beeing POSITIVE sends all points to positive counterpart of the axis.
    \note signal beeing NEGATIVE sends all points to negative counterpart of the axis.
    \note signal beeing NEUTRAL invert the axis signal of all points.
*/
static Error mirror_axis(Frame *self, AXIS axis, SIGNAL signal)
{
    BlockFrame *myself = (BlockFrame *)(self->frame);
    uint32_t    line   = 0;

    Error error = {0};

    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        switch (axis)
        {
        case X:
            switch (signal)
            {
            case POSITIVE:
                myself->point[i].x = fabsf(myself->point[i].x);
                break;
            case NEGATIVE:
                myself->point[i].x = -fabsf(myself->point[i].x);
                break;
            case NEUTRAL:
                myself->point[i].x *= -1;
                break;

            default:
                line = __LINE__;

                error = (Error){
                    .description = "received invalid signal",
                    .error       = INVALID_ARGUMENT,
                    .file_line   = BLOCK_FRAME + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
            break;

        case Y:
            switch (signal)
            {
            case POSITIVE:
                myself->point[i].y = fabsf(myself->point[i].y);
                break;
            case NEGATIVE:
                myself->point[i].y = -fabsf(myself->point[i].y);
                break;
            case NEUTRAL:
                myself->point[i].y *= -1;
                break;

            default:
                line = __LINE__;

                error = (Error){
                    .description = "received invalid signal",
                    .error       = INVALID_ARGUMENT,
                    .file_line   = BLOCK_FRAME + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
            break;

        case Z:
            switch (signal)
            {
            case POSITIVE:
                myself->point[i].z = fabsf(myself->point[i].z);
                break;
            case NEGATIVE:
                myself->point[i].z = -fabsf(myself->point[i].z);
                break;
            case NEUTRAL:
                myself->point[i].z *= -1;
                break;

            default:
                line = __LINE__;

                error = (Error){
                    .description = "received invalid signal",
                    .error       = INVALID_ARGUMENT,
                    .file_line   = BLOCK_FRAME + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
            break;

        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = BLOCK_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}
