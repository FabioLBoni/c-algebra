/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "../OO_Global/oo_Global.h"
#include "c_FragFrame.h"

static Frame *new(Frame *self, uint32_t frame_size);

static void *construct();
static Error destruct(Frame *self);

static Error clean(Frame *self);
static Error reset(Frame *self);
static Error resize(Frame *self, uint32_t size);

static Error copy_into(Frame *self, Frame *destiny);
static Error copy_interval(Frame *self, Frame *destiny, uint32_t start, uint32_t end);

static uint32_t size(Frame *self);
static uint32_t quantity(Frame *self);

static Error print(Frame *self);
static Error to_matrix(Frame *self, Matrix *matrix);

static Error
position_push_cartesian(Frame *self, CoordinatesCartesian point, uint32_t position);
static Error
position_push_spherical(Frame *self, CoordinatesSpherical point, uint32_t position);

static Error push_cartesian(Frame *self, CoordinatesCartesian point);
static Error push_spherical(Frame *self, CoordinatesSpherical point);

static Error pull_cartesian(Frame *self, CoordinatesCartesian *point, uint32_t position);
static Error pull_spherical(Frame *self, CoordinatesSpherical *point, uint32_t position);

static Error highest_pull_cartesian(Frame *self, CoordinatesCartesian *point, AXIS axis);
static Error highest_pull_spherical(Frame *self, CoordinatesSpherical *point, AXIS axis);

static Error lowest_pull_cartesian(Frame *self, CoordinatesCartesian *point, AXIS axis);
static Error lowest_pull_spherical(Frame *self, CoordinatesSpherical *point, AXIS axis);

static Error move_points(Frame *self, float x_axis, float y_axis, float z_axis);
static Error trade_axis(Frame *self, AXIS a, AXIS b);
static Error mirror_axis(Frame *self, AXIS axis, SIGNAL signal);

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param matrix Object that will be the abstraction of the frame positions.
    \param frame_size Quantity of position.
    \param logger Optional object to controll the flow of the logs and its priorities.

    \note Upon any errors returns NULL.
*/
Frame *new_frag_frame(Matrix *matrix, uint32_t frame_size, Logger *logger)
{
    Frame *frame = construct_frame(
        construct,
        destruct,
        new,
        clean,
        resize,
        reset,
        copy_into,
        copy_interval,
        print,
        size,
        quantity,
        to_matrix,
        position_push_cartesian,
        position_push_spherical,
        push_cartesian,
        push_spherical,
        pull_cartesian,
        pull_spherical,
        highest_pull_cartesian,
        highest_pull_spherical,
        lowest_pull_cartesian,
        lowest_pull_spherical,
        move_points,
        trade_axis,
        mirror_axis);
    if (frame == NULL)
    {
        goto return_error;
    }

    FragFrame *fragFrame = (FragFrame *)(frame->frame);

    if (logger != NULL)
    {
        fragFrame->logger   = logger;
        fragFrame->FUNC_log = logger->FUNC_log;
    }
    else
    {
        fragFrame->FUNC_log = simple_logger;
    }

    if (matrix == NULL)
    {
        fragFrame->FUNC_log(
            fragFrame->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            "failed to allocate memory");

        goto return_error;
    }

    fragFrame->point = matrix->FUNC_new(matrix, frame_size, 3);
    if (fragFrame->point == NULL)
    {
        fragFrame->FUNC_log(
            fragFrame->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            "failed to allocate memory");

        goto return_error;
    }

    fragFrame->size = frame_size;

    return frame;

return_error:
    if (frame != NULL)
    {
        frame->FUNC_destruct(frame);
    }

    return NULL;
}

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param self Object that invoked the method.
    \param size Quantity of position.

    \note Upon any errors returns NULL.
*/
static Frame *new(Frame *self, uint32_t frame_size)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    if (myself->point == NULL)
    {
        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            "argument holds NULL pointer");

        goto return_error;
    }

    return new_frag_frame(myself->point, frame_size, myself->logger);

return_error:
    return NULL;
}

/*!
    \brief Allocate any necessary memory and make variable initializations.

    \note Upon any errors returns NULL.
*/
static void *construct()
{
    return calloc(1, sizeof(FragFrame));
}

/*!
    \brief Completely destruct the invoker interface and object memory allocations.

    \param self Object that invoked the method.
*/
static Error destruct(Frame *self)
{
    if (self != NULL)
    {
        if (self->frame != NULL)
        {
            FragFrame *myself = (FragFrame *)(self->frame);

            if (myself->point != NULL)
            {
                myself->point->FUNC_destruct(myself->point);
                myself->point = NULL;
            }

            free(self->frame);
            self->frame = NULL;
        }

        free(self);
        self = NULL;
    }

    return OO_SUCCESS;
}

/*!
    \brief Clean the frame by inserting 0 in all positions and setting its quantity to 0.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error clean(Frame *self)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error error = {0};

    error = myself->point->FUNC_clean(myself->point);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    myself->quantity = 0;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Allocates the necessary memory for the new matrix size.

    \param self Object that invoked the method.
    \param size Quantity of positions that frame should have.

    \warning Process will always target self, beeing destructive towards the original data.

    \note Update point quantity to 0.
*/
static Error resize(Frame *self, uint32_t size)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error error = {0};

    error = myself->point->FUNC_resize(myself->point, size, 3);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    myself->size     = size;
    myself->quantity = 0;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Free all allocated positions, safely preparing FragFrame to recieve new allocations.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.

    \note Update point quantity to 0.
*/
static Error reset(Frame *self)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error error = {0};

    error = myself->point->FUNC_reset(myself->point);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    myself->quantity = 0;
    myself->size     = 0;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Make a copy of the invoker.

    \param self Object that invoked the method.
    \param destiny Object that will recieve the copy.
*/
static Error copy_into(Frame *self, Frame *destiny)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error = {0};
    CoordinatesCartesian point = {0};

    if (destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = destiny->FUNC_resize(destiny, myself->size);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        error = pull_cartesian(self, &point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = destiny->FUNC_push_cartesian(destiny, point);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Copy an interval of the invoker into destiny, preserving any previously data present at destiny.

    \param self Object that invoked the method.
    \param destiny Object that will recieve the copy.
    \param start Starting position of self to copy into destiny.
    \param end Final position of self to copy into destiny.

    \warning If you wish destiny to be composed only of the start~end interval, you must enforce a clean destiny is passed as argument.
*/
static Error copy_interval(Frame *self, Frame *destiny, uint32_t start, uint32_t end)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error     = {0};
    Frame               *aux       = NULL;
    CoordinatesCartesian point     = {0};
    bool                 using_aux = false;

    if (destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (end - start <= 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "start position is equal or before end position",
            .error       = INVALID_ARGUMENT,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (myself->quantity < end)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "access on invalid frame position",
            .error       = INVALID_ARGUMENT,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (destiny->FUNC_quantity(destiny) > 0)
    {
        using_aux = true;

        aux = new_frag_frame(myself->point, 1, myself->logger);
        if (aux == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        error = destiny->FUNC_copy_into(destiny, aux);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    error = destiny->FUNC_resize(
        destiny, destiny->FUNC_quantity(destiny) + (end - start) - 1);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (using_aux)
    {
        for (uint32_t i = 0; i < aux->FUNC_quantity(aux); i++)
        {
            error = aux->FUNC_pull_cartesian(aux, &point, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = destiny->FUNC_push_cartesian(destiny, point);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }

        aux->FUNC_destruct(aux);
        aux = NULL;
    }

    for (uint32_t i = start + 1; i < end; i++)
    {
        error = pull_cartesian(self, &point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = destiny->FUNC_push_cartesian(destiny, point);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    if (aux != NULL)
    {
        aux->FUNC_destruct(aux);
    }

    return error;
}

/*!
    \brief Print the frame in a stylized format.

    \param self Object that invoked the method.
*/
static Error print(Frame *self)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error = {0};
    CoordinatesCartesian point = {0};

    // printf("\n");
    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        error = pull_cartesian(self, &point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            return error;
        }

        printf("%10.4f;%10.4f;%10.4f\n", point.x, point.y, point.z);
    }
    printf("\n");

    return OO_SUCCESS;
}

/*!
    \brief Getter for the size of the frame.

    \param self Object that invoked the method.
*/
uint32_t size(Frame *self)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    return myself->size;
}

/*!
    \brief Getter for the quantity of points at the frame.

    \param self Object that invoked the method.
*/
uint32_t quantity(Frame *self)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    return myself->quantity;
}

/*!
    \brief Resize and fill the matrix with an representation of the frame values.

    \param self Object that invoked the method.
    \param matrix Object that will recieve the representation of the frame
*/
static Error to_matrix(Frame *self, Matrix *matrix)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error error = {0};

    if (matrix == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "argument matrix is NULL pointer",
            .error       = NULL_ARGUMENT,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_copy_into(myself->point, matrix);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add a point to a previously initialized position.

    \param self Object that invoked the method.
    \param point Value to be inserted into the frame.
    \param position Position of the frame to be pushed into.

    \warning Process will always target self, beeing destructive towards the original data.
    \warning Push into uninitialized position will lead to an error.

    \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
*/
static Error
position_push_cartesian(Frame *self, CoordinatesCartesian point, uint32_t position)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error error = {0};

    if (position >= myself->quantity)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "pushed into invalid position",
            .error       = INVALID_ARGUMENT,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_push(myself->point, point.x, position, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_push(myself->point, point.y, position, 1);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_push(myself->point, point.z, position, 2);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add a point to a previously initialized position.

    \param self Object that invoked the method.
    \param point Value to be inserted into the frame.
    \param position Position of the frame to be pushed into.

    \warning Process will always target self, beeing destructive towards the original data.
    \warning Push into uninitialized position will lead to an error.

    \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
*/
static Error
position_push_spherical(Frame *self, CoordinatesSpherical point, uint32_t position)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error     = {0};
    CoordinatesCartesian cartesian = {0};

    cartesian.x = point.radius * sin(point.phi) * cos(point.theta);
    cartesian.y = point.radius * sin(point.phi) * sin(point.theta);
    cartesian.z = point.radius * cos(point.phi);

    error = position_push_cartesian(self, cartesian, position);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add a point to the next uninitialized position.

    \param self Object that invoked the method.
    \param point Value to be inserted into the frame.

    \warning Process will always target self, beeing destructive towards the original data.

    \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
*/
static Error push_cartesian(Frame *self, CoordinatesCartesian point)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error error = {0};

    if (myself->quantity >= myself->size)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.SIZE_OVERFLOW,
            .error       = SIZE_OVERFLOW,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_push(myself->point, point.x, myself->quantity, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_push(myself->point, point.y, myself->quantity, 1);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_push(myself->point, point.z, myself->quantity, 2);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    myself->quantity++;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add a point to the next uninitialized position.

    \param self Object that invoked the method.
    \param point Value to be inserted into the frame.

    \warning Process will always target self, beeing destructive towards the original data.

    \note All points are stored as CartesianCoordinates. It is safe to push spherical and cartesian at the same frame.
*/
static Error push_spherical(Frame *self, CoordinatesSpherical point)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error     = {0};
    CoordinatesCartesian cartesian = {0};

    cartesian.x = point.radius * sin(point.phi) * cos(point.theta);
    cartesian.y = point.radius * sin(point.phi) * sin(point.theta);
    cartesian.z = point.radius * cos(point.phi);

    error = push_cartesian(self, cartesian);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract a point from the frame at requested position.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param position Position of the value.
*/
static Error pull_cartesian(Frame *self, CoordinatesCartesian *point, uint32_t position)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error error = {0};

    if (position >= myself->quantity)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.OUT_BOUNDARIES,
            .error       = OUT_BOUNDARIES,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_pull(myself->point, &point->x, position, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_pull(myself->point, &point->y, position, 1);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = myself->point->FUNC_pull(myself->point, &point->z, position, 2);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract a point from the frame at requested position.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param position Position of the value.
*/
static Error pull_spherical(Frame *self, CoordinatesSpherical *point, uint32_t position)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error     = {0};
    CoordinatesCartesian cartesian = {0};

    error = pull_cartesian(self, &cartesian, position);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (cartesian.x == 0)
    {
        cartesian.x = 0.00000000001;
    }

    if (cartesian.z == 0)
    {
        cartesian.z = 0.00000000001;
    }

    point->radius =
        pow(pow(cartesian.x, 2) + pow(cartesian.y, 2) + pow(cartesian.z, 2), 0.5);
    point->theta = atan(cartesian.y / cartesian.x);
    point->phi = atan(pow(pow(cartesian.x, 2) + pow(cartesian.y, 2), 0.5) / cartesian.z);

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the point with the highest value relative to axis.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param axis Axis that must be highest of the frame.
*/
static Error highest_pull_cartesian(Frame *self, CoordinatesCartesian *point, AXIS axis)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error    = {0};
    CoordinatesCartesian auxPoint = {0};
    uint32_t             line     = 0;

    error = pull_cartesian(self, point, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 1; i < myself->quantity; i++)
    {
        error = pull_cartesian(self, &auxPoint, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        switch (axis)
        {
        case X:
            if (point->x < auxPoint.x)
            {
                *point = auxPoint;
            }
            break;
        case Y:
            if (point->y < auxPoint.y)
            {
                *point = auxPoint;
            }
            break;
        case Z:
            if (point->z < auxPoint.z)
            {
                *point = auxPoint;
            }
            break;
        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = FRAG_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the point with the highest value relative to axis.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param axis Axis that must be highest of the frame.
*/
static Error highest_pull_spherical(Frame *self, CoordinatesSpherical *point, AXIS axis)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error    = {0};
    CoordinatesSpherical auxPoint = {0};
    uint32_t             line     = 0;

    error = pull_spherical(self, point, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 1; i < myself->quantity; i++)
    {
        error = pull_spherical(self, &auxPoint, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        switch (axis)
        {
        case RADIUS:
            if (point->radius < auxPoint.radius)
            {
                *point = auxPoint;
            }
            break;
        case THETA:
            if (point->theta < auxPoint.theta)
            {
                *point = auxPoint;
            }
            break;
        case PHI:
            if (point->phi < auxPoint.phi)
            {
                *point = auxPoint;
            }
            break;
        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = FRAG_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the point with the lowest value relative to axis.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param axis Axis that must be lowest of the frame.
*/
static Error lowest_pull_cartesian(Frame *self, CoordinatesCartesian *point, AXIS axis)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error    = {0};
    CoordinatesCartesian auxPoint = {0};
    uint32_t             line     = 0;

    error = pull_cartesian(self, point, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 1; i < myself->quantity; i++)
    {
        error = pull_cartesian(self, &auxPoint, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        switch (axis)
        {
        case X:
            if (point->x > auxPoint.x)
            {
                *point = auxPoint;
            }
            break;
        case Y:
            if (point->y > auxPoint.y)
            {
                *point = auxPoint;
            }
            break;
        case Z:
            if (point->z > auxPoint.z)
            {
                *point = auxPoint;
            }
            break;
        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = FRAG_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the point with the lowest value relative to axis.

    \param self Object that invoked the method.
    \param point Pointer to the variable that will hold the extracted data.
    \param axis Axis that must be lowest of the frame.
*/
static Error lowest_pull_spherical(Frame *self, CoordinatesSpherical *point, AXIS axis)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error    = {0};
    CoordinatesSpherical auxPoint = {0};
    uint32_t             line     = 0;

    error = pull_spherical(self, point, 0);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 1; i < myself->quantity; i++)
    {
        error = pull_spherical(self, &auxPoint, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        switch (axis)
        {
        case RADIUS:
            if (point->radius > auxPoint.radius)
            {
                *point = auxPoint;
            }
            break;
        case THETA:
            if (point->theta > auxPoint.theta)
            {
                *point = auxPoint;
            }
            break;
        case PHI:
            if (point->phi > auxPoint.phi)
            {
                *point = auxPoint;
            }
            break;
        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = FRAG_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Move all points of frame respectively to parameters.

    \param self Object that invoked the method.
    \param x_axis Distance that will be moved relative to x axis.
    \param y_axis Distance that will be moved relative to y axis.
    \param z_axis Distance that will be moved relative to z axis.
*/
static Error move_points(Frame *self, float x_axis, float y_axis, float z_axis)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error = {0};
    CoordinatesCartesian point = {0};

    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        error = pull_cartesian(self, &point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        point.x += x_axis;
        point.y += y_axis;
        point.z += z_axis;

        error = position_push_cartesian(self, point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Trade the cartesian coordinates of axis a and axis b for every point of the frame.

    \param self Object that invoked the method.
    \param a Axis that will be trade position with b.
    \param b Axis that will be trade position with a.
*/
static Error trade_axis(Frame *self, AXIS a, AXIS b)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error = {0};
    CoordinatesCartesian point = {0};
    float                aux   = 0;

    if ((a == X && b == Y) || (b == X && a == Y))
    {
        for (uint32_t i = 0; i < myself->quantity; i++)
        {
            error = pull_cartesian(self, &point, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            aux     = point.x;
            point.x = point.y;
            point.y = aux;

            error = position_push_cartesian(self, point, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }
    else if ((a == X && b == Z) || (b == X && a == Z))
    {
        for (uint32_t i = 0; i < myself->quantity; i++)
        {
            error = pull_cartesian(self, &point, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            aux     = point.x;
            point.x = point.z;
            point.z = aux;

            error = position_push_cartesian(self, point, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }
    else if ((a == Y && b == Z) || (b == Y && a == Z))
    {
        for (uint32_t i = 0; i < myself->quantity; i++)
        {
            error = pull_cartesian(self, &point, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            aux     = point.y;
            point.y = point.z;
            point.z = aux;

            error = position_push_cartesian(self, point, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }
    else if (a != b)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "received unknown axis",
            .error       = INVALID_ARGUMENT,
            .file_line   = FRAG_FRAME + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Change the signal of the every point of the frame based on axis and signal arguments.

    \param self Object that invoked the method.
    \param axis Axis that will recieve any changes.
    \param signal Flag to sinalize the inversion of points that are on that signal side of the axis.

    \note signal beeing POSITIVE sends all points to positive counterpart of the axis.
    \note signal beeing NEGATIVE sends all points to negative counterpart of the axis.
    \note signal beeing NEUTRAL invert the axis signal of all points.
*/
static Error mirror_axis(Frame *self, AXIS axis, SIGNAL signal)
{
    FragFrame *myself = (FragFrame *)(self->frame);

    Error                error = {0};
    CoordinatesCartesian point = {0};
    uint32_t             line  = 0;

    for (uint32_t i = 0; i < myself->quantity; i++)
    {
        error = pull_cartesian(self, &point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        switch (axis)
        {
        case X:
            switch (signal)
            {
            case POSITIVE:
                point.x = fabsf(point.x);
                break;
            case NEGATIVE:
                point.x = -fabsf(point.x);
                break;
            case NEUTRAL:
                point.x *= -1;
                break;

            default:
                line = __LINE__;

                error = (Error){
                    .description = "received invalid signal",
                    .error       = INVALID_ARGUMENT,
                    .file_line   = FRAG_FRAME + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
            break;

        case Y:
            switch (signal)
            {
            case POSITIVE:
                point.y = fabsf(point.y);
                break;
            case NEGATIVE:
                point.y = -fabsf(point.y);
                break;
            case NEUTRAL:
                point.y *= -1;
                break;

            default:
                line = __LINE__;

                error = (Error){
                    .description = "received invalid signal",
                    .error       = INVALID_ARGUMENT,
                    .file_line   = FRAG_FRAME + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
            break;

        case Z:
            switch (signal)
            {
            case POSITIVE:
                point.z = fabsf(point.z);
                break;
            case NEGATIVE:
                point.z = -fabsf(point.z);
                break;
            case NEUTRAL:
                point.z *= -1;
                break;

            default:
                line = __LINE__;

                error = (Error){
                    .description = "received invalid signal",
                    .error       = INVALID_ARGUMENT,
                    .file_line   = FRAG_FRAME + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
            break;

        default:
            line = __LINE__;

            error = (Error){
                .description = "received unknown axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = FRAG_FRAME + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        error = position_push_cartesian(self, point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}
