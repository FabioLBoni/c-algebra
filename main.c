/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    27 de nov de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "OO_Equation/c_CompositeEquation.h"
#include "OO_EquationDiscoveryMethod/c_LeastSquares.h"
#include "OO_EquationTerm/c_IntegerPowerTerm2D.h"
#include "OO_Frame/c_FragFrame.h"
#include "OO_Logger/c_LoggerEnlist.h"
#include "OO_Matrix/c_FragMatrix.h"
#include "OO_MeetingPointMethod/c_NewtonRaphson.h"

#include "OO_Interfaces/i_Equation.h"
#include "OO_Interfaces/i_EquationTerm.h"
#include "OO_Interfaces/i_Frame.h"
#include "OO_Interfaces/i_Logger.h"
#include "OO_Interfaces/i_Matrix.h"
#include "OO_Interfaces/i_MeetingPointMethod.h"
#include "OO_Interfaces/interfaces.h"

#include "OO_Global/oo_Global.h"

char test_buffer[200] = {0};
void test_logger(void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority);

int32_t main()
{
    Error                 error              = {0};
    LoggerConfig         *loggerConfig       = NULL;
    EquationVariableLabel dependant_variable = {
        .name = "y",
        .variable =
            (EquationVariable){
                .axis = Y,
            },
    };
    EquationVariableLabel independent_variable = {
        .name = "x",
        .variable =
            (EquationVariable){
                .axis = X,
            },
    };

    ////////////////////// DECLARATIONS AND CONSTRUCTORS ///////////////////////
    Logger *logger = new_logger_enlist();

    Matrix *matrix = new_frag_matrix(1, 1, logger);
    Frame  *frame  = new_frag_frame(matrix, 20, logger);

    Equation     *equation = new_composite_equation(dependant_variable, logger);
    EquationTerm *term     = new_integer_power_term_2d(independent_variable, logger);

    // EquationDiscoveryMethod *method = new_least_squares(matrix, logger);
    MeetingPointMethod *method = new_newton_raphson(logger);
    //////////////////// DECLARATIONS AND CONSTRUCTORS END /////////////////////

    /////////////////////////// LOGGER SUBSCRIPTION ////////////////////////////
    loggerConfig = logger->FUNC_subscribe(logger, NULL, test_logger);

    loggerConfig->level = DEBUG_PRIORITY;
    // loggerConfig->active = false;
    ///////////////////////// LOGGER SUBSCRIPTION END //////////////////////////

    /////////////////////////////////// TEST ///////////////////////////////////
    //TEST NEWTON-RAPHSON
    Equation *equation2 = equation->FUNC_new(equation, dependant_variable);
    Equation *equation3 = equation->FUNC_new(equation, dependant_variable);

    error = term->FUNC_change(term, 5, 1.0 / 12.0);
    if (error.error != OO_OK)
    {
        printf("received error %6d\n", error.file_line);
        return error.error;
    }

    error = equation2->FUNC_add_term(equation2, term);
    if (error.error != OO_OK)
    {
        printf("received error %6d\n", error.file_line);
        return error.error;
    }

    error = term->FUNC_change(term, 2, 4);
    if (error.error != OO_OK)
    {
        printf("received error %6d\n", error.file_line);
        return error.error;
    }

    error = equation2->FUNC_add_term(equation2, term);
    if (error.error != OO_OK)
    {
        printf("received error %6d\n", error.file_line);
        return error.error;
    }

    error = term->FUNC_change(term, 4, 1);
    if (error.error != OO_OK)
    {
        printf("received error %6d\n", error.file_line);
        return error.error;
    }

    error = equation3->FUNC_add_term(equation3, term);
    if (error.error != OO_OK)
    {
        printf("received error %6d\n", error.file_line);
        return error.error;
    }

    equation2->FUNC_print(equation2);
    equation3->FUNC_print(equation3);

    error = method->FUNC_meeting_point(method, frame, equation2, equation3);
    if (error.error != OO_OK)
    {
        printf("received error %6d\n", error.file_line);
        return error.error;
    }

    printf("roots quantity: %d\n", frame->FUNC_quantity(frame));

    frame->FUNC_print(frame);

    equation2->FUNC_destruct(equation2);
    equation3->FUNC_destruct(equation3);
    //TEST NEWTON-RAPHSON
    ///////////////////////////////// TEST END //////////////////////////////////

    //////////////////////////// DESTRUCTING STUFF /////////////////////////////
    term->FUNC_destruct(term);
    method->FUNC_destruct(method);
    equation->FUNC_destruct(equation);
    frame->FUNC_destruct(frame);
    matrix->FUNC_destruct(matrix);

    logger->FUNC_destruct(logger);
    ////////////////////////// DESTRUCTING STUFF END ///////////////////////////

    return 0;
}

void test_logger(void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority)
{
    char *level = NULL;

    if (priority >= ERROR_PRIORITY)
    {
        level = "ERROR";
    }
    else if (priority >= WARNING_PRIORITY)
    {
        level = "WARNING";
    }
    else if (priority >= INFO_PRIORITY)
    {
        level = "INFO";
    }
    else
    {
        level = "DEBUG";
    }

    printf("%s: %s\n", level, buffer);
}

// //TEST NEWTON-RAPHSON NO ROOTS
// Equation *equation2 = equation->FUNC_new(equation, dependant_variable);
// Equation *equation3 = equation->FUNC_new(equation, dependant_variable);

// error = term->FUNC_change(term, QUADRATIC, 1);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation2->FUNC_add_term(equation2, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, CONSTANT, 4);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation2->FUNC_add_term(equation2, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, QUARTIC, 1);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation3->FUNC_add_term(equation3, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, CONSTANT, 5);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation3->FUNC_add_term(equation3, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// equation2->FUNC_print(equation2);
// equation3->FUNC_print(equation3);

// error = method->FUNC_meeting_point(method, frame, equation2, equation3);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// printf("roots quantity: %d\n", frame->FUNC_quantity(frame));

// frame->FUNC_print(frame);

// equation2->FUNC_destruct(equation2);
// equation3->FUNC_destruct(equation3);
// //TEST NEWTON-RAPHSON NO ROOTS

// //TEST NEWTON-RAPHSON BHASCARA WITHOUT ROOT
// Equation *equation2 = equation->FUNC_new(equation, dependant_variable);
// Equation *equation3 = equation->FUNC_new(equation, dependant_variable);

// error = term->FUNC_change(term, 3, 1.0 / 30.0);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation2->FUNC_add_term(equation2, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, 0, 15);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation2->FUNC_add_term(equation2, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, 2, 1);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation3->FUNC_add_term(equation3, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, 1, -11);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation3->FUNC_add_term(equation3, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// equation2->FUNC_print(equation2);
// equation3->FUNC_print(equation3);

// error = method->FUNC_meeting_point(method, frame, equation2, equation3);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// printf("roots quantity: %d\n", frame->FUNC_quantity(frame));

// frame->FUNC_print(frame);

// equation2->FUNC_destruct(equation2);
// equation3->FUNC_destruct(equation3);
// //TEST NEWTON-RAPHSON BHASCARA WITHOUT ROOT

// //TEST NEWTON-RAPHSON
// Equation *equation2 = equation->FUNC_new(equation, dependant_variable);
// Equation *equation3 = equation->FUNC_new(equation, dependant_variable);

// error = term->FUNC_change(term, 5, 1.0 / 12.0);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation2->FUNC_add_term(equation2, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, 2, 4);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation2->FUNC_add_term(equation2, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, 4, 1);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation3->FUNC_add_term(equation3, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// equation2->FUNC_print(equation2);
// equation3->FUNC_print(equation3);

// error = method->FUNC_meeting_point(method, frame, equation2, equation3);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// printf("roots quantity: %d\n", frame->FUNC_quantity(frame));

// frame->FUNC_print(frame);

// equation2->FUNC_destruct(equation2);
// equation3->FUNC_destruct(equation3);
// //TEST NEWTON-RAPHSON

// //TEST NEWTON-RAPHSON
// Equation *equation2 = equation->FUNC_new(equation, dependant_variable);
// Equation *equation3 = equation->FUNC_new(equation, dependant_variable);

// error = term->FUNC_change(term, 4, 1.0 / 6.0);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation2->FUNC_add_term(equation2, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, 1, -3);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation2->FUNC_add_term(equation2, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, 2, 2);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation3->FUNC_add_term(equation3, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = term->FUNC_change(term, 0, 20);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// error = equation3->FUNC_add_term(equation3, term);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// equation2->FUNC_print(equation2);
// equation3->FUNC_print(equation3);

// error = method->FUNC_meeting_point(method, frame, equation2, equation3);
// if (error.error != OO_OK)
// {
//     printf("received error %6d\n", error.file_line);
//     return error.error;
// }

// printf("roots quantity: %d\n", frame->FUNC_quantity(frame));

// frame->FUNC_print(frame);

// equation2->FUNC_destruct(equation2);
// equation3->FUNC_destruct(equation3);
// //TEST NEWTON-RAPHSON

//     // //TEST LEAST SQUARES
//     CoordinatesCartesian point = {0};

//     point.x = 0.5;
//     point.y = 1;
//     error   = frame->FUNC_push_cartesian(frame, point);
//     if (error.error != OO_OK)
//     {
//         printf("received error %6d\n", error.file_line);
//         return error.error;
//     }

//     point.x = 3;
//     point.y = 4;
//     error   = frame->FUNC_push_cartesian(frame, point);
//     if (error.error != OO_OK)
//     {
//         printf("received error %6d\n", error.file_line);
//         return error.error;
//     }

//     point.x = 7;
//     point.y = 8;
//     error   = frame->FUNC_push_cartesian(frame, point);
//     if (error.error != OO_OK)
//     {
//         printf("received error %6d\n", error.file_line);
//         return error.error;
//     }

//     point.x = -1;
//     point.y = -1;
//     error   = frame->FUNC_push_cartesian(frame, point);
//     if (error.error != OO_OK)
//     {
//         printf("received error %6d\n", error.file_line);
//         return error.error;
//     }

//     point.x = -2;
//     point.y = -5;
//     error   = frame->FUNC_push_cartesian(frame, point);
//     if (error.error != OO_OK)
//     {
//         printf("received error %6d\n", error.file_line);
//         return error.error;
//     }

//     point.x = -3;
//     point.y = -15;
//     error   = frame->FUNC_push_cartesian(frame, point);
//     if (error.error != OO_OK)
//     {
//         printf("received error %6d\n", error.file_line);
//         return error.error;
//     }

//     term->FUNC_change(term, 4, 1);
//     equation->FUNC_add_term(equation, term);

//     term->FUNC_change(term, 3, 1);
//     equation->FUNC_add_term(equation, term);

//     term->FUNC_change(term, 2, 1);
//     equation->FUNC_add_term(equation, term);

//     term->FUNC_change(term, 1, 1);
//     equation->FUNC_add_term(equation, term);

//     term->FUNC_change(term, 0, 1);
//     equation->FUNC_add_term(equation, term);

//     equation->FUNC_print(equation);

//     error = equation->FUNC_find_equation(equation, frame, method);
//     if (error.error != OO_OK)
//     {
//         printf("received error %6d\n", error.file_line);
//         return error.error;
//     }

//     printf("\n");
//     // frame->FUNC_print(frame);
//     printf("\n");

//     equation->FUNC_print(equation);
//     //TEST LEAST SQUARES
