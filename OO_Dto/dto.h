/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    05 de dez de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include "../OO_Types/oo_Types.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

/*!
    \param self In case of object orientation, this should point to the object in controll of config. DO NOT CHANGE IT IN RUNTIME.
    \param name String alias for legibility.
    \param active Signal if logger should log.
    \param level Signal the lower priority to be logged
    \param id Unique identification.
*/
typedef struct LoggerConfig
{
    void        *self;
    char        *name;
    bool         active;
    LOG_PRIORITY level;
    uint8_t      id;
} LoggerConfig;

/*!
    \param radius Distance of the point from the origin.
    \param theta Rotation angle on the plane xy.
    \param phi Angle that axis z does with plane xy.
*/
typedef struct CoordinatesSpherical
{
    float radius;
    float theta;
    float phi;
} CoordinatesSpherical;

/*!
    \param x Distance from the axis x.
    \param y Distance from the axis y.
    \param z Distance from the axis z.
*/
typedef struct CoordinatesCartesian
{
    float x;
    float y;
    float z;
} CoordinatesCartesian;

/*!
    
    \param value Value of the variable, will be passed to EquationTerms and be transformed by them.
    \param axis Axis used to identify correspondence with other variables.
*/
typedef struct EquationVariable
{
    float value;
    AXIS  axis;
} EquationVariable;

/*!
    \param name Name of the variable to be printed.
    \param EquationVariable Axis used to identify correspondence with other variables.
*/
typedef struct EquationVariableLabel
{
    char            *name;
    EquationVariable variable;
} EquationVariableLabel;

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif
