/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    14 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../OO_Global/oo_Global.h"
#include "c_CompositeEquation.h"

#define INITIAL_TERMS_ALLOCATION 5
#define RESIZING_INCREMENT 4

static Equation *new(Equation *self, EquationVariableLabel dependent_variable);

static void *construct();
static Error destruct(Equation *self);

static Error
replace_dependent_variable(Equation *self, EquationVariableLabel dependent_variable);
static EquationVariableLabel dependent_variable(Equation *self);

static Error grow(Equation *self);
static Error reset(Equation *self);
static Error copy_into(Equation *self, Equation *destiny);
static Error print(Equation *self);

static Error aggregate(Equation *self);
static Error ordenate(Equation *self);

static Error add_term(Equation *self, EquationTerm *term);
static Error get_terms(Equation *self, EquationTerm *(*terms[]), uint16_t *quantity);

static Error find_equation(Equation *self, Frame *frame, EquationDiscoveryMethod *method);

static Error calculate(
    Equation        *self,
    EquationVariable variables[],
    uint16_t         variables_quantity,
    float           *result);
static Error meeting_point(
    Equation *self, Equation *equation, MeetingPointMethod *method, Frame *frame);

static Error integrate(Equation *self, Equation *destiny);
static Error derivative(Equation *self, Equation *destiny);
static Error power(Equation *self, Equation *destiny, float power);

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param variable Signal to which axis the function return value is.
    \param value Constant value of the left term of an equation. 
    \param logger Optional object to controll the flow of the logs and its priorities.

    \note Upon any errors returns NULL.
*/
Equation *
new_composite_equation(EquationVariableLabel dependent_variable_label, Logger *logger)
{
    Equation *equation = construct_equation(
        construct,
        new,
        destruct,
        replace_dependent_variable,
        dependent_variable,
        reset,
        copy_into,
        print,
        aggregate,
        ordenate,
        add_term,
        get_terms,
        find_equation,
        calculate,
        meeting_point,
        integrate,
        derivative,
        power);
    if (equation == NULL)
    {
        goto return_error;
    }

    CompositeEquation *compositeEquation = (CompositeEquation *)(equation->equation);

    if (logger != NULL)
    {
        compositeEquation->logger   = logger;
        compositeEquation->FUNC_log = logger->FUNC_log;
    }
    else
    {
        compositeEquation->FUNC_log = simple_logger;
    }

    if (dependent_variable_label.name == NULL)
    {
        compositeEquation->FUNC_log(
            compositeEquation->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            "variable must have a name");

        goto return_error;
    }

    if (dependent_variable_label.name[0] == '\0')
    {
        compositeEquation->FUNC_log(
            compositeEquation->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            "variable must have a name");

        goto return_error;
    }

    compositeEquation->terms = calloc(INITIAL_TERMS_ALLOCATION, sizeof(EquationTerm *));
    if (compositeEquation->terms == NULL)
    {
        compositeEquation->FUNC_log(
            compositeEquation->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            ERROR_STRING.FAILED_MEMORY_ALLOCATION);

        goto return_error;
    }

    compositeEquation->dependent_variable_label = dependent_variable_label;

    return equation;

return_error:
    if (equation != NULL)
    {
        equation->FUNC_destruct(equation);
        equation = NULL;
    }

    return NULL;
}

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param self Object that invoked the method.
    \param variable Signal to which axis the function return value is.
    \param value Constant value of the left term of an equation. 

    \note Upon any errors returns NULL.
*/
static Equation *new(Equation *self, EquationVariableLabel dependent_variable)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    return new_composite_equation(dependent_variable, myself->logger);
}

/*!
    \brief Allocate any necessary memory and make variable initializations.

    \note Upon any errors returns NULL.
*/
static void *construct()
{
    return calloc(1, sizeof(CompositeEquation));
}

/*!
    \brief Completely destruct the invoker interface and object memory allocations.

    \param self Object that invoked the method.
*/
static Error destruct(Equation *self)
{
    if (self != NULL)
    {
        if (self->equation != NULL)
        {
            CompositeEquation *myself = (CompositeEquation *)(self->equation);

            if (myself->terms != NULL)
            {
                if (myself->quantity > 0)
                {
                    for (uint16_t i = 0; i < myself->quantity; i++)
                    {
                        myself->terms[i]->FUNC_destruct(myself->terms[i]);
                        myself->terms[i] = NULL;
                    }
                }

                free(myself->terms);
                myself->terms = NULL;
            }

            free(self->equation);
            self->equation = NULL;
        }

        free(self);
        self = NULL;
    }

    return OO_SUCCESS;
}

/*!
    \brief Change the basic information of the left part of the equation.

    \param self Object that invoked the method.
    \param variable Signal to which axis the function return value is.
    \param value Constant value of the left term of an equation.

    \note Case variable is NO_AXIS, it defines a constant left term of value.
*/
static Error
replace_dependent_variable(Equation *self, EquationVariableLabel dependent_variable)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    if (dependent_variable.name == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (dependent_variable.name[0] == '\0')
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "variable must have a name",
            .error       = INVALID_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    myself->dependent_variable_label = dependent_variable;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract the basic information of the left part of the equation.

    \param self Object that invoked the method.
    \param variable Pointer to the EquationVariableLabel that will hold the variable information.

    \note value is useful only if the equation have a left term constant.

*/
static EquationVariableLabel dependent_variable(Equation *self)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    return myself->dependent_variable_label;
}

/*!
    \brief Make a new memory allocation to hold more terms.

    \param self Object that invoked the method.
*/
static Error grow(Equation *self)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    EquationTerm **terms = (EquationTerm **)calloc(
        myself->size + RESIZING_INCREMENT, sizeof(EquationTerm *));
    if (terms == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    for (uint16_t i = 0; i < myself->quantity; i++)
    {
        terms[i] = myself->terms[i];
    }

    free(myself->terms);
    myself->terms = terms;
    myself->size += RESIZING_INCREMENT;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Destruct all the EquationTerm referenced by the invoker and set quantity to 0. 

    \param self Object that invoked the method.
*/
static Error reset(Equation *self)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    for (uint16_t i = 0; i < myself->quantity; i++)
    {
        error = myself->terms[i]->FUNC_destruct(myself->terms[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        myself->terms[i] = NULL;
    }

    myself->quantity = 0;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Make a copy of the invoker.

    \param self Object that invoked the method. Will have its content copied into destiny.
    \param destiny Object that will recieve the copy.
*/
static Error copy_into(Equation *self, Equation *destiny)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    if (destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = destiny->FUNC_reset(destiny);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint16_t i = 0; i < myself->quantity; i++)
    {
        error = destiny->FUNC_add_term(destiny, myself->terms[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Print the equation in a stylized format.

    \param self Object that invoked the method.
*/
static Error print(Equation *self)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    printf("%s = ", myself->dependent_variable_label.name);

    for (uint16_t i = 0; i < myself->quantity; i++)
    {
        error = myself->terms[i]->FUNC_print(myself->terms[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    printf("\n");

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Agregate compatible terms present on array of EquationTerm, destructing one of duplicateded terms and rearranging the array
    to fill the newly void position.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error aggregate(Equation *self)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    for (uint16_t i = 0; i < myself->quantity - 1; i++)
    {
        for (uint16_t j = i + 1; j < myself->quantity; j++)
        {
            error = myself->terms[i]->FUNC_aggregate(myself->terms[i], myself->terms[j]);
            if (error.error != OO_OK && error.error)
            {
                if (error.error != IMPOSSIBLE_AGGREGATION)
                {
                    myself->FUNC_log(
                        myself->logger,
                        DEBUG_PRIORITY,
                        ERROR_STRING.HEADER_RECEIVED,
                        __FILENAME__,
                        __LINE__,
                        error.file_line,
                        error.description);

                    goto return_error;
                }

                continue;
            }

            myself->terms[j]->FUNC_destruct(myself->terms[j]);
            for (uint16_t k = j; k < myself->quantity - 1; k++)
            {
                myself->terms[k] = myself->terms[k + 1];
            }

            myself->quantity--;
            j--;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Ordenate the array of EquationTerm by the term type then term core.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.

    \note It first call to FUNC_aggregate to avoid any ambiguity.
*/
static Error ordenate(Equation *self)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error         error = {0};
    EQUATION_TERM earliest_type, latest_type;
    float         earliest_core, latest_core;
    EquationTerm *term = NULL;

    error = aggregate(self);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint16_t i = 0; i < myself->quantity - 1; i++)
    {
        for (uint16_t j = i; j < myself->quantity; j++)
        {
            earliest_type = myself->terms[i]->FUNC_type(myself->terms[i]);
            latest_type   = myself->terms[j]->FUNC_type(myself->terms[j]);

            earliest_core = myself->terms[i]->FUNC_core(myself->terms[i]);
            latest_core   = myself->terms[j]->FUNC_core(myself->terms[j]);

            if (latest_type < earliest_type
                || (earliest_type == latest_type && latest_core > earliest_core))
            {
                term             = myself->terms[i];
                myself->terms[i] = myself->terms[j];
                myself->terms[j] = term;
            }
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Add term to the equation behaviour.

    \param self Object that invoked the method.
    \param term Object containing a piece of equation behaviour.

    \warning Object term will not be controled, modified, destructed nor referenced by self.
*/
static Error add_term(Equation *self, EquationTerm *term)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    if (term == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (myself->dependent_variable_label.variable.value
        == term->FUNC_variable(term).variable.axis)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description =
                "equation dependent variable cannot have reference to the same axis as any term",
            .error     = INVALID_ARGUMENT,
            .file_line = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (myself->quantity == myself->size)
    {
        error = grow(self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    myself->terms[myself->quantity] = term->FUNC_new(term->FUNC_variable(term), term);
    if (myself->terms[myself->quantity] == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = term->FUNC_copy_into(term, &myself->terms[myself->quantity]);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    myself->quantity++;

    return OO_SUCCESS;

return_error:
    if (myself->quantity < myself->size - 1 && myself->terms[myself->quantity] != NULL)
    {
        myself->terms[myself->quantity]->FUNC_destruct(myself->terms[myself->quantity]);
    }

    return error;
}

/*!
    \brief Extract the terms existent at invoker.

    \param self Object that invoked the method.
    \param terms Pointer to an array of EquationTerm objects. The array will hold all terms extracted.
    \param quantity Pointer to a variable that will signal how much terms were extracted.

    \warning Keep in mind that any term previously existent at terms will lose its reference, possibly leading to memory leak.
    \warning Enforce that terms point to a valid dynamic memory allocated array. Method will free and re-allocate for the adequate size. 
    Memory garbage will lead to invalid memory access.
    \warning The terms array passed as argument will hold a not a copy, but a reference of self's terms. Keep in mind that modify or 
    destruct these terms outside the object will affect directly the source, possibly causing unexpected behaviour if values changes, 
    or even invalid memory access if destructed.

    \note If in need to change values without affecting the equation behaviour, you must use FUNC_new and FUNC_copy_into from EquationTerm 
    object to create another instance EquationTerm holding the same values.
*/
static Error get_terms(Equation *self, EquationTerm *(*terms[]), uint16_t *quantity)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error          error = {0};
    EquationTerm **aux   = NULL;

    if (terms == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (*terms == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    aux = *terms;

    *terms = calloc(myself->quantity, sizeof(EquationTerm *));
    if (*terms == NULL)
    {
        *terms = aux;

        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }
    free(aux);

    for (uint16_t i = 0; i < myself->quantity; i++)
    {
        (*terms)[i] = myself->terms[i];
    }
    *quantity = myself->quantity;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Use a EquationDiscoveryMethod interface to find the most aproximate equation to the given collection of points.

    \param self Object that invoked the method. One of the equations to be analyzed.
    \param method Object that implements a method of finding a curve equation from a collection of points.
    \param frame Object that hold the collection of points.
*/
static Error find_equation(Equation *self, Frame *frame, EquationDiscoveryMethod *method)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    error = method->FUNC_equation(method, frame, self);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Calculate a single point of the equation.

    \param self Object that invoked the method.
    \param point Pointer to DTO that will hold the result of the calculation.
    \param giving Confirmation of the axis you expect to be used for calculation.
*/
static Error calculate(
    Equation        *self,
    EquationVariable variables[],
    uint16_t         variables_quantity,
    float           *result)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error                 error                  = {0};
    float                 calculated             = 0;
    EquationVariable      variable               = {0};
    EquationVariableLabel term_variable          = {0};
    uint16_t              equation_axis_quantity = 0;
    bool                  is_valid_argument      = false;
    AXIS                  equation_axis[myself->quantity];

    *result = 0;
    memset(equation_axis, NO_AXIS, myself->quantity);

    if (variables == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    for (uint16_t i = 0; i < variables_quantity - 1; i++)
    {
        if (variables[i].axis == NO_AXIS)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = "argument with no axis",
                .error       = INVALID_AXIS,
                .file_line   = COMPOSITE_EQUATION + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        for (uint16_t j = i; j < variables_quantity; j++)
        {
            if (variables[i].axis == variables[j].axis)
            {
                uint32_t line = __LINE__;

                error = (Error){
                    .description = "ambiguous argument due to repeated axis",
                    .error       = DUPLICATED_AXIS,
                    .file_line   = COMPOSITE_EQUATION + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
        }
    }

    for (uint16_t i = 0; i < myself->quantity; i++)
    {
        term_variable = myself->terms[i]->FUNC_variable(myself->terms[i]);

        if (term_variable.variable.axis == NO_AXIS)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = "equation holds term with no axis relation",
                .error       = INVALID_AXIS,
                .file_line   = COMPOSITE_EQUATION + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        for (uint16_t j = 0; j < equation_axis_quantity; j++)
        {
            if (term_variable.variable.axis == equation_axis[j])
            {
                goto same_equation_axis;
            }
        }

        equation_axis[equation_axis_quantity] = term_variable.variable.axis;
        equation_axis_quantity++;

    same_equation_axis:
        continue;
    }

    if (variables_quantity != equation_axis_quantity)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "argument axis do not matches equation existent axis",
            .error       = INVALID_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    for (uint16_t i = 0; i < variables_quantity; i++, is_valid_argument = false)
    {
        for (uint16_t j = 0; j < equation_axis_quantity; j++)
        {
            if (variables[i].axis == equation_axis[j])
            {
                is_valid_argument = true;
                break;
            }
        }

        if (is_valid_argument == false)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = "argument axis do not matches equation existent axis",
                .error       = INVALID_ARGUMENT,
                .file_line   = COMPOSITE_EQUATION + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }

    for (uint16_t i = 0; i < myself->quantity; i++, calculated = 0)
    {
        term_variable = myself->terms[i]->FUNC_variable(myself->terms[i]);

        for (uint16_t j = 0; j < myself->quantity; j++)
        {
            if (term_variable.variable.axis == variables[j].axis)
            {
                variable = variables[j];
                break;
            }
        }

        error = myself->terms[i]->FUNC_calculate(myself->terms[i], variable, &calculated);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        *result += calculated;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Use a MeetingPointMethod interface to find the meeting points between two curve equations.

    \param self Object that invoked the method. One of the equations to be analyzed.
    \param equation Object contains one of the equations to be analyzed.
    \param method Object that implements a method of finding curve meeting points.
    \param frame Object that will hold the collection of meeting points found by method.

    \warning frame will be reseted, beeing destructive towards any data existent data.
*/
static Error meeting_point(
    Equation *self, Equation *equation, MeetingPointMethod *method, Frame *frame)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error error = {0};

    if (equation == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (method == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (frame == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = method->FUNC_meeting_point(method, frame, self, equation);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Calculate the integral of the equation, therefore, calculate the integral of all the terms of the equation.

    \param self Object that invoked the method.
    \param destiy Object that will recieve the integral result.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.
*/
static Error integrate(Equation *self, Equation *destiny)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error     error = {0};
    Equation *aux   = NULL;

    if (myself->quantity <= 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "equation has no terms",
            .error       = INVALID_SELF_DATA,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (destiny != NULL)
    {
        error = copy_into(self, destiny);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    for (uint16_t i = 0; i < myself->quantity; i++)
    {
        error = myself->terms[i]->FUNC_integrate(myself->terms[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    if (destiny != NULL)
    {
        aux = new_composite_equation(myself->dependent_variable_label, myself->logger);
        if (aux == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = COMPOSITE_EQUATION + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        error = destiny->FUNC_copy_into(destiny, aux);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = copy_into(self, destiny);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = aux->FUNC_copy_into(aux, self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        aux->FUNC_destruct(aux);
    }

    return OO_SUCCESS;

return_error:
    if (aux != NULL)
    {
        aux->FUNC_destruct(aux);
    }

    return error;
}

/*!
    \brief Calculate the derivative of the equation, therefore, calculate the derivative of all the terms of the equation.

    \param self Object that invoked the method.
    \param destiy Object that will recieve the derivative result.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.
*/
static Error derivative(Equation *self, Equation *destiny)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error     error       = {0};
    bool      modify_self = true;
    Equation *aux         = NULL;

    if (myself->quantity <= 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "equation has no terms",
            .error       = INVALID_SELF_DATA,
            .file_line   = COMPOSITE_EQUATION + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (destiny != NULL)
    {
        modify_self = false;

        error = copy_into(self, destiny);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    for (uint16_t i = 0; i < myself->quantity; i++)
    {
        error = myself->terms[i]->FUNC_derivative(myself->terms[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        if (myself->terms[i]->FUNC_coeficient(myself->terms[i]) == 0)
        {
            myself->terms[i]->FUNC_destruct(myself->terms[i]);
            for (uint16_t k = i; k < myself->quantity - 1; k++)
            {
                myself->terms[k] = myself->terms[k + 1];
            }

            myself->quantity--;
            i--;
        }
    }

    if (modify_self == false)
    {
        aux = new_composite_equation(myself->dependent_variable_label, myself->logger);
        if (aux == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = COMPOSITE_EQUATION + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        error = destiny->FUNC_copy_into(destiny, aux);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = copy_into(self, destiny);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = aux->FUNC_copy_into(aux, self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        aux->FUNC_destruct(aux);
    }

    return OO_SUCCESS;

return_error:
    if (aux != NULL)
    {
        aux->FUNC_destruct(aux);
    }

    return error;
}

/*!
    \warning UNIMPLEMENTED
*/
static Error power(Equation *self, Equation *destiny, float power)
{
    CompositeEquation *myself = (CompositeEquation *)(self->equation);

    Error    error = {0};
    uint32_t line  = __LINE__;

    error = (Error){
        .description = "unimplemented method",
        .error       = INVALID_SELF_DATA,
        .file_line   = COMPOSITE_EQUATION + line,
    };

    myself->FUNC_log(
        myself->logger,
        ERROR_PRIORITY,
        ERROR_STRING.HEADER_PRODUCED,
        __FILENAME__,
        line,
        __func__,
        error.description);

    return error;
}
