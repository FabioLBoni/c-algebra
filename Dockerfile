FROM gcc:latest AS builder

COPY . /usr/src/c-algebra

WORKDIR /usr/src/c-algebra

RUN sh build.sh

FROM alpine:3.19.0 AS final

#https://github.com/jeanblanchard/docker-alpine-glibc/blob/main/Dockerfile
ENV GLIBC_VERSION 2.35-r1

RUN apk add --update curl && \
    curl -Lo /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
    curl -Lo glibc.apk "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk" && \
    curl -Lo glibc-bin.apk "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-bin-${GLIBC_VERSION}.apk" && \
    apk add --force-overwrite glibc-bin.apk glibc.apk && \
    /usr/glibc-compat/sbin/ldconfig /lib /usr/glibc-compat/lib && \
    echo 'hosts: files mdns4_minimal [NOTFOUND=return] dns mdns4' >> /etc/nsswitch.conf && \
    apk del curl && \
    rm -rf /var/cache/apk/* glibc.apk glibc-bin.apk

COPY --from=builder /usr/src/c-algebra .

CMD ["./c-algebra"]