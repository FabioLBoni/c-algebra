/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    23 de jan de 2024
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "c_LoggerEnlist.h"

static void         *construct();
static Error         destruct(Logger *self);
static LoggerConfig *subscribe(
    Logger *self,
    void   *log_object,
    void (*FUNC_log)(
        void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority));
static LoggerConfig *subscribe_config(
    Logger       *self,
    LoggerConfig *config,
    void (*FUNC_log)(
        void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority));
static void close_flow(Logger *self);
static void open_flow(Logger *self);
static void log_message(Logger *self, LOG_PRIORITY priority, char *format, ...);

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \attention LoggerEnlist is bound to have the #define LOGGER_ENLIST_SIZE as maximum subscribed functions quantity. This should 
        be improved to dynamic size in the future.

    \note Upon any errors returns NULL.
*/
Logger *new_logger_enlist()
{
    Logger *logger = construct_logger(
        construct,
        destruct,
        subscribe,
        subscribe_config,
        close_flow,
        open_flow,
        log_message);
    if (logger == NULL)
    {
        goto return_error;
    }

    LoggerEnlist *loggerEnlist = (LoggerEnlist *)(logger->logger);

    pthread_mutex_init(&loggerEnlist->mutex, NULL);

    loggerEnlist->flow_open = true;

    return logger;

return_error:
    if (logger != NULL)
    {
        logger->FUNC_destruct(logger);
    }

    return NULL;
}

/*!
    \brief Allocate any necessary memory and make variable initializations.

    \note Upon any errors returns NULL.
*/
static void *construct()
{
    return calloc(1, sizeof(LoggerEnlist));
}

/*!
    \brief Completely destruct the invoker interface and object memory allocations.

    \param self Object that invoked the method.

    \warning Destroing this object may lead to invalid memory access problems on others loggers using the same reference 
        to LoggerConfig structs. 
*/
static Error destruct(Logger *self)
{
    if (self != NULL)
    {
        if (self->logger != NULL)
        {
            LoggerEnlist *myself = (LoggerEnlist *)(self->logger);

            if (myself->subscriber != NULL)
            {
                for (uint8_t i = 0; i < myself->subscriber_quantity; i++)
                {
                    if (myself->subscriber->config != NULL)
                    {
                        free(myself->subscriber->config);
                        myself->subscriber->config = NULL;
                    }
                }

                free(myself->subscriber);
                myself->subscriber = NULL;
            }

            free(self->logger);
            self->logger = NULL;
        }

        free(self);
        self = NULL;
    }

    return OO_SUCCESS;
}

/*!
    \brief Subscribe a function to be potential call when logging.

    \attention LoggerEnlist is bound to have the #define LOGGER_ENLIST_SIZE as maximum subscribed functions quantity. This should 
        be improved to dynamic size in the future.

    \param self Object that invoked the method.
    \param log_object Object that logger will link to self parameter of FUNC_log. In case of non object oriented function subscribe, it is safe to pass NULL.
    \param FUNC_log Pointer to function that will be called by the logger.

    \return Pointer to DTO that hold the configurations of the subscribed function.
        Modifying its values will affect the behavior between Logger and FUNC_log.

    \note Safe to modify returned value at runtime.
*/
static LoggerConfig *subscribe(
    Logger *self,
    void   *log_object,
    void (*FUNC_log)(
        void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority))
{
    LoggerEnlist *myself = (LoggerEnlist *)(self->logger);

    void *aux = NULL;

    aux = realloc(
        myself->subscriber,
        (myself->subscriber_quantity + 1) * sizeof(LoggerEnlistSubscriber));
    if (aux == NULL)
    {
        goto return_error;
    }

    myself->subscriber = (LoggerEnlistSubscriber *)aux;

    myself->subscriber[myself->subscriber_quantity].FUNC_log = FUNC_log;
    myself->subscriber[myself->subscriber_quantity].config =
        (LoggerConfig *)calloc(1, sizeof(LoggerConfig));
    if (myself->subscriber[myself->subscriber_quantity].config == NULL)
    {
        goto return_error;
    }

    myself->subscriber[myself->subscriber_quantity].config->active = true,
    myself->subscriber[myself->subscriber_quantity].config->level  = ERROR_PRIORITY,
    myself->subscriber[myself->subscriber_quantity].config->self   = log_object,

    myself->subscriber_quantity++;

    return myself->subscriber[myself->subscriber_quantity - 1].config;

return_error:
    return NULL;
}

/*!
    \brief Subscribe a function to be potential call when logging.

    \attention LoggerEnlist is bound to have the #define LOGGER_ENLIST_SIZE as maximum subscribed functions quantity. This should 
        be improved to dynamic size in the future.

    \param self Object that invoked the method.
    \param config Pointer to already existent configuration, it will be referenced by the logger.
    \param FUNC_log Pointer to function that will be called by the logger.

    \return Pointer to DTO that hold the configurations of the subscribed function.
        Modifying its values will affect the behavior between Logger and FUNC_log.

    \note Safe to modify returned value at runtime.
*/
static LoggerConfig *subscribe_config(
    Logger       *self,
    LoggerConfig *config,
    void (*FUNC_log)(
        void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority))
{
    LoggerEnlist *myself = (LoggerEnlist *)(self->logger);

    void *aux = NULL;

    aux = realloc(
        myself->subscriber,
        (myself->subscriber_quantity + 1) * sizeof(LoggerEnlistSubscriber));
    if (aux == NULL)
    {
        goto return_error;
    }

    myself->subscriber = (LoggerEnlistSubscriber *)aux;

    myself->subscriber[myself->subscriber_quantity].FUNC_log = FUNC_log;
    myself->subscriber[myself->subscriber_quantity].config =
        (LoggerConfig *)calloc(1, sizeof(LoggerConfig));
    if (myself->subscriber[myself->subscriber_quantity].config == NULL)
    {
        goto return_error;
    }

    myself->subscriber[myself->subscriber_quantity].config = config;

    myself->subscriber_quantity++;

    return myself->subscriber[myself->subscriber_quantity - 1].config;

return_error:
    return NULL;
}

static void close_flow(Logger *self)
{
    LoggerEnlist *myself = (LoggerEnlist *)(self->logger);

    myself->flow_open = false;
}

static void open_flow(Logger *self)
{
    LoggerEnlist *myself = (LoggerEnlist *)(self->logger);

    myself->flow_open = true;
}

/*!
    \brief Log via all subscribed functions that are active and meets the priority of the log.

    \param self Object that invoked the method.
    \param priority Level of priority of the log.
    \param format String following the printf pattern.
    \param ... Multiple arguments following the printf pattern.

    \warning Subscribed functions must not access this logger, directly or indirectly via referenced objects. If a 
        subscribed function access an object that uses a logger, that object must reference another logger.
*/
static void log_message(Logger *self, LOG_PRIORITY priority, char *format, ...)
{
    LoggerEnlist *myself = (LoggerEnlist *)(self->logger);

    if (myself->flow_open == false)
    {
        return;
    }

    va_list args;
    va_start(args, format);

    pthread_mutex_lock(&myself->mutex);

    vsnprintf(myself->buffer, BUFFER_SIZE_LOGGER_ENLIST, format, args);
    va_end(args);

    for (uint8_t i = 0; i < myself->subscriber_quantity; i++)
    {
        if (myself->subscriber[i].config->level <= priority
            && myself->subscriber[i].config->active == true)
        {
            myself->subscriber[i].FUNC_log(
                myself->subscriber[i].config->self,
                myself->buffer,
                BUFFER_SIZE_LOGGER_ENLIST,
                priority);
        }
    }

    pthread_mutex_unlock(&myself->mutex);
}