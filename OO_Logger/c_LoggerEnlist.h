/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    23 de jan de 2024
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include <pthread.h>

#include "../OO_Interfaces/i_Logger.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

#define BUFFER_SIZE_LOGGER_ENLIST 256

typedef struct LoggerEnlistSubscriber
{
    LoggerConfig *config;
    void (*FUNC_log)(
        void *self, char buffer[], size_t buffer_size, LOG_PRIORITY priority);
} LoggerEnlistSubscriber;

/*!
    \attention LoggerEnlist is bound to have the #define LOGGER_ENLIST_SIZE as maximum subscribed functions quantity. This should 
        be improved to dynamic size in the future.
*/
typedef struct LoggerEnlist
{
    LoggerEnlistSubscriber *subscriber;
    pthread_mutex_t         mutex;

    bool    flow_open;
    uint8_t subscriber_quantity;

    char buffer[BUFFER_SIZE_LOGGER_ENLIST];
} LoggerEnlist;

Logger *new_logger_enlist();

#ifdef __cplusplus /* If this is a C++ compiler, end C linkage */
}
#endif