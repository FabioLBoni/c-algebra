/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "../OO_Global/oo_Global.h"
#include "c_FragMatrix.h"

static Matrix *new(Matrix *self, uint32_t rows, uint32_t collumns);

static void *construct();
static Error destruct(Matrix *self);

static Error clean(Matrix *self);
static Error resize(Matrix *self, uint32_t rows, uint32_t collumns);
static Error reset(Matrix *self);
static Error copy_into(Matrix *self, Matrix *destiny);

static Error print(Matrix *self);

static uint32_t rows(Matrix *self);
static uint32_t collumns(Matrix *self);
static uint32_t order(Matrix *self);
static Error    position(
       FragMatrix *myself,
       uint32_t   *outter,
       uint32_t   *inner,
       uint32_t    row,
       uint32_t    collumn);
static Error push(Matrix *self, float value, uint32_t row, uint32_t collumn);
static Error pull(Matrix *self, float *value, uint32_t row, uint32_t collumn);

static Error multiply_scalar(Matrix *self, float scalar);
static Error multiply(Matrix *self, Matrix *multiplier, Matrix *destiny);
static Error determinant(Matrix *self, float *determinant);
static Error transposed(Matrix *self, Matrix *destiny);
static Error minorized(Matrix *self, Matrix *destiny);
static Error place_signify(Matrix *self);
static Error cofactor(Matrix *self, Matrix *destiny);
static Error adjoint(Matrix *self, Matrix *destiny);
static Error inverse(Matrix *self, Matrix *destiny);

static Error position(
    FragMatrix *myself,
    uint32_t   *outter,
    uint32_t   *inner,
    uint32_t    row,
    uint32_t    collumn);

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param rows Quantity of rows that matrix should have.
    \param collumns Quantity of collumns that matrix should have. 
    \param logger Optional object to controll the flow of the logs and its priorities.

    \note Upon any errors returns NULL.
*/
Matrix *new_frag_matrix(uint32_t matrix_rows, uint32_t matrix_collumns, Logger *logger)
{
    Error error = {0};

    Matrix *matrix = construct_matrix(
        construct,
        destruct,
        new,
        clean,
        resize,
        reset,
        copy_into,
        print,
        rows,
        collumns,
        order,
        push,
        pull,
        multiply_scalar,
        multiply,
        determinant,
        transposed,
        minorized,
        place_signify,
        cofactor,
        adjoint,
        inverse);
    if (matrix == NULL)
    {
        goto return_error;
    }

    FragMatrix *fragMatrix = (FragMatrix *)(matrix->matrix);

    if (logger != NULL)
    {
        fragMatrix->logger   = logger;
        fragMatrix->FUNC_log = logger->FUNC_log;
    }
    else
    {
        fragMatrix->FUNC_log = simple_logger;
    }

    error = matrix->FUNC_resize(matrix, matrix_rows, matrix_collumns);
    if (error.error != OO_OK)
    {
        fragMatrix->FUNC_log(
            fragMatrix->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return matrix;

return_error:
    if (matrix != NULL)
    {
        matrix->FUNC_destruct(matrix);
    }

    return NULL;
}

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param self Object that invoked the method.
    \param rows Quantity of rows that matrix should have.
    \param collumns Quantity of collumns that matrix should have. 

    \note Upon any errors returns NULL.
*/
Matrix *new(Matrix *self, uint32_t matrix_rows, uint32_t matrix_collumns)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    return new_frag_matrix(matrix_rows, matrix_collumns, myself->logger);
}

/*!
    \brief Allocate any necessary memory and make variable initializations.

    \note Upon any errors returns NULL.
*/
void *construct()
{
    return calloc(1, sizeof(FragMatrix));
}

/*!
    \brief Completely destruct the invoker interface and object memory allocations.

    \param self Object that invoked the method.
*/
static Error destruct(Matrix *self)
{
    if (self != NULL)
    {
        if (self->matrix != NULL)
        {
            // FragMatrix *myself = (FragMatrix *)(self->matrix);

            reset(self);

            free(self->matrix);
            self->matrix = NULL;
        }

        free(self);
        self = NULL;
    }

    return OO_SUCCESS;
}

/*!
    \brief Clean the matrix by inserting 0 in all positions.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error clean(Matrix *self)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error = {0};

    for (uint32_t i = 0; i < myself->rows; i++)
    {
        for (uint32_t j = 0; j < myself->collumns; j++)
        {
            error = push(self, 0, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Allocates the necessary memory for the new matrix size.

    \param self Object that invoked the method.
    \param rows Quantity of rows that matrix should have.
    \param collumns Quantity of collumns that matrix should have. 

    \warning Process will always target self, beeing destructive towards the original data.

    \note Update outter, inner, rows, collumns and order fields.
*/
static Error resize(Matrix *self, uint32_t rows, uint32_t collumns)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error    error     = {0};
    uint32_t positions = rows * collumns;
    uint32_t allocated = 0;

    if (positions == 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "non-dimensional matrix",
            .error       = INVALID_ARGUMENT,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (myself->rows != rows || myself->collumns != collumns)
    {
        reset(self);

        myself->rows     = rows;
        myself->collumns = collumns;

        myself->outter = ((uint32_t)pow(
            OUTTER_FACTOR * positions * (sizeof(float) / sizeof(float *)), 0.5));

        if (myself->outter == 0)
        {
            myself->outter++;
        }

        myself->inner = positions / myself->outter;

        for (; true;)
        {
            if (myself->outter * myself->inner > positions + myself->inner)
            {
                myself->outter--;
            }
            else if (myself->outter * myself->inner < positions)
            {
                myself->outter++;
            }
            else
            {
                break;
            }
        }

        myself->position = (float **)malloc(myself->outter * sizeof(float *));
        if (myself->position == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_MATRIX + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        for (uint32_t i = 0; i < myself->outter; i++)
        {
            if (allocated == positions)
            {
                break;
            }
            else if (allocated + myself->inner > positions)
            {
                myself->position[i] =
                    (float *)malloc((positions - allocated) * sizeof(float));
                allocated += positions - allocated;
            }
            else
            {
                myself->position[i] = (float *)malloc(myself->inner * sizeof(float));
                allocated += myself->inner;
            }

            if (myself->position[i] == NULL)
            {
                uint32_t line = __LINE__;

                error = (Error){
                    .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                    .error       = FAILED_MEMORY_ALLOCATION,
                    .file_line   = FRAG_MATRIX + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
        }

        if (myself->rows == myself->collumns)
        {
            myself->order = myself->rows;
        }
        else
        {
            myself->order = 0;
        }
    }

    error = clean(self);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Free all allocated positions, safely preparing FragMatrix to recieve new allocations.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.

    \note Update rows and collumns quantity to 0.
*/
static Error reset(Matrix *self)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    if (myself != NULL)
    {
        if (myself->position != NULL)
        {
            for (uint32_t i = 0; i < myself->outter; i++)
            {
                if (myself->position[i] != NULL)
                {
                    free(myself->position[i]);
                    myself->position[i] = NULL;
                }
            }
            free(myself->position);
            myself->position = NULL;
        }

        myself->outter = 0;
        myself->inner  = 0;

        myself->rows     = 0;
        myself->collumns = 0;
        myself->order    = 0;
    }

    return OO_SUCCESS;
}

/*!
    \brief Make a copy of the invoker.

    \param self Object that invoked the method.
    \param destiny Object that will recieve the copy.
*/
static Error copy_into(Matrix *self, Matrix *destiny)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error = {0};
    float value = 0;

    if (destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = destiny->FUNC_resize(destiny, myself->rows, myself->collumns);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 0; i < myself->rows; i++)
    {
        for (uint32_t j = 0; j < myself->collumns; j++)
        {
            error = pull(self, &value, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = destiny->FUNC_push(destiny, value, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Print the matrix in a stylized format.

    \param self Object that invoked the method.
*/
static Error print(Matrix *self)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error = {0};
    float value = 0;

    printf("|");
    for (uint32_t i = 0; i < myself->collumns; i++)
    {
        printf("-----------");
        if (i < myself->collumns - 1)
        {
            printf("-");
        }
    }
    printf("|\n");

    for (uint32_t i = 0; i < myself->rows; i++)
    {
        printf("|");
        for (uint32_t j = 0; j < myself->collumns; j++)
        {
            error = pull(self, &value, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            printf("%11.4f|", value);
        }
        printf("\n");
        if (i < myself->rows - 1)
        {
            for (uint32_t j = 0; j < myself->collumns; j++)
            {
                printf("|-----------");
            }
            printf("|\n");
        }
    }

    printf("|");
    for (uint32_t i = 0; i < myself->collumns; i++)
    {
        printf("-----------");
        if (i < myself->collumns - 1)
        {
            printf("-");
        }
    }
    printf("|\n");

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Getter for the rows quantity of the matrix.

    \param self Object that invoked the method.
*/
static uint32_t rows(Matrix *self)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    return myself->rows;
}

/*!
    \brief Getter for the collumns quantity of the matrix.

    \param self Object that invoked the method.
*/
static uint32_t collumns(Matrix *self)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    return myself->collumns;
}

/*!
    \brief Getter for the order of the matrix.

    \param self Object that invoked the method.

    \note Order is an aspect of square matrices, 0 signal that it is not a square matrix.
*/
static uint32_t order(Matrix *self)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    return myself->order;
}

/*!
    \brief Add a value to a position.

    \param self Object that invoked the method.
    \param value Value to be inserted into the matrix.
    \param row Desired row.
    \param collumn Desired collumn.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error push(Matrix *self, float value, uint32_t row, uint32_t collumn)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error    error = {0};
    uint32_t outer = 0;
    uint32_t inner = 0;

    if ((myself->rows <= row || myself->collumns <= collumn))
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "access out of boundaries",
            .error       = OUT_BOUNDARIES,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = position(myself, &outer, &inner, row, collumn);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    myself->position[outer][inner] = value;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Extract a value from position.

    \param self Object that invoked the method.
    \param value Pointer to the variable that will hold the extracted value.
    \param row Desired row.
    \param collumn Desired collumn.
*/
static Error pull(Matrix *self, float *value, uint32_t row, uint32_t collumn)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error    error = {0};
    uint32_t outer = 0;
    uint32_t inner = 0;

    if (value == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (myself->rows <= row || myself->collumns <= collumn)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "access out of boundaries",
            .error       = OUT_BOUNDARIES,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = position(myself, &outer, &inner, row, collumn);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    *value = myself->position[outer][inner];

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Multiply the matrix by a scalar number.

    \param self Object that invoked the method.
    \param scalar Value to be multiplied.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error multiply_scalar(Matrix *self, float scalar)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error = {0};
    float value = 0;

    for (uint32_t i = 0; i < myself->rows; i++)
    {
        for (uint32_t j = 0; j < myself->collumns; j++)
        {
            error = pull(self, &value, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            value *= scalar;

            error = push(self, value, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Multiply two matrices, always self X multiplier.

    \param self Object that invoked the method.
    \param multiplier Object that will be multiplied.
    \param destiny Object that will hold the result of the multiplication.

    \warning Keep in mind that which is first matrix complety changes the result.
    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.

    \note If destiny is a valid object, it will be resized to the expected size internally.
*/
static Error multiply(Matrix *self, Matrix *multiplier, Matrix *destiny)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error       = {0};
    bool  modify_self = false;

    if (multiplier == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (myself->collumns != multiplier->FUNC_rows(multiplier))
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "matrix multiplication must have AxN * NxB sizes",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (destiny == NULL)
    {
        modify_self = true;
        destiny     = new_frag_matrix(
            myself->rows, multiplier->FUNC_collumns(multiplier), myself->logger);
        if (destiny == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_MATRIX + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }
    else
    {
        error = destiny->FUNC_resize(
            destiny, myself->rows, multiplier->FUNC_collumns(multiplier));
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    for (uint32_t i = 0; i < myself->rows; i++)
    {
        for (uint32_t j = 0; j < multiplier->FUNC_collumns(multiplier); j++)
        {
            float result = 0;

            for (uint32_t k = 0; k < multiplier->FUNC_rows(multiplier); k++)
            {
                float collumn = 0;
                float row     = 0;

                error = pull(self, &collumn, i, k);
                if (error.error != OO_OK)
                {
                    myself->FUNC_log(
                        myself->logger,
                        DEBUG_PRIORITY,
                        ERROR_STRING.HEADER_RECEIVED,
                        __FILENAME__,
                        __LINE__,
                        error.file_line,
                        error.description);

                    goto return_error;
                }

                error = multiplier->FUNC_pull(multiplier, &row, k, j);
                if (error.error != OO_OK)
                {
                    myself->FUNC_log(
                        myself->logger,
                        DEBUG_PRIORITY,
                        ERROR_STRING.HEADER_RECEIVED,
                        __FILENAME__,
                        __LINE__,
                        error.file_line,
                        error.description);

                    goto return_error;
                }

                result += collumn * row;
            }

            error = destiny->FUNC_push(destiny, result, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }

    if (modify_self == true)
    {
        error = destiny->FUNC_copy_into(destiny, self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
        destiny->FUNC_destruct(destiny);
    }

    return OO_SUCCESS;

return_error:
    if (destiny != NULL && modify_self == true)
    {
        destiny->FUNC_destruct(destiny);
    }

    return error;
}

/*!
    \brief Calculated the determinant of the matrix.

    \param self Object that invoked the method.
    \param determinant Pointer to the variable that will hold the value of the determinant.

    \note Determinant is an aspect of square matrices, any non square matrices will return an error.
    \note It is a circular call to cofactor and minorize until a 2x2 matrix is produced. Very taxing in greater matrices.
*/
static Error determinant(Matrix *self, float *determinant)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error    error          = {0};
    Matrix  *cofactorMatrix = NULL;
    uint32_t line           = 0;

    if (determinant == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    switch (myself->order)
    {
    case 0:
        line = __LINE__;

        error = (Error){
            .description = "determinant can only be applied to square matrix",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;

    case 1:
        error = pull(self, determinant, 0, 0);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
        break;

    default:
        cofactorMatrix = new_frag_matrix(myself->order, myself->order, myself->logger);
        if (cofactorMatrix == NULL)
        {
            line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_MATRIX + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        error = cofactor(self, cofactorMatrix);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        for (uint32_t i = 0; i < myself->order; i++)
        {
            float cofactor = 0;
            float value    = 0;

            error = cofactorMatrix->FUNC_pull(cofactorMatrix, &cofactor, i, 0);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = pull(self, &value, i, 0);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            *determinant += value * cofactor;
        }

        cofactorMatrix->FUNC_destruct(cofactorMatrix);
        break;
    }

    return OO_SUCCESS;

return_error:
    if (cofactorMatrix != NULL)
    {
        cofactorMatrix->FUNC_destruct(cofactorMatrix);
    }

    return error;
}

/*!
    \brief Transpose the matrix, sizing a ixj into a jxi.

    \param self Object that invoked the method.
    \param destiny Object that will hold the result of the multiplication.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.

    \note If destiny is a valid object, it will be resized to the expected size internally.
*/
static Error transposed(Matrix *self, Matrix *destiny)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error       = {0};
    bool  modify_self = false;
    float value       = 0;

    if (destiny == NULL)
    {
        modify_self = true;
        destiny     = new_frag_matrix(myself->collumns, myself->rows, myself->logger);
        if (destiny == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_MATRIX + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }
    else
    {
        error = destiny->FUNC_resize(destiny, myself->collumns, myself->rows);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    for (uint32_t i = 0; i < myself->rows; i++)
    {
        for (uint32_t j = 0; j < myself->collumns; j++)
        {
            error = pull(self, &value, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = destiny->FUNC_push(destiny, value, j, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }

    if (modify_self == true)
    {
        error = destiny->FUNC_copy_into(destiny, self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
        destiny->FUNC_destruct(destiny);
    }

    return OO_SUCCESS;

return_error:
    if (destiny != NULL && modify_self == true)
    {
        destiny->FUNC_destruct(destiny);
    }

    return error;
}

/*!
    \brief Produce the matrix with all the fields with its calculated minorized values.

    \param self Object that invoked the method.
    \param destiny Object that will hold the result of the multiplication.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.

    \note If destiny is a valid object, it will be resized to the expected size internally.
    \note It is a circular call to determinat and cofactor until a 2x2 matrix is produced. Very taxing in greater matrices.
*/
static Error minorized(Matrix *self, Matrix *destiny)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error   error        = {0};
    Matrix *minorMatrix  = NULL;
    uint8_t m_correction = 0;
    uint8_t n_correction = 0;
    bool    modify_self  = false;

    if (myself->order <= 1)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "matrix 1x1 cannot be minorized",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    minorMatrix = new_frag_matrix(myself->order - 1, myself->order - 1, myself->logger);
    if (minorMatrix == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (destiny == NULL)
    {
        modify_self = true;

        destiny = new_frag_matrix(myself->rows, myself->collumns, myself->logger);
        if (destiny == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_MATRIX + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }
    else
    {
        error = destiny->FUNC_resize(destiny, myself->rows, myself->collumns);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    for (uint32_t m = 0; m < myself->order; m++)
    {
        for (uint32_t n = 0; n < myself->order; n++)
        {
            float determinant = 0;
            m_correction      = 0;
            for (uint32_t i = 0; i < myself->order - 1; i++)
            {
                if (i >= m)
                {
                    m_correction = 1;
                }

                n_correction = 0;
                for (uint32_t j = 0; j < myself->order - 1; j++)
                {
                    if (j >= n)
                    {
                        n_correction = 1;
                    }

                    float value = 0;

                    error = pull(self, &value, i + m_correction, j + n_correction);
                    if (error.error != OO_OK)
                    {
                        myself->FUNC_log(
                            myself->logger,
                            DEBUG_PRIORITY,
                            ERROR_STRING.HEADER_RECEIVED,
                            __FILENAME__,
                            __LINE__,
                            error.file_line,
                            error.description);

                        goto return_error;
                    }

                    error = minorMatrix->FUNC_push(minorMatrix, value, i, j);
                    if (error.error != OO_OK)
                    {
                        myself->FUNC_log(
                            myself->logger,
                            DEBUG_PRIORITY,
                            ERROR_STRING.HEADER_RECEIVED,
                            __FILENAME__,
                            __LINE__,
                            error.file_line,
                            error.description);

                        goto return_error;
                    }
                }
            }

            error = destiny->FUNC_pull(destiny, &determinant, m, n);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = minorMatrix->FUNC_determinant(minorMatrix, &determinant);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = destiny->FUNC_push(destiny, determinant, m, n);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }

    minorMatrix->FUNC_destruct(minorMatrix);
    minorMatrix = NULL;

    if (modify_self == true)
    {
        error = destiny->FUNC_copy_into(destiny, self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        destiny->FUNC_destruct(destiny);
    }

    return OO_SUCCESS;

return_error:
    if (destiny != NULL && modify_self == true)
    {
        destiny->FUNC_destruct(destiny);
    }

    if (minorMatrix != NULL)
    {
        minorMatrix->FUNC_destruct(minorMatrix);
    }

    return error;
}

/*!
    \brief Multiply each field of the matrix by the universal place sign(1 or -1), inverting the signal of multiple fields.

    \param self Object that invoked the method.
    \param destiny Object that will hold the result of the multiplication.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.

    \note If destiny is a valid object, it will be resized to the expected size internally.
*/
static Error place_signify(Matrix *self)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error  error    = {0};
    int8_t cofactor = 0;
    float  value    = 0;

    for (uint32_t i = 0; i < myself->order; i++)
    {
        cofactor = pow(-1, i);

        for (uint32_t j = 0; j < myself->order; j++)
        {
            error = pull(self, &value, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            value *= cofactor;

            error = push(self, value, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            cofactor *= -1;
        }
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Produce the cofactor matrix, that is the combination of its minorized matrix and place sign process.

    \param self Object that invoked the method.
    \param destiny Object that will hold the result of the multiplication.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.

    \note If destiny is a valid object, it will be resized to the expected size internally.
    \note It is a circular call to minorize and determinat until a 2x2 matrix is produced. Very taxing in greater matrices.
*/
static Error cofactor(Matrix *self, Matrix *destiny)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error       = {0};
    bool  modify_self = false;

    if (destiny == NULL)
    {
        modify_self = true;

        destiny = new_frag_matrix(myself->rows, myself->collumns, myself->logger);
        if (destiny == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_MATRIX + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }
    else
    {
        error = destiny->FUNC_resize(destiny, myself->rows, myself->collumns);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    error = minorized(self, destiny);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = destiny->FUNC_place_signify(destiny);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (modify_self == true)
    {
        error = destiny->FUNC_copy_into(destiny, self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
        destiny->FUNC_destruct(destiny);
    }

    return OO_SUCCESS;

return_error:
    if (destiny != NULL && modify_self == true)
    {
        destiny->FUNC_destruct(destiny);
    }

    return error;
}

/*!
    \brief Produce the adjoint matrix, that is the multiplication of its transposed matrix and cofactor matrix.

    \param self Object that invoked the method.
    \param destiny Object that will hold the result of the multiplication.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.

    \note If destiny is a valid object, it will be resized to the expected size internally.
*/
static Error adjoint(Matrix *self, Matrix *destiny)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error       = {0};
    bool  modify_self = false;

    if (destiny == NULL)
    {
        modify_self = true;

        destiny = new_frag_matrix(myself->rows, myself->collumns, myself->logger);
        if (destiny == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_MATRIX + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }
    else
    {
        error = destiny->FUNC_resize(destiny, myself->rows, myself->collumns);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    error = transposed(self, destiny);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = destiny->FUNC_cofactor(destiny, NULL);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (modify_self == true)
    {
        error = destiny->FUNC_copy_into(destiny, self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
        destiny->FUNC_destruct(destiny);
    }

    return OO_SUCCESS;

return_error:
    if (destiny != NULL && modify_self == true)
    {
        destiny->FUNC_destruct(destiny);
    }

    return error;
}

/*!
    \brief Produce the inverse matrix, that is the multiplication of its inversed determinant scalar and the adjoint matrix.

    \param self Object that invoked the method.
    \param destiny Object that will hold the result of the multiplication.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, and beeing destructive towards the original data.

    \note If destiny is a valid object, it will be resized to the expected size internally.
*/
static Error inverse(Matrix *self, Matrix *destiny)
{
    FragMatrix *myself = (FragMatrix *)(self->matrix);

    Error error                = {0};
    bool  modify_self          = false;
    float inversed_determinant = 0;

    if (destiny == NULL)
    {
        modify_self = true;

        destiny = new_frag_matrix(myself->rows, myself->collumns, myself->logger);
        if (destiny == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = FRAG_MATRIX + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }
    }
    else
    {
        error = destiny->FUNC_resize(destiny, myself->rows, myself->collumns);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    error = determinant(self, &inversed_determinant);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    inversed_determinant = 1 / inversed_determinant;

    error = adjoint(self, destiny);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = destiny->FUNC_multiply_scalar(destiny, inversed_determinant);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (modify_self == true)
    {
        error = destiny->FUNC_copy_into(destiny, self);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
        destiny->FUNC_destruct(destiny);
    }

    return OO_SUCCESS;

return_error:
    if (destiny != NULL && modify_self == true)
    {
        destiny->FUNC_destruct(destiny);
    }

    return error;
}

/*!
    \brief Intern calculation for frag matrix position based on row and collums desired and fragmentation ratio.

    \param self Object that invoked the method.
    \param outter Pointer to the variable that will hold which inner array will have the desired position.
    \param inner Pointer to the variable that will hold which position of inner array is the desired position.
    \param row Desired row.
    \param collumn Desired collumn.
*/
static Error position(
    FragMatrix *myself, uint32_t *outter, uint32_t *inner, uint32_t row, uint32_t collumn)
{
    Error error = {0};

    if (myself->inner == 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "no grid has been detected to calculate requested position",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = FRAG_MATRIX + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    *outter = (uint32_t)(((row * myself->collumns) + collumn) / myself->inner);
    *inner  = (uint32_t)(((row * myself->collumns) + collumn) % myself->inner);

    return OO_SUCCESS;

return_error:
    return error;
}