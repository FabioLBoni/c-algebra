# c-algebra

## Name
C-Algebra

## Description
Project focused on creating a Object Oriented library for algebraic purposes.
Implements interfaces and concrete objects for matrix, cartesian/spherical point frames, equations and numeric methods operations.

## Usage
The project is Object Oriented and make use of interfaces. At the OO_Interfaces folder we hold all interface contructors, one per file. The interfaces types and structs declarations are all inside OO_Interfaces/interfaces.h to avoid any circular reference.

The project hold OO_Dto/dto.h to hold any anemic structs that are project wide known and commom data holders. Examples are CoordinatesCartesian and CoordinatesSpherical that are globally known to be the same thing and just carry some simple data.

The project have a pattern of error handling based on file ID and file lines wraped byErrortype. It have suport to file ID up to 4293 and files with up to 999999 lines. 
The error have its first 6 digit for the line of the file, everything after is the file ID respectively related to OO_Error/oo_error.h.

Example: 

   Errorerror = 123000597;   Errorerror = 4293999999; 
    file ID 123 at line 597.    file ID 4293 at line 999999.

The heart of the library is the EquationTerm interface. They are small blocks of variables that compose the Equation interface concrete implementations. Each EquationTerm have a unique behaviour and have the potential of change completly the Equation behaviour.

## Installation
Just import or copy its content on your C or C++ project.

## Support
Fabio Luis Boni: fabioboni96@hotmail.com

## Roadmap
Accepting any suggestions of numeric methods or algebraic procedures.
- Logarithimic equation terms.
- Composite equation terms.
- Trigonometric equation terms.  

## Long Term Objectives
- Suport for 3D algebraic calculations.
- Suport for 3D+ algebraic calculations.

## Contributing
Feel free to email fabioboni96@hotmail.com for any recomendations, tips and suggestions, or if you would like to volunteer for the developing. 

## Authors and acknowledgment
Fabio Luis Boni - Owner, Maintainer and Developer.

## Project status
Ongoing improvements at free time.

## License
This project is released under the MIT license (see LICENSE file).