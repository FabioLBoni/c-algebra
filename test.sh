#!/bin/sh
for OUTPUT in $(find ./ -name *_test.sh )
do
    echo "$OUTPUT"

    DIRECTORY="${OUTPUT%/*}"
    FILENAME="${OUTPUT##*/}"

    cd $DIRECTORY
    chmod +x $FILENAME

    ./$FILENAME
    RESULT=$?

    if [ $RESULT -ne 0 ]; then
        echo "FAILED: ${FILENAME%sh}c returned $RESULT\n\n"
        exit 1
    fi

    cd - > /dev/null
done

echo "\nALL TESTS OK"