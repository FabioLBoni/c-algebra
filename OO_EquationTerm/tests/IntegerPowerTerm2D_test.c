/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    14 de jan de 2024
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <stdio.h>
#include <stdlib.h>

#include "../../OO_Global/oo_Global.h"
#include "../c_IntegerPowerTerm2D.h"

Error test_new_integer_power_term_2d();

int main()
{
    Error error = {0};

    error = test_new_integer_power_term_2d();
    if (error.error != OO_OK)
    {
        uint32_t line = __LINE__;

        simple_logger(
            NULL,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            line,
            error.file_line,
            error.description);

        return error.error;
    }

    return 0;
}

Error test_new_integer_power_term_2d()
{
    Error                 error    = {0};
    EquationTerm         *term     = NULL;
    EquationVariableLabel variable = {
        .name = "x",
        .variable =
            (EquationVariable){
                .axis = X,
            },
    };

    term = new_integer_power_term_2d(variable, NULL);
    if (term == NULL)
    {
        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = INTEGER_POWER_TERM_2D + __LINE__,
        };

        return error;
    }

    IntegerPowerTerm2D *integerPowerTerm = ((IntegerPowerTerm2D *)term->term);

    if (integerPowerTerm->type != POLYNOMIAL_2D)
    {
        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = INTEGER_POWER_TERM_2D + __LINE__,
        };

        return error;
    }

    return OO_SUCCESS;
}