#!/bin/sh
gcc -Os "$@" IntegerPowerTerm2D_test.c \
    ../c_IntegerPowerTerm2D.c \
    ../../OO_Interfaces/i_EquationTerm.c \
    ../../OO_Error/oo_Error.c \
    ../../OO_Global/oo_Global.c \
-Wall -o test -lm 

./test

exit $?