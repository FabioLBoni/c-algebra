/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    14 de dez de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "../OO_Global/oo_Global.h"
#include "c_IntegerPowerTerm2D.h"

static EquationTerm *new(EquationVariableLabel variable_label, EquationTerm *self);

static void *construct();
static Error destruct(EquationTerm *self);

static Error change(EquationTerm *self, float core, float coeficient);
static Error copy_into(EquationTerm *self, EquationTerm **destiny);
static Error print(EquationTerm *self);

static Error aggregate(EquationTerm *self, EquationTerm *term);

static EQUATION_TERM         type(EquationTerm *self);
static float                 core(EquationTerm *self);
static float                 coeficient(EquationTerm *self);
static EquationVariableLabel variable(EquationTerm *self);

static Error calculate(EquationTerm *self, EquationVariable variable, float *result);

static Error integrate(EquationTerm *self);
static Error derivative(EquationTerm *self);
static Error multiply(EquationTerm *self, EquationTerm *term, EquationTerm *destiny);
static Error divide(EquationTerm *self, EquationTerm *term, EquationTerm *destiny);

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param logger Optional object to controll the flow of the logs and its priorities.

    \note Upon any errors returns NULL.
*/
EquationTerm *
new_integer_power_term_2d(EquationVariableLabel variable_label, Logger *logger)
{
    EquationTerm *term = construct_equation_term(
        construct,
        new,
        destruct,
        change,
        copy_into,
        print,
        aggregate,
        type,
        core,
        coeficient,
        variable,
        calculate,
        integrate,
        derivative,
        multiply,
        divide);
    if (term == NULL)
    {
        goto return_error;
    }

    IntegerPowerTerm2D *integerPowerTerm = (IntegerPowerTerm2D *)(term->term);

    if (logger != NULL)
    {
        integerPowerTerm->logger   = logger;
        integerPowerTerm->FUNC_log = logger->FUNC_log;
    }
    else
    {
        integerPowerTerm->FUNC_log = simple_logger;
    }

    if (variable_label.name == NULL)
    {
        integerPowerTerm->FUNC_log(
            integerPowerTerm->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            "variable must have a name");

        goto return_error;
    }

    if (variable_label.name[0] == '\0')
    {
        integerPowerTerm->FUNC_log(
            integerPowerTerm->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            "variable must have a name");

        goto return_error;
    }

    integerPowerTerm->variable = variable_label;
    integerPowerTerm->type     = POLYNOMIAL_2D;

    return term;

return_error:
    return NULL;
}

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param self Object that invoked the method.

    \note Upon any errors returns NULL.
*/
static EquationTerm *new(EquationVariableLabel variable_label, EquationTerm *self)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    return new_integer_power_term_2d(variable_label, myself->logger);
}

/*!
    \brief Allocate any necessary memory and make variable initializations.

    \note Upon any errors returns NULL.
*/
static void *construct()
{
    return calloc(1, sizeof(IntegerPowerTerm2D));
}

/*!
    \brief Completely destruct the invoker interface and object memory allocations.

    \param self Object that invoked the method.
*/
static Error destruct(EquationTerm *self)
{
    if (self != NULL)
    {
        if (self->term != NULL)
        {
            // IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

            free(self->term);
            self->term = NULL;
        }

        free(self);
        self = NULL;
    }

    return OO_SUCCESS;
}

/*!
    \brief Will modify completly the invoker behaviour.

    \param self Object that invoked the method.
    \param core In IntegerPowerTerm2D its de power over the variable.
    \param coeficient In IntegerPowerTerm2D its the scalar that multiply the variable.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error change(EquationTerm *self, float core, float coeficient)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    Error error = {0};

    if (core != (int64_t)core)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "laurent polynomials can only accept integer values",
            .error       = INVALID_ARGUMENT,
            .file_line   = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    myself->exponent   = core;
    myself->coeficient = coeficient;

    if (myself->exponent >= 0)
    {
        myself->type = POLYNOMIAL_2D;
    }
    else
    {
        myself->type = NEGATIVE_INTEGER_POWER_2D;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Make a copy of the invoker.

    \param self Object that invoked the method. Will have its content copied into destiny.
    \param destiny Pointer to the Object that will recieve the copy.

    \warning destiny must be a valid non NULL pointer.
    \warning destiny must point to NULL or to a valid EquationTerm object. Otherwise will lead to invalid memory access.
    
    \note destiny pointing to a valid EquationTerm object will always be destructed and re-constructed to avoid object typing conflics.
*/
static Error copy_into(EquationTerm *self, EquationTerm **destiny)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    Error         error = {0};
    EquationTerm *aux   = NULL;

    if (destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (*destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    aux = *destiny;

    *destiny = new_integer_power_term_2d(myself->variable, myself->logger);
    if (*destiny == NULL)
    {
        *destiny = aux;

        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }
    aux->FUNC_destruct(aux);

    error = (*destiny)->FUNC_change(*destiny, myself->exponent, myself->coeficient);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Print the term in a stylized format.

    \param self Object that invoked the method.
*/
static Error print(EquationTerm *self)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    if (myself->coeficient != 0)
    {
        if (myself->coeficient > 0)
        {
            printf("+");
        }

        if (myself->coeficient == -1 && myself->exponent != 0)
        {
            printf("-");
        }

        if (fabsf(myself->coeficient) != 1 || myself->exponent == 0)
        {
            printf("%.3f", myself->coeficient);
        }

        if (myself->exponent != 0)
        {
            printf("%s", myself->variable.name);

            if (myself->exponent != 1)
            {
                printf("^%d", myself->exponent);
            }
        }

        printf(" ");
    }

    return OO_SUCCESS;
}

/*!
    \brief Agregate two equal typed and cored terms if they have same AXIS variable reference.

    \param self Object that invoked the method.
    \param term Object that will be agregated by self.

    \warning Process will always target self, beeing destructive towards the original data.

    \note Returned error should be treated carefully. There is nothing wrong having two different cored terms, but will be impossible to 
    agregate them, even so, the method will return an error sinalizing the failure of the agregation.
*/
static Error aggregate(EquationTerm *self, EquationTerm *term)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    Error error = {0};

    if (term == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (type(self) != term->FUNC_type(term))
    {
        error = (Error){
            .description = "aggregation is not possible",
            .error       = IMPOSSIBLE_AGGREGATION,
            .file_line   = INTEGER_POWER_TERM_2D + __LINE__,
        };

        goto return_error;
    }

    if (myself->variable.variable.axis != term->FUNC_variable(term).variable.axis)
    {
        error = (Error){
            .description = "aggregation is not possible",
            .error       = IMPOSSIBLE_AGGREGATION,
            .file_line   = INTEGER_POWER_TERM_2D + __LINE__,
        };

        goto return_error;
    }

    if (myself->exponent != term->FUNC_core(term))
    {
        error = (Error){
            .description = "aggregation is not possible",
            .error       = IMPOSSIBLE_AGGREGATION,
            .file_line   = INTEGER_POWER_TERM_2D + __LINE__,
        };

        goto return_error;
    }

    myself->coeficient += term->FUNC_coeficient(term);

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Getter for the EquationTerm concrete implementation type.

    \param self Object that invoked the method.

    \note Used to differentiate concrete implementations of the EquationTerm interface.
*/
static EQUATION_TERM type(EquationTerm *self)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    return myself->type;
}

/*!
    \brief Getter for the core of the term.

    \param self Object that invoked the method.
*/
static float core(EquationTerm *self)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    return myself->exponent;
}

/*!
    \brief Getter for the coeficient of the term.

    \param self Object that invoked the method.
*/
static float coeficient(EquationTerm *self)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    return myself->coeficient;
}

/*!
    \brief Getter for the AXIS type of the variable that the term is expecting to recieve.

    \param self Object that invoked the method.
*/
static EquationVariableLabel variable(EquationTerm *self)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    return myself->variable;
}

/*!
    \brief Calculate the value of the term relative to a given axis value.

    \param self Object that invoked the method.
    \param point DTO that hold cartesian coordinates.
    \param givin Enum that mark what coordinates will be used from point to calculate the term.
    \param result Pointer to the variable that will hold the calculation result.
*/
static Error calculate(EquationTerm *self, EquationVariable variable, float *result)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    Error error = {0};

    if (result == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (myself->variable.variable.axis != variable.axis)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "axis does not match",
            .error       = INVALID_ARGUMENT,
            .file_line   = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    *result = myself->coeficient * powf(variable.value, myself->exponent);

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Calculate the integral of the term.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error integrate(EquationTerm *self)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    Error error = {0};

    //! \bug THIS ERROR ONLY OCCURS BECAUSE THIS INTEGRAL IS A LOGARITHYMIC TERM THAT MUST BE IMPLEMENTED
    if (myself->exponent == -1)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description =
                "integration of exponent -1 term is a unimplemented logarithimic term",
            .error     = IMPOSSIBLE_EXECUTION,
            .file_line = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    myself->exponent++;
    myself->coeficient *= 1 / myself->exponent;

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Calculate the derivative of the term.

    \param self Object that invoked the method.

    \warning Process will always target self, beeing destructive towards the original data.
*/
static Error derivative(EquationTerm *self)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    myself->coeficient *= myself->exponent;
    myself->exponent--;

    return OO_SUCCESS;
}

/*!
    \brief Multiply two terms.

    \param self Object that invoked the method, will be multiplied by term.
    \param term Object that will be multiplied by self.
    \param destiny Object that will hold the result of the multiplication.
 
    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, beeing destructive towards the original data.

    \note self can be the same reference to term (effective power by 2). 
    \note destiny can the be same reference to self or term.
*/
static Error multiply(EquationTerm *self, EquationTerm *term, EquationTerm *destiny)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    Error error      = {0};
    float exponent   = 0;
    float coeficient = 0;

    if (destiny == NULL)
    {
        destiny = self;
    }

    //! \bug THIS ERROR CAN BE REPLACED BY A MORE COMPLEX TRATATIVE, LIKE COMPOSITE TERMS
    if (type(self) != term->FUNC_type(term))
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description =
                "cannot handle multiplication of terms with different labels without unimplemented composite term",
            .error     = IMPOSSIBLE_EXECUTION,
            .file_line = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    //! \bug THIS ERROR CAN BE REPLACED BY A MORE COMPLEX TRATATIVE, LIKE COMPOSITE TERMS
    if (myself->variable.variable.axis != term->FUNC_variable(term).variable.axis)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description =
                "cannot handle multiplication of terms with different labels without unimplemented composite term",
            .error     = IMPOSSIBLE_EXECUTION,
            .file_line = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    exponent   = myself->exponent + term->FUNC_core(term);
    coeficient = myself->coeficient * term->FUNC_coeficient(term);

    error = destiny->FUNC_change(destiny, exponent, coeficient);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}

/*!
    \brief Divide two terms.

    \param self Object that invoked the method, will be divided by term.
    \param term Object that will be divided by self.
    \param destiny Object that will hold the result of the division.

    \warning Enforce destiny beeing a valid pointer or NULL. Memory garbage will lead to invalid memory access.
    \warning In case of destiny beeing NULL, process will target self, beeing destructive towards the original data.

    \note self can be the same reference to term (effective constant 1).
    \note destiny can the be same reference to self or term.
*/
static Error divide(EquationTerm *self, EquationTerm *term, EquationTerm *destiny)
{
    IntegerPowerTerm2D *myself = (IntegerPowerTerm2D *)(self->term);

    Error error      = {0};
    float exponent   = 0;
    float coeficient = 0;

    if (destiny == NULL)
    {
        destiny = self;
    }

    //! \bug THIS ERROR CAN BE REPLACED BY A MORE COMPLEX TRATATIVE, LIKE COMPOSITE TERMS
    if (type(self) != term->FUNC_type(term))
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description =
                "cannot handle multiplication of terms with different labels without unimplemented composite term",
            .error     = IMPOSSIBLE_EXECUTION,
            .file_line = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    //! \bug THIS ERROR CAN BE REPLACED BY A MORE COMPLEX TRATATIVE, LIKE COMPOSITE TERMS
    if (myself->variable.variable.axis != term->FUNC_variable(term).variable.axis)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description =
                "cannot handle multiplication of terms with different labels without unimplemented composite term",
            .error     = IMPOSSIBLE_EXECUTION,
            .file_line = INTEGER_POWER_TERM_2D + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    exponent   = myself->exponent - term->FUNC_core(term);
    coeficient = myself->coeficient / term->FUNC_coeficient(term);

    error = destiny->FUNC_change(destiny, exponent, coeficient);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    return error;
}