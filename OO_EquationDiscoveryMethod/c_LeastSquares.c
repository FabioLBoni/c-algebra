/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    01 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "../OO_Global/oo_Global.h"
#include "c_LeastSquares.h"

static void *construct();
static Error destruct(EquationDiscoveryMethod *self);

static Error equation(EquationDiscoveryMethod *self, Frame *frame, Equation *equation);

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param matrix Dependency injection of Matrix interface. 
    \param logger Optional object to controll the flow of the logs and its priorities.

    \warning Object matrix will not be controled, modified, destructed nor referenced by the new Object.
    \warning Object self will not be controled, modified, destructed nor referenced by the new Object.

    \note One of self or matrix can be NULL, but not both.
    \note Upon any errors returns NULL.
*/
EquationDiscoveryMethod *new_least_squares(Matrix *matrix, Logger *logger)
{
    EquationDiscoveryMethod *equationDiscoveryMethod =
        construct_equation_discovery_method(construct, destruct, equation);
    if (equationDiscoveryMethod == NULL)
    {
        goto return_error;
    }

    LeastSquares *leastSquares = (LeastSquares *)(equationDiscoveryMethod->method);

    if (logger != NULL)
    {
        leastSquares->logger   = logger;
        leastSquares->FUNC_log = logger->FUNC_log;
    }
    else
    {
        leastSquares->FUNC_log = simple_logger;
    }

    if (matrix == NULL)
    {
        leastSquares->FUNC_log(
            leastSquares->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            "argument pointer is NULL");

        goto return_error;
    }

    leastSquares->theta = matrix->FUNC_new(matrix, 1, 1);
    if (leastSquares->theta == NULL)
    {
        leastSquares->FUNC_log(
            leastSquares->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            __LINE__,
            __func__,
            ERROR_STRING.FAILED_MEMORY_ALLOCATION);

        goto return_error;
    }

    return equationDiscoveryMethod;

return_error:
    if (equationDiscoveryMethod != NULL)
    {
        equationDiscoveryMethod->FUNC_destruct(equationDiscoveryMethod);
    }

    return NULL;
}

/*!
    \brief Allocate any necessary memory and make variable initializations.

    \note Upon any errors returns NULL;
*/
static void *construct()
{
    return calloc(1, sizeof(LeastSquares));
}

/*!
    \brief Completely destruct the invoker interface and object memory allocations.

    \param self Object that invoked the method.
*/
static Error destruct(EquationDiscoveryMethod *self)
{
    if (self != NULL)
    {
        if (self->method != NULL)
        {
            LeastSquares *myself = (LeastSquares *)(self->method);

            if (myself->theta != NULL)
            {
                myself->theta->FUNC_destruct(myself->theta);
                myself->theta = NULL;
            }

            free(self->method);
            self->method = NULL;
        }

        free(self);
        self = NULL;
    }

    return OO_SUCCESS;
}

/*!
    \brief Will execute least squares numeric method to discover a equation from a collection of points.

    \param self Object that invoked the method.
    \param frame Object containing the collection of points to be analized by the method.
    \param equation Object that will hold the resultant equation that the method discover.

    \warning equation needs to have prepared terms pre-execution, so the least squares method knows what is expected as result.
*/
static Error equation(EquationDiscoveryMethod *self, Frame *frame, Equation *equation)
{
    LeastSquares *myself = (LeastSquares *)(self->method);

    Error                error          = {0};
    uint16_t             equation_terms = 0;
    float                coeficient     = 0;
    float                term_result    = 0;
    Matrix              *y_axis         = NULL;   // points x 1
    Matrix              *phi            = NULL;   // points x terms
    Matrix              *phiT           = NULL;   // terms  x points
    Matrix              *phiResultant   = NULL;
    CoordinatesCartesian point          = {0};
    EquationVariable     variable       = {0};
    EquationTerm       **terms = (EquationTerm **)calloc(1, sizeof(EquationTerm *));

    if (terms == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = LEAST_SQUARES + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = equation->FUNC_get_terms(equation, &terms, &equation_terms);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (equation_terms == 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "equation has no term",
            .error       = INVALID_ARGUMENT,
            .file_line   = LEAST_SQUARES + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    y_axis = myself->theta->FUNC_new(myself->theta, frame->FUNC_quantity(frame), 1);
    if (y_axis == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = LEAST_SQUARES + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    phi = myself->theta->FUNC_new(
        myself->theta, frame->FUNC_quantity(frame), equation_terms);
    if (phi == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = LEAST_SQUARES + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    for (uint32_t i = 0; i < frame->FUNC_quantity(frame); i++)
    {
        error = frame->FUNC_pull_cartesian(frame, &point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = y_axis->FUNC_push(y_axis, point.y, i, 0);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        for (uint16_t j = 0; j < equation_terms; j++)
        {
            error = terms[j]->FUNC_change(terms[j], terms[j]->FUNC_core(terms[j]), 1);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            variable       = terms[j]->FUNC_variable(terms[j]).variable;
            variable.value = point.x;

            error = terms[j]->FUNC_calculate(terms[j], variable, &term_result);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = phi->FUNC_push(phi, term_result, i, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }
        }
    }

    // phi->FUNC_print(phi);

    phiT = phi->FUNC_new(phi, equation_terms, frame->FUNC_quantity(frame));
    if (phiT == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = LEAST_SQUARES + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = phi->FUNC_transposed(phi, phiT);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    phiResultant = phi->FUNC_new(phi, 1, 1);
    if (phiResultant == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = LEAST_SQUARES + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = phiT->FUNC_multiply_matrix(phiT, phi, phiResultant);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    phi->FUNC_destruct(phi);
    phi = NULL;

    error = phiResultant->FUNC_inverse(phiResultant, NULL);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = phiResultant->FUNC_multiply_matrix(phiResultant, phiT, NULL);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    phiT->FUNC_destruct(phiT);
    phiT = NULL;

    error = phiResultant->FUNC_multiply_matrix(phiResultant, y_axis, myself->theta);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    y_axis->FUNC_destruct(y_axis);
    phiResultant->FUNC_destruct(phiResultant);
    y_axis       = NULL;
    phiResultant = NULL;

    for (uint16_t i = 0; i < equation_terms; i++)
    {
        error = myself->theta->FUNC_pull(myself->theta, &coeficient, i, 0);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error =
            terms[i]->FUNC_change(terms[i], terms[i]->FUNC_core(terms[i]), coeficient);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    free(terms);
    return OO_SUCCESS;

return_error:
    if (terms != NULL)
    {
        free(terms);
    }

    if (y_axis != NULL)
    {
        y_axis->FUNC_destruct(y_axis);
    }

    if (phi != NULL)
    {
        phi->FUNC_destruct(phi);
    }

    if (phiT != NULL)
    {
        phiT->FUNC_destruct(phiT);
    }

    if (phiResultant != NULL)
    {
        phiResultant->FUNC_destruct(phiResultant);
    }

    return error;
}
