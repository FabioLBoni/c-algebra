/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    14 de ago de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "../OO_Equation/c_CompositeEquation.h"

#include "../OO_Global/oo_Global.h"
#include "c_NewtonRaphson.h"

static void *construct();
static Error destruct(MeetingPointMethod *self);

static Error meeting_point(
    MeetingPointMethod *self, Frame *frame, Equation *equation1, Equation *equation2);

static Error prepare_equation(
    MeetingPointMethod *self,
    Equation           *equation1,
    Equation           *equation2,
    Equation           *destiny);
static Error find_roots(MeetingPointMethod *self, Equation *equation, Frame *roots);
static Error find_line_root(MeetingPointMethod *self, Equation *equation, Frame *roots);
static Error
find_quadratic_roots(MeetingPointMethod *self, Equation *equation, Frame *roots);
static Error
find_polynomial_roots(MeetingPointMethod *self, Equation *equation, Frame *roots);
static Error bhaskara(
    MeetingPointMethod *self,
    Equation           *equation,
    float               x_roots[2],
    uint8_t            *root_quantity);

static bool is_line_equation(EquationTerm *terms[], uint16_t terms_quantity);
static bool is_quadratic_equation(EquationTerm *terms[], uint16_t terms_quantity);
static bool is_polynomial_equation(EquationTerm *terms[], uint16_t terms_quantity);

/*!
    \brief Build the interface and link it to the concrete implementation. It is also responsible for any dependencies injection.

    \param logger Optional object to controll the flow of the logs and its priorities.

    \note Upon any errors returns NULL.
*/
MeetingPointMethod *new_newton_raphson(Logger *logger)
{
    MeetingPointMethod *meetingPointMethod =
        construct_meeting_point_method(construct, destruct, meeting_point);
    if (meetingPointMethod == NULL)
    {
        goto return_error;
    }

    NewtonRaphson *newtonRaphson = (NewtonRaphson *)(meetingPointMethod->method);

    if (logger != NULL)
    {
        newtonRaphson->logger   = logger;
        newtonRaphson->FUNC_log = logger->FUNC_log;
    }
    else
    {
        newtonRaphson->FUNC_log = simple_logger;
    }

    return meetingPointMethod;

return_error:
    return NULL;
}

/*!
    \brief Allocate any necessary memory and make variable initializations.

    \note Upon any errors returns NULL;
*/
void *construct()
{
    return calloc(1, sizeof(NewtonRaphson));
}

/*!
    \brief Completely destruct the invoker interface and object memory allocations.

    \param self Object that invoked the method.
*/
static Error destruct(MeetingPointMethod *self)
{
    if (self != NULL)
    {
        if (self->method != NULL)
        {
            // NewtonRaphson *myself = (NewtonRaphson *)(self->method);

            free(self->method);
            self->method = NULL;
        }

        free(self);
        self = NULL;
    }

    return OO_SUCCESS;
}

/*!
    \brief Find the meeting points between two curve equations.

    \param self Object that invoked the method.
    \param frame Object that will hold the collection of meeting points found by method.
    \param equation1 Object contains one of the equations to be analyzed.
    \param equation2 Object contains one of the equations to be analyzed.

    \warning frame will be reseted, beeing destructive towards any existent data.
*/
static Error meeting_point(
    MeetingPointMethod *self, Frame *frame, Equation *equation1, Equation *equation2)
{
    NewtonRaphson *myself = (NewtonRaphson *)(self->method);

    Error                error    = {0};
    Equation            *equation = NULL;
    CoordinatesCartesian point    = {0};
    EquationVariable     variable = {
            .axis = X,
    };

    if (frame == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (equation1 == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (equation2 == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    equation =
        equation1->FUNC_new(equation1, equation1->FUNC_dependent_variable(equation1));
    if (equation == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = prepare_equation(self, equation1, equation2, equation);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = find_roots(self, equation, frame);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    equation->FUNC_destruct(equation);
    equation = NULL;

    for (uint32_t i = 0; i < frame->FUNC_quantity(frame); i++)
    {
        error = frame->FUNC_pull_cartesian(frame, &point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        variable.value = point.x;

        error = equation1->FUNC_calculate(equation1, &variable, 1, &point.y);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = frame->FUNC_position_push_cartesian(frame, point, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    return OO_SUCCESS;

return_error:
    if (equation != NULL)
    {
        equation->FUNC_destruct(equation);
    }

    return error;
}

/*!
    \brief Prepare the equation by subtracting equation2 from equation1.

    \param logger Optional object to controll the flow of the logs and its priorities.
    \param equation1 Object contains one of the equations to be analyzed.
    \param equation2 Object contains one of the equations to be analyzed.
    \param destiny Object that will hold the resulting equation.
*/
Error prepare_equation(
    MeetingPointMethod *self, Equation *equation1, Equation *equation2, Equation *destiny)
{
    NewtonRaphson *myself = (NewtonRaphson *)(self->method);

    Error          error = {0};
    uint16_t       terms_quantity;
    EquationTerm **terms = calloc(1, sizeof(EquationTerm *));
    EquationTerm  *term  = NULL;

    if (equation1 == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (equation2 == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (destiny == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = equation1->FUNC_get_terms(equation1, &terms, &terms_quantity);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (terms_quantity == 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "equation has no term",
            .error       = INVALID_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    for (uint16_t i = 0; i < terms_quantity; i++)
    {
        error = destiny->FUNC_add_term(destiny, terms[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    error = equation2->FUNC_get_terms(equation2, &terms, &terms_quantity);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (terms_quantity == 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "equation has no term",
            .error       = INVALID_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    for (uint16_t i = 0; i < terms_quantity; i++)
    {
        if (term == NULL || term->FUNC_type(term) != terms[i]->FUNC_type(terms[i]))
        {
            if (term != NULL)
            {
                term->FUNC_destruct(term);
            }

            term = terms[i]->FUNC_new(terms[i]->FUNC_variable(terms[i]), terms[i]);
            if (term == NULL)
            {
                uint32_t line = __LINE__;

                error = (Error){
                    .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                    .error       = FAILED_MEMORY_ALLOCATION,
                    .file_line   = NEWTON_RAPHSON + line,
                };

                myself->FUNC_log(
                    myself->logger,
                    ERROR_PRIORITY,
                    ERROR_STRING.HEADER_PRODUCED,
                    __FILENAME__,
                    line,
                    __func__,
                    error.description);

                goto return_error;
            }
        }

        error = term->FUNC_change(
            term, term->FUNC_core(terms[i]), -term->FUNC_coeficient(terms[i]));
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = destiny->FUNC_add_term(destiny, term);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    free(terms);
    term->FUNC_destruct(term);
    terms = NULL;
    term  = NULL;

    error = destiny->FUNC_ordenate(destiny);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    return OO_SUCCESS;

return_error:
    if (terms != NULL)
    {
        free(terms);
    }

    if (term != NULL)
    {
        term->FUNC_destruct(term);
    }

    return error;
}

/*!
    \brief Recursive method for finding roots of an equation.

    \param logger Optional object to controll the flow of the logs and its priorities.
    \param equation Object to be analyzed for its roots.
    \param frame Object that will hold the collection of roots found.

    \warning frame will be reseted, beeing destructive towards any data existent data.
*/
static Error find_roots(MeetingPointMethod *self, Equation *equation, Frame *roots)
{
    NewtonRaphson *myself = (NewtonRaphson *)(self->method);

    Error          error          = {0};
    EquationTerm **terms          = (EquationTerm **)calloc(1, sizeof(EquationTerm *));
    uint16_t       terms_quantity = 0;

    printf("\n");

    if (roots == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (equation == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (terms == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = equation->FUNC_ordenate(equation);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    equation->FUNC_print(equation);

    error = equation->FUNC_get_terms(equation, &terms, &terms_quantity);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (terms_quantity == 0
        || (terms_quantity == 1 && terms[0]->FUNC_core(terms[0]) == CONSTANT))
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "constant equation cannot have roots",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (is_line_equation(terms, terms_quantity))
    {
        error = find_line_root(self, equation, roots);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        goto return_success;
    }

    if (is_quadratic_equation(terms, terms_quantity))
    {
        error = find_quadratic_roots(self, equation, roots);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        goto return_success;
    }

    if (is_polynomial_equation(terms, terms_quantity))
    {
        error = find_polynomial_roots(self, equation, roots);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        goto return_success;
    }
    else
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "unimplemented solution",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

return_success:
    printf("\n");
    equation->FUNC_print(equation);
    roots->FUNC_print(roots);
    printf("\n");

    free(terms);

    return OO_SUCCESS;

return_error:
    if (terms != NULL)
    {
        free(terms);
    }

    return error;
}

static Error find_line_root(MeetingPointMethod *self, Equation *equation, Frame *roots)
{
    NewtonRaphson *myself = (NewtonRaphson *)(self->method);

    Error                error = {0};
    CoordinatesCartesian root  = {0};
    EquationTerm       **terms = (EquationTerm **)calloc(1, sizeof(EquationTerm *));
    uint16_t             terms_quantity   = 0;
    bool                 is_line_equation = false;
    EquationVariable     variable         = {
                    .axis = X,
    };

    if (roots == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (equation == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (terms == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    equation->FUNC_aggregate(equation);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = equation->FUNC_get_terms(equation, &terms, &terms_quantity);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    is_line_equation = (terms[0]->FUNC_type(terms[0]) == POLYNOMIAL_2D
                        && terms[0]->FUNC_core(terms[0]) == LINE)
                    && (terms_quantity == 1
                        || (terms_quantity == 2
                            && (terms[1]->FUNC_type(terms[1]) == POLYNOMIAL_2D
                                && terms[1]->FUNC_core(terms[1]) == CONSTANT)));
    if (is_line_equation == false)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "it is not a line equation",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (terms[0]->FUNC_coeficient(terms[0]) == 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.DIVISION_BY_ZERO,
            .error       = DIVISION_BY_ZERO,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (terms_quantity > 1)
    {
        root.x = -terms[1]->FUNC_coeficient(terms[1]);
    }
    root.x /= terms[0]->FUNC_coeficient(terms[0]);

    variable.value = root.x;

    free(terms);
    terms = NULL;

    error = equation->FUNC_calculate(equation, &variable, 1, &root.y);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = roots->FUNC_resize(roots, 1);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = roots->FUNC_push_cartesian(roots, root);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

return_error:
    if (terms != NULL)
    {
        free(terms);
    }

    return error;
}

static Error
find_quadratic_roots(MeetingPointMethod *self, Equation *equation, Frame *roots)
{
    NewtonRaphson *myself = (NewtonRaphson *)(self->method);

    Error                error       = {0};
    float                x_roots[2]  = {0};
    uint8_t              roots_found = 0;
    CoordinatesCartesian root[2]     = {0};
    EquationVariable     variable    = {
               .axis = X,
    };

    if (roots == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (equation == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = bhaskara(self, equation, x_roots, &roots_found);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (roots_found != 0)
    {
        error = roots->FUNC_resize(roots, roots_found);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    for (uint8_t i = 0; i < roots_found; i++)
    {
        root[i].x      = x_roots[i];
        variable.value = root[i].x;

        error = equation->FUNC_calculate(equation, &variable, 1, &root[i].y);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = roots->FUNC_push_cartesian(roots, root[i]);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

return_error:
    return error;
}

static Error
find_polynomial_roots(MeetingPointMethod *self, Equation *equation, Frame *roots)
{
    NewtonRaphson *myself = (NewtonRaphson *)(self->method);

    Error                error                 = {0};
    Frame               *extremums             = NULL;
    Equation            *derivative            = NULL;
    Equation            *secondDerivative      = NULL;
    CoordinatesCartesian extremum              = {0};
    CoordinatesCartesian next_extremum         = {0};
    CoordinatesCartesian previous_extremum     = {0};
    CoordinatesCartesian aproximation          = {0};
    CoordinatesCartesian tangent               = {0};
    CoordinatesCartesian concavity             = {0};
    float                next_aproximation     = 0;
    bool                 has_previous_extremum = false;
    bool                 has_next_extremum     = false;
    bool                 is_pseudo_extremum    = false;
    uint32_t             extremum_quantity     = 0;
    uint16_t             terms_quantity        = 0;
    EquationTerm       **terms                 = NULL;
    EquationVariable     variable              = {
                         .axis = X,
    };

    if (roots == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (equation == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    extremums = roots->FUNC_new(roots, 1);
    if (extremums == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    derivative =
        equation->FUNC_new(equation, equation->FUNC_dependent_variable(equation));
    if (derivative == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    secondDerivative =
        equation->FUNC_new(equation, equation->FUNC_dependent_variable(equation));
    if (secondDerivative == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    error = equation->FUNC_derivative(equation, derivative);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = derivative->FUNC_derivative(derivative, secondDerivative);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = find_roots(self, derivative, extremums);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    extremum_quantity = extremums->FUNC_quantity(extremums);

    if (extremum_quantity == 0)
    {
        terms          = (EquationTerm **)calloc(1, sizeof(EquationTerm *));
        terms_quantity = 0;

        if (terms == NULL)
        {
            uint32_t line = __LINE__;

            error = (Error){
                .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
                .error       = FAILED_MEMORY_ALLOCATION,
                .file_line   = NEWTON_RAPHSON + line,
            };

            myself->FUNC_log(
                myself->logger,
                ERROR_PRIORITY,
                ERROR_STRING.HEADER_PRODUCED,
                __FILENAME__,
                line,
                __func__,
                error.description);

            goto return_error;
        }

        error = equation->FUNC_ordenate(equation);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        error = equation->FUNC_get_terms(equation, &terms, &terms_quantity);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        if ((POLYNOMIAL_DEGREE)terms[0]->FUNC_core(terms[0]) % 2 == 1)
        {
            error = extremums->FUNC_resize(extremums, 1);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            CoordinatesCartesian even_more_previous_extremum = {0};
            previous_extremum                                = (CoordinatesCartesian){0};
            extremum.x                                       = 0;
            for (SIGNAL direction = POSITIVE;; extremum.x += direction * 0.1)
            {
                variable.value = extremum.x;

                error = equation->FUNC_calculate(equation, &variable, 1, &extremum.y);
                if (error.error != OO_OK)
                {
                    myself->FUNC_log(
                        myself->logger,
                        DEBUG_PRIORITY,
                        ERROR_STRING.HEADER_RECEIVED,
                        __FILENAME__,
                        __LINE__,
                        error.file_line,
                        error.description);

                    goto return_error;
                }

                if (extremum.y >= -0.1 && extremum.y <= 0.1)
                {
                    break;
                }

                if ((extremum.y > 0 && previous_extremum.y < 0
                     && even_more_previous_extremum.y > 0)
                    || (extremum.y < 0 && previous_extremum.y > 0
                        && even_more_previous_extremum.y < 0))
                {
                    break;
                }

                even_more_previous_extremum = previous_extremum;
                previous_extremum           = extremum;

                direction = extremum.y > 0.1 ? NEGATIVE : POSITIVE;
            }

            error = extremums->FUNC_push_cartesian(extremums, extremum);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            extremum_quantity  = 1;
            is_pseudo_extremum = true;

            free(terms);
            terms = NULL;
        }
        else
        {
            return OO_SUCCESS;
        }
    }

    if (is_pseudo_extremum == false)
    {
        error = roots->FUNC_resize(roots, extremum_quantity + 1);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }
    }

    for (uint32_t i = 0; i < extremum_quantity - 1; i++)   //ORDENATE EXTREMUM
    {
        error = extremums->FUNC_pull_cartesian(extremums, &extremum, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        for (uint32_t j = i + 1; j < extremum_quantity; j++)
        {
            error = extremums->FUNC_pull_cartesian(extremums, &next_extremum, j);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            if (extremum.x < next_extremum.x)
            {
                error = extremums->FUNC_position_push_cartesian(extremums, extremum, j);
                if (error.error != OO_OK)
                {
                    myself->FUNC_log(
                        myself->logger,
                        DEBUG_PRIORITY,
                        ERROR_STRING.HEADER_RECEIVED,
                        __FILENAME__,
                        __LINE__,
                        error.file_line,
                        error.description);

                    goto return_error;
                }

                error =
                    extremums->FUNC_position_push_cartesian(extremums, next_extremum, i);
                if (error.error != OO_OK)
                {
                    myself->FUNC_log(
                        myself->logger,
                        DEBUG_PRIORITY,
                        ERROR_STRING.HEADER_RECEIVED,
                        __FILENAME__,
                        __LINE__,
                        error.file_line,
                        error.description);

                    goto return_error;
                }

                extremum = next_extremum;
            }
        }
    }

    for (uint32_t i = 0; i < extremum_quantity; i++, has_next_extremum = false)
    {
        error = extremums->FUNC_pull_cartesian(extremums, &extremum, i);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        variable.value = extremum.x;
        error          = equation->FUNC_calculate(equation, &variable, 1, &extremum.y);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        myself->FUNC_log(
            myself->logger,
            INFO_PRIORITY,
            "at %s:%d: in %s extremum: (%.20f , %.20f)",
            __FILENAME__,
            __LINE__,
            __func__,
            extremum.x,
            extremum.y);

        if (extremum.y == 0)
        {
            myself->FUNC_log(
                myself->logger,
                INFO_PRIORITY,
                "at %s:%d: in %s is exactly the root (%.2f , %.2f)",
                __FILENAME__,
                __LINE__,
                __func__,
                extremum.x,
                extremum.y);

            error = roots->FUNC_push_cartesian(roots, extremum);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            i++;

            if (i == extremum_quantity - 1 && is_pseudo_extremum == false)
            {
                goto last_root;
            }

            error = extremums->FUNC_pull_cartesian(extremums, &previous_extremum, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            variable.value = extremum.x;
            error = equation->FUNC_calculate(equation, &variable, 1, &extremum.y);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            has_previous_extremum = true;

            continue;
        }

        extremum.x += 0.1;
        variable.value = extremum.x;
        error          = equation->FUNC_calculate(equation, &variable, 1, &extremum.y);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        if (has_previous_extremum == true
            && ((previous_extremum.y > 0 && extremum.y > 0)
                || (previous_extremum.y < 0 && extremum.y < 0)))
        {
            myself->FUNC_log(
                myself->logger,
                INFO_PRIORITY,
                "at %s:%d: in %s its not a root,  (%.2f , %.2f)",
                __FILENAME__,
                __LINE__,
                __func__,
                extremum.x,
                extremum.y);

            if (i == extremum_quantity - 1 && is_pseudo_extremum == false)
            {
                goto last_root;
            }

            previous_extremum = extremum;

            continue;
        }

        concavity.x = extremum.x;
        error       = secondDerivative->FUNC_calculate(
            secondDerivative, &variable, 1, &concavity.y);
        if (error.error != OO_OK)
        {
            myself->FUNC_log(
                myself->logger,
                DEBUG_PRIORITY,
                ERROR_STRING.HEADER_RECEIVED,
                __FILENAME__,
                __LINE__,
                error.file_line,
                error.description);

            goto return_error;
        }

        if (is_pseudo_extremum == false && i == 0
            && ((extremum.y > 0 && concavity.y > 0)
                || (extremum.y < 0 && concavity.y < 0)))
        {
            myself->FUNC_log(
                myself->logger,
                INFO_PRIORITY,
                "at %s:%d: in %s its not a root,  (%.2f , %.2f)",
                __FILENAME__,
                __LINE__,
                __func__,
                extremum.x,
                extremum.y);

            if (i == extremum_quantity - 1)
            {
                goto last_root;
            }

            previous_extremum     = extremum;
            has_previous_extremum = true;

            continue;
        }

        if (i < extremum_quantity - 1)
        {
            error = extremums->FUNC_pull_cartesian(extremums, &next_extremum, i + 1);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = equation->FUNC_calculate(equation, &variable, 1, &next_extremum.y);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            has_next_extremum = true;
        }

        aproximation.x = extremum.x;
        for (;;)
        {
            if (has_next_extremum && aproximation.x < next_extremum.x)
            {
                aproximation.x = (aproximation.x - next_extremum.x) / 2.0;
            }

            if (has_previous_extremum && aproximation.x > previous_extremum.x)
            {
                aproximation.x = (aproximation.x - previous_extremum.x) / 2.0;
            }

            tangent.x      = aproximation.x;
            variable.value = aproximation.x;
            error = equation->FUNC_calculate(equation, &variable, 1, &aproximation.y);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = derivative->FUNC_calculate(derivative, &variable, 1, &tangent.y);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            next_aproximation = aproximation.x - (aproximation.y / tangent.y);

            printf("aproximation (%f , %f)\n", aproximation.x, aproximation.y);

            if ((aproximation.x < next_aproximation + 0.000001
                 && aproximation.x > next_aproximation - 0.000001))
            {
                if (aproximation.x <= 0.000001 && aproximation.x >= -0.000001)
                {
                    aproximation.x = 0;
                }

                myself->FUNC_log(
                    myself->logger,
                    INFO_PRIORITY,
                    "at %s:%d:found good aproximation of: %.9f",
                    __FILENAME__,
                    __LINE__,
                    aproximation.x);

                error = roots->FUNC_push_cartesian(roots, aproximation);
                if (error.error != OO_OK)
                {
                    myself->FUNC_log(
                        myself->logger,
                        DEBUG_PRIORITY,
                        ERROR_STRING.HEADER_RECEIVED,
                        __FILENAME__,
                        __LINE__,
                        error.file_line,
                        error.description);

                    goto return_error;
                }

                break;
            }

            aproximation.x = next_aproximation;
        }

    last_root:
        if (i == extremum_quantity - 1)
        {
            error = extremums->FUNC_pull_cartesian(extremums, &extremum, i);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            extremum.x -= 0.1;
            concavity.x    = extremum.x;
            variable.value = extremum.x;
            error = equation->FUNC_calculate(equation, &variable, 1, &extremum.y);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            error = secondDerivative->FUNC_calculate(
                secondDerivative, &variable, 1, &concavity.y);
            if (error.error != OO_OK)
            {
                myself->FUNC_log(
                    myself->logger,
                    DEBUG_PRIORITY,
                    ERROR_STRING.HEADER_RECEIVED,
                    __FILENAME__,
                    __LINE__,
                    error.file_line,
                    error.description);

                goto return_error;
            }

            if (is_pseudo_extremum == false
                && ((extremum.y > 0 && concavity.y < 0)
                    || (extremum.y < 0 && concavity.y > 0)))
            {
                aproximation.x = extremum.x;
                for (;;)
                {
                    tangent.x      = aproximation.x;
                    variable.value = aproximation.x;
                    error =
                        equation->FUNC_calculate(equation, &variable, 1, &aproximation.y);
                    if (error.error != OO_OK)
                    {
                        myself->FUNC_log(
                            myself->logger,
                            DEBUG_PRIORITY,
                            ERROR_STRING.HEADER_RECEIVED,
                            __FILENAME__,
                            __LINE__,
                            error.file_line,
                            error.description);

                        goto return_error;
                    }

                    error =
                        derivative->FUNC_calculate(derivative, &variable, 1, &tangent.y);
                    if (error.error != OO_OK)
                    {
                        myself->FUNC_log(
                            myself->logger,
                            DEBUG_PRIORITY,
                            ERROR_STRING.HEADER_RECEIVED,
                            __FILENAME__,
                            __LINE__,
                            error.file_line,
                            error.description);

                        goto return_error;
                    }

                    next_aproximation = aproximation.x - (aproximation.y / tangent.y);

                    if ((aproximation.x < next_aproximation + 0.000001
                         && aproximation.x > next_aproximation - 0.000001))
                    {
                        if (aproximation.x <= 0.000001 && aproximation.x >= -0.000001)
                        {
                            aproximation.x = 0;
                        }

                        myself->FUNC_log(
                            myself->logger,
                            INFO_PRIORITY,
                            "at %s:%d:found good aproximation of last root: %.9f",
                            __FILENAME__,
                            __LINE__,
                            aproximation.x);

                        error = roots->FUNC_push_cartesian(roots, aproximation);
                        if (error.error != OO_OK)
                        {
                            myself->FUNC_log(
                                myself->logger,
                                DEBUG_PRIORITY,
                                ERROR_STRING.HEADER_RECEIVED,
                                __FILENAME__,
                                __LINE__,
                                error.file_line,
                                error.description);

                            goto return_error;
                        }

                        break;
                    }

                    aproximation.x = next_aproximation;
                }

                break;
            }
        }

        previous_extremum     = extremum;
        has_previous_extremum = true;
    }

    extremums->FUNC_destruct(extremums);
    derivative->FUNC_destruct(derivative);
    secondDerivative->FUNC_destruct(secondDerivative);

    return OO_SUCCESS;

return_error:
    if (terms != NULL)
    {
        free(terms);
    }

    if (extremums != NULL)
    {
        extremums->FUNC_destruct(extremums);
    }

    if (derivative != NULL)
    {
        derivative->FUNC_destruct(derivative);
    }

    if (secondDerivative != NULL)
    {
        secondDerivative->FUNC_destruct(secondDerivative);
    }

    return error;
}

/*!
    \brief Bhaskara method for finding roots of a quadratic equation.

    \param logger Optional object to controll the flow of the logs and its priorities.
    \param terms Pointer to array containing the terms of the equation.
    \param terms_quantity Length of the terms pointer.
    \param x_roots Array to hold up to two roots.
    \param root_quantity Pointer to variable that signal how much roots were found by bhaskara method. 
*/
static Error bhaskara(
    MeetingPointMethod *self,
    Equation           *equation,
    float               x_roots[2],
    uint8_t            *root_quantity)
{
    NewtonRaphson *myself = (NewtonRaphson *)(self->method);

    Error          error          = {0};
    float          a              = 0;
    float          b              = 0;
    float          c              = 0;
    float          delta          = 0;
    uint16_t       terms_quantity = 0;
    EquationTerm **terms          = (EquationTerm **)calloc(1, sizeof(EquationTerm *));

    if (x_roots == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (root_quantity == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (equation == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.NULL_ARGUMENT,
            .error       = NULL_ARGUMENT,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (terms == NULL)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = ERROR_STRING.FAILED_MEMORY_ALLOCATION,
            .error       = FAILED_MEMORY_ALLOCATION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    equation->FUNC_ordenate(equation);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    error = equation->FUNC_get_terms(equation, &terms, &terms_quantity);
    if (error.error != OO_OK)
    {
        myself->FUNC_log(
            myself->logger,
            DEBUG_PRIORITY,
            ERROR_STRING.HEADER_RECEIVED,
            __FILENAME__,
            __LINE__,
            error.file_line,
            error.description);

        goto return_error;
    }

    if (terms_quantity == 0)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "no terms",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    if (is_quadratic_equation(terms, terms_quantity) == false)
    {
        uint32_t line = __LINE__;

        error = (Error){
            .description = "can only aplly bhaskara into quadratic equations",
            .error       = IMPOSSIBLE_EXECUTION,
            .file_line   = NEWTON_RAPHSON + line,
        };

        myself->FUNC_log(
            myself->logger,
            ERROR_PRIORITY,
            ERROR_STRING.HEADER_PRODUCED,
            __FILENAME__,
            line,
            __func__,
            error.description);

        goto return_error;
    }

    for (uint16_t i = 0; i < terms_quantity; i++)
    {
        if (terms[i]->FUNC_core(terms[i]) == 2)
        {
            a = terms[i]->FUNC_coeficient(terms[i]);
        }
        else if (terms[i]->FUNC_core(terms[i]) == 1)
        {
            b = terms[i]->FUNC_coeficient(terms[i]);
        }
        else if (terms[i]->FUNC_core(terms[i]) == 0)
        {
            c = terms[i]->FUNC_coeficient(terms[i]);
        }
    }
    free(terms);
    terms = NULL;

    delta = pow(b, 2) - (4 * a * c);

    if (delta < 0)
    {
        *root_quantity = 0;
    }
    else if (delta == 0)
    {
        x_roots[0]     = (-b) / (2 * a);
        *root_quantity = 1;
    }
    else
    {
        x_roots[0]     = ((-b) + pow(delta, 0.5)) / (2 * a);
        x_roots[1]     = ((-b) - pow(delta, 0.5)) / (2 * a);
        *root_quantity = 2;
    }

    for (uint8_t i = 0; i < *root_quantity; i++)
    {
        if (x_roots[i] <= 0.000001 && x_roots[i] >= -0.000001)
        {
            x_roots[i] = 0;
        }
    }

    return OO_SUCCESS;

return_error:
    if (terms != NULL)
    {
        free(terms);
    }

    return error;
}

static bool is_line_equation(EquationTerm *terms[], uint16_t terms_quantity)
{
    return (terms[0]->FUNC_type(terms[0]) == POLYNOMIAL_2D
            && terms[0]->FUNC_core(terms[0]) == LINE)
        && (terms_quantity == 1
            || (terms_quantity == 2
                && (terms[1]->FUNC_type(terms[1]) == POLYNOMIAL_2D
                    && terms[1]->FUNC_core(terms[1]) == CONSTANT)));
}

static bool is_quadratic_equation(EquationTerm *terms[], uint16_t terms_quantity)
{
    return (terms[0]->FUNC_type(terms[0]) == POLYNOMIAL_2D
            && terms[0]->FUNC_core(terms[0]) == QUADRATIC)
        && (terms_quantity == 1
            || (terms_quantity == 2
                && (terms[1]->FUNC_type(terms[1]) == POLYNOMIAL_2D
                    && terms[1]->FUNC_core(terms[1]) == LINE))
            || (terms_quantity == 2
                && (terms[1]->FUNC_type(terms[1]) == POLYNOMIAL_2D
                    && terms[1]->FUNC_core(terms[1]) == CONSTANT))
            || (terms_quantity == 3
                && (terms[1]->FUNC_type(terms[1]) == POLYNOMIAL_2D
                    && terms[1]->FUNC_core(terms[1]) == LINE)
                && (terms[2]->FUNC_type(terms[2]) == POLYNOMIAL_2D
                    && terms[2]->FUNC_core(terms[2]) == CONSTANT)));
}

static bool is_polynomial_equation(EquationTerm *terms[], uint16_t terms_quantity)
{
    for (uint16_t i = 0; i < terms_quantity; i++)
    {
        if (terms[i]->FUNC_type(terms[i]) != POLYNOMIAL_2D)
        {
            return false;
        }
    }

    return true;
}