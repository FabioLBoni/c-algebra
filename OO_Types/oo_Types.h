/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    24 de jul de 2023
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#pragma once

#include <string.h>

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C"
{
#endif

#ifndef __FILENAME__
    #define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#define PI 3.141592653589793
#define DEGREE_TO_RADIAN (PI / 180.0)
#define RADIAN_TO_DEGREE (1.0 / DEGREE_TO_RADIAN)

typedef enum LOG_PRIORITY
{
    DEBUG_PRIORITY,
    INFO_PRIORITY,
    WARNING_PRIORITY,
    ERROR_PRIORITY,
} LOG_PRIORITY;

typedef enum EQUATION_TERM
{
    COMPOSITE_2D,
    POLYNOMIAL_2D,
    NEGATIVE_INTEGER_POWER_2D,
    EXPONENTIAL_2D,
    LOGARITHIMIC_2D,
    TRIGONOMETRIC_2D,
    RATIONAL_2D,
    IRATIONAL_2D,
} EQUATION_TERM;

typedef enum AXIS
{
    ANY     = -1,
    NO_AXIS = 0,
    ORIGIN  = 1,
    RADIUS  = 3,
    THETA   = 5,
    PHI     = 7,
    X       = 11,
    Y       = 13,
    Z       = 17,
} AXIS;

typedef enum SIGNAL
{
    NEGATIVE = -1,
    NEUTRAL  = 0,
    POSITIVE = 1,
} SIGNAL;

typedef enum POLYNOMIAL_DEGREE
{
    CONSTANT,
    LINE,
    QUADRATIC,
    CUBIC,
    QUARTIC,
    QUINTIC,
    SEXTIC,
    SEPTIC,
    OCTIC,
    NONIC,
    DECIC,
    HENDECIC,
    DODECIC,
    TREDECIC,
    QUATTUORDECIC,
    QUINDECIC,
    UNKNOWN,
} POLYNOMIAL_DEGREE;

#ifdef __cplusplus /* If this is a C++ compiler, end C linkage */
}
#endif
