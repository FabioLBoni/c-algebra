/*
 *
 *    /\\\\\\\\\\\\\\\                  /\\\\\\\\\\\\\         /\\\\\       /\\\\\     /\\\  /\\\\\\\\\\\         
 *    \/\\\///////////                  \/\\\/////////\\\     /\\\///\\\    \/\\\\\\   \/\\\ \/////\\\///         
 *     \/\\\                             \/\\\       \/\\\   /\\\/  \///\\\  \/\\\/\\\  \/\\\     \/\\\           
 *      \/\\\\\\\\\\\                     \/\\\\\\\\\\\\\\   /\\\      \//\\\ \/\\\//\\\ \/\\\     \/\\\          
 *       \/\\\///////                      \/\\\/////////\\\ \/\\\       \/\\\ \/\\\\//\\\\/\\\     \/\\\         
 *        \/\\\                             \/\\\       \/\\\ \//\\\      /\\\  \/\\\ \//\\\/\\\     \/\\\        
 *         \/\\\                             \/\\\       \/\\\  \///\\\  /\\\    \/\\\  \//\\\\\\     \/\\\       
 *          \/\\\              /\\\           \/\\\\\\\\\\\\\/     \///\\\\\/     \/\\\   \//\\\\\  /\\\\\\\\\\\  
 *           \///              \///            \/////////////         \/////       \///     \/////  \///////////  
 *
 *
 *  Created:    25 de jan de 2024
 *  Author:     F. Boni    Email:      fabioboni96@hotmail.com
 *  Repository: gitlab.com/FabioLBoni/c-algebra
 *  Copyright (c) 2023-2024 Fabio Luis Boni - MIT License
 */

#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "oo_Global.h"

pthread_mutex_t simple_logger_buffer_mutex = PTHREAD_MUTEX_INITIALIZER;
char            simple_logger_buffer[BUFFER_SIZE_SIMPLE_LOGGER] = {0};
char           *get_simple_logger_buffer();
void            release_simple_logger_buffer();

/*!
    \brief Standard function for objects to register as logger. It prints on terminal.

    \param self Should be NULL. Pseudo paramater, used to implement Logger interface function.
    \param priority Level of priority of the log.
    \param format String following the printf pattern.
    \param ... Multiple arguments following the printf pattern.
*/
void simple_logger(Logger *self, LOG_PRIORITY priority, char *format, ...)
{
    char   *level  = NULL;
    char   *buffer = get_simple_logger_buffer();
    va_list args;
    va_start(args, format);

    if (priority >= ERROR_PRIORITY)
    {
        level = "ERROR: ";
    }
    else if (priority >= WARNING_PRIORITY)
    {
        level = "WARNING: ";
    }
    else if (priority >= INFO_PRIORITY)
    {
        level = "INFO: ";
    }
    else
    {
        level = "DEBUG: ";
    }

    vsnprintf(buffer, BUFFER_SIZE_SIMPLE_LOGGER, format, args);

    printf("%s%s\n", level, buffer);

    release_simple_logger_buffer();
}

char *get_simple_logger_buffer()
{
    pthread_mutex_lock(&simple_logger_buffer_mutex);

    simple_logger_buffer[0] = '\0';

    return simple_logger_buffer;
}

void release_simple_logger_buffer()
{
    simple_logger_buffer[0] = '\0';

    pthread_mutex_unlock(&simple_logger_buffer_mutex);
}